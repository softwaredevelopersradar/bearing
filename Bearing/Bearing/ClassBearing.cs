﻿/*
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
*/


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
//using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.IO;
//using System.Drawing.Imaging;
//using System.Windows.Input;

using GeoCalculator;
using ModelsTablesDBLib;

namespace Bearing
{
    public static class ClassBearing
    {


        [DllImport("user32.dll")]
        static extern IntPtr GetForegroundWindow();

        // VARS *******************************************************************************



        // ******************************************************************************* VARS


        // Конструктор *************************************************************************

        //public ClassBearing()
        //{



        //} // Конструктор
        // ************************************************************************* Конструктор

        // *************************************************************************************
        // Module
        // *************************************************************************************
        public static double module(double a)
        {
            if (a >= 0) return (a);
            else return (-a);

        } // P/P module
        // *************************************************************************************

        // *************************************************************************************
        // Module
        // *************************************************************************************

        public static int f_validate(double latitude, double longitude)
        {
            if (latitude < -90.0 || latitude > 90.0 || longitude < -180.0
                || longitude > 180.0)
            {
                //MessageBox.Show("Incorrect input of latitude and longitude");
                return -1;
            }

            return 0;
        }
        // *************************************************************************************

        // BEARING ****************************************************************

        // ***********************************************************************
        // 1-й вариант

        // Отрисовка траектории N фиктивных точек по пеленгу
        //
        // Входные параметры: 
        // Theta_Pel(град) - пеленг,
        // Mmax_Pel(м) - максимальная дальность отображения пеленга,
        // NumbFikt_Pel - количество фиктивных точек
        // LatP_Pel,LongP_Pel (град) - широта и долгота стояния пеленгатора 

        // fl_84_42
        //=1-> широта и долгота в WGS84
        //=2-> широта и долгота в СК42(Красовский)

        //
        // Выходные параметры: 

        // массив mas_Pel(Latitude(град),Longitude(град)) ->
        // широта, долгота фиктивных точек

        // массив mas_Pel_XYZ -> координаты фиктивных точек в
        // опорной геоцентрической СК

        // XP_Pel,YP_Pel,ZP_Pel (м) -координаты пеленгатора в опорной геоцентрической СК
        // XFN_Pel,YFN_Pel,ZFN_Pel (м) -координаты N-й фиктивной точки в опорной 
        // геоцентрической СК

        // Широта -90(юг)...+90(север)
        // Долгота -180(на запад от Гринвича)...+180(на восток от Гринвича)
        // Пеленг 0...360(по часовой стрелке)
        // ***********************************************************************
        public static void f_Bearing
            (

                 // Пеленг (град)
                 double Theta_Pel,
            // Max дальность отображения пеленга (m)
                 double Mmax_Pel,
            // Количество фиктивных точек в плоскости пеленгования пеленгатора
                 uint NumbFikt_Pel,

                 // Широта и долгота стояния пеленгатора (град)
                 double LatP_Pel,
                 double LongP_Pel,

                 //=1-> широта и долгота в WGS84
                 //=2-> широта и долгота в СК42(Красовский)
                 int fl_84_42,

                 // Координаты пеленгатора в опорной геоцентрической СК
                 ref double XP_Pel,
                 ref double YP_Pel,
                 ref double ZP_Pel,

                 // Координаты N-й фиктивной точки в опорной геоцентрической СК
                 ref double XFN_Pel,
                 ref double YFN_Pel,
                 ref double ZFN_Pel,

                 // Координаты фиктивных точек
                 ref double[] mas_Pel,
                 ref double[] mas_Pel_XYZ


            )
        {

            // ------------------------------------------------------------------------

            // Индекс фиктивной точки (1,2, ...)
            uint IndFP_Pel = 0;
            // Индекс i-й фиктивной точки в массивах(0,...)
            uint IndMas_Pel = 0;

            // Радиус Земли (шар)
            double REarth_Pel = 0;
            // Радиус Земли  в точке стояния пеленгатора с учетом текущей широты 
            double REarthP_Pel = 0;
            // Координаты фиктивной точки в опорной геоцентрической СК
            // (Поворот СК1 на угол=долготе пеленгатора)
            double XiG_Pel = 0;
            double YiG_Pel = 0;
            double ZiG_Pel = 0;

            // Угловые координаты фиктивной точки в опорной геоцентрической СК
            double Ri_Pel = 0;
            double LATi_Pel = 0;
            double LONGi_Pel = 0;

            // Широта и долгота стояния пеленгатора
            double LatP_Pel_tmp = 0;
            double LongP_Pel_tmp = 0;

            // Количество фиктивных точек в плоскости пеленгования пеленгатора
            uint NumbFikt_Pel_tmp = 0;

            // Max дальность отображения пеленга
            double Mmax_Pel_tmp = 0;

            // Пеленг
            double Theta_Pel_tmp = 0;

            // Координаты пеленгатора в опорной геоцентрической СК
            double XP_Pel_tmp = 0;
            double YP_Pel_tmp = 0;
            double ZP_Pel_tmp = 0;

            // ----------------------------------------------------------------

            double Lat_Pel_N = 0;  //Lat(n)
            double Lat_Pel_N1 = 0;  //Lat(n-1)
            double Long_Pel_N = 0;  //Long(n)
            double Long_Pel_N1 = 0;  //Long(n-1)
            double V_Pel = 0;
            double r_Pel_N = 0;  //Lat(n)
            double r_Pel_N1 = 0;  //Lat(n-1)

            int fl_var2 = 0;
            int fl1_var2 = 0;
            int fl2_var2 = 0;

            // WGS84
            double aa = 6378137;

            // SSS1
            //double RRR = 6371008.8;
            double RRR = 6371008.771333;


            double bb = 6356752.3142;

            double cc = 299796459.2;
            double sss = 0;
            double ttt = 0;
            double dttt = 0;
            // Initial conditions ****************************************************

            // ...............................................................

            // Индекс фиктивной точки (1,2, ...)
            IndFP_Pel = 1;

            // Old
            //REarth_Pel = 6378245; // !!! Был в старом проекте

            // PPLL
            //Средний радиус Земли (шар)
            if (fl_84_42 == 2) // Красовский
                // SSS1
                //REarth_Pel = 6371220;
                REarth_Pel = 6371117.673;
            else
                // RRR
                // SSS1
                //REarth_Pel = 6378136.3; // WGS84
                REarth_Pel = 6371008.771333; // WGS84

            // .........................................................................
            // Широта и долгота стояния пеленгатора

            LatP_Pel_tmp = (LatP_Pel * Math.PI) / 180;   // grad->rad
            LongP_Pel_tmp = (LongP_Pel * Math.PI) / 180;
            // ...............................................................
            // Радиус Земли  в точке стояния пеленгатора с учетом текущей широты 

            // m  Old
            REarthP_Pel = REarth_Pel; // Шар

            // PPLL
            // Сжатие=(a-b)/a
            // 0.003352 -Красовский
            if (fl_84_42 == 2) // Красовский
            {
                REarthP_Pel = REarth_Pel * (1 - 0.003352 * Math.Sin(LatP_Pel_tmp) * Math.Sin(LatP_Pel_tmp));

                // SSS1
                REarthP_Pel = 6371117.673;

            }
            else
            {
                REarthP_Pel = REarth_Pel * (1 - 0.00669438 * Math.Sin(LatP_Pel_tmp) * Math.Sin(LatP_Pel_tmp)); // WGS84

                //REarthP_Pel = Math.Sqrt( ((aa*aa*Math.Cos(LatP_Pel_tmp))*(aa * aa * Math.Cos(LatP_Pel_tmp)) +(bb*bb*Math.Sin(LatP_Pel_tmp))*(bb * bb * Math.Sin(LatP_Pel_tmp)))/
                //                        ((aa*Math.Cos(LatP_Pel_tmp))*(aa * Math.Cos(LatP_Pel_tmp)) +(bb * Math.Sin(LatP_Pel_tmp)) *(bb * Math.Sin(LatP_Pel_tmp))) );

                // RRR 
                // SSS1
                //REarthP_Pel = 6371008.8;
                REarthP_Pel = 6371008.771333;


            }

            // .........................................................................
            // Количество фиктивных точек в плоскости пеленгования пеленгатора

            NumbFikt_Pel_tmp = NumbFikt_Pel;
            // .........................................................................
            // Max дальность отображения пеленга (m)

            Mmax_Pel_tmp = Mmax_Pel;
            // .........................................................................


            // Координаты пеленгатора в опорной геоцентрической СК

            // m
            XP_Pel_tmp = REarthP_Pel * Math.Cos(LatP_Pel_tmp) * Math.Cos(LongP_Pel_tmp);
            YP_Pel_tmp = REarthP_Pel * Math.Cos(LatP_Pel_tmp) * Math.Sin(LongP_Pel_tmp);
            ZP_Pel_tmp = REarthP_Pel * Math.Sin(LatP_Pel_tmp);


            // LLPP1
            ClassGeoCalculator.f_BLH_XYZ_84(
                                            LatP_Pel,
                                            LongP_Pel,
                                            ref XP_Pel_tmp,
                                            ref YP_Pel_tmp,
                                            ref ZP_Pel_tmp
                                            );


            // m
            XP_Pel = XP_Pel_tmp;
            YP_Pel = YP_Pel_tmp;
            ZP_Pel = ZP_Pel_tmp;
            // .........................................................................
            // Пеленг

            // grad -> rad
            Theta_Pel_tmp = (Theta_Pel * Math.PI) / 180;
            // .........................................................................

            V_Pel = Mmax_Pel_tmp / NumbFikt_Pel;

            Lat_Pel_N1 = LatP_Pel_tmp;
            Long_Pel_N1 = LongP_Pel_tmp;

            fl_var2 = 0;
            fl1_var2 = 0;
            fl2_var2 = 0;
            // .........................................................................


            // **************************************************** Initial conditions

            // WHILE *****************************************************************

            // PPLL
            double dss = 0;
            double lati = 0;
            double lngi = 0;


            //double TTT = 0;
            //ttt = 1; // grad
            //ttt = (ttt * Math.PI) / 180;   // grad->rad

            while (IndMas_Pel < (NumbFikt_Pel))
            {

                // .........................................................................

                // .........................................................................
                // LAT

                // PPLL
                if (fl_84_42 == 2) // Красовский
                {
                    REarthP_Pel = REarth_Pel * (1 - 0.003352 * Math.Sin(Lat_Pel_N1) * Math.Sin(Lat_Pel_N1));

                    // SSS1
                    REarthP_Pel = 6371117.673;

                }
                else
                {
                    REarthP_Pel = REarth_Pel * (1 - 0.00669438 * Math.Sin(Lat_Pel_N1) * Math.Sin(Lat_Pel_N1)); // WGS84

                    //REarthP_Pel = Math.Sqrt(((aa * aa * Math.Cos(Lat_Pel_N1)) * (aa * aa * Math.Cos(Lat_Pel_N1)) + (bb * bb * Math.Sin(Lat_Pel_N1)) * (bb * bb * Math.Sin(Lat_Pel_N1))) /
                    //                        ((aa * Math.Cos(Lat_Pel_N1)) * (aa * Math.Cos(Lat_Pel_N1)) + (bb * Math.Sin(Lat_Pel_N1)) * (bb * Math.Sin(Lat_Pel_N1))));

                    // RRR
                    // SSS1
                    //REarthP_Pel = 6371008.8;
                    REarthP_Pel = 6371008.771333;

                }

                // PPLL
                // rab
                if ((fl_var2 == 0) && (fl2_var2 == 0))
                    Lat_Pel_N = Lat_Pel_N1 + (V_Pel * Math.Cos(Theta_Pel_tmp)) / REarthP_Pel;
                //if ((fl_var2 == 0) && (fl2_var2 == 0))
                //    Lat_Pel_N = Lat_Pel_N1 + (V_Pel * Math.Cos(Theta_Pel_tmp)* Math.Cos(ttt)) / REarthP_Pel;


                // Перешли через северный полюс
                else if ((fl_var2 == 1) && (fl2_var2 == 0) && (Math.Cos(Theta_Pel_tmp) >= 0))
                    Lat_Pel_N = Lat_Pel_N1 - (V_Pel * Math.Cos(Theta_Pel_tmp)) / REarthP_Pel;
                else if ((fl_var2 == 1) && (fl2_var2 == 0) && (Math.Cos(Theta_Pel_tmp) < 0))
                    Lat_Pel_N = Lat_Pel_N1 + (V_Pel * Math.Cos(Theta_Pel_tmp)) / REarthP_Pel;

                // Перешли через южный полюс 
                else if ((fl2_var2 == 1) && (fl_var2 == 0) && (Math.Cos(Theta_Pel_tmp) >= 0))
                    Lat_Pel_N = Lat_Pel_N1 + (V_Pel * Math.Cos(Theta_Pel_tmp)) / REarthP_Pel;
                else if ((fl2_var2 == 1) && (fl_var2 == 0) && (Math.Cos(Theta_Pel_tmp) < 0))
                    Lat_Pel_N = Lat_Pel_N1 - (V_Pel * Math.Cos(Theta_Pel_tmp)) / REarthP_Pel;


                // Перешли через северный полюс
                if (
                    (fl_var2 == 0) &&
                    (Lat_Pel_N > 0) &&
                    (Lat_Pel_N >= Math.PI / 2)
                   )
                {
                    Lat_Pel_N = Math.PI / 2;

                    fl_var2 = 1;
                    fl2_var2 = 0;
                    fl1_var2 = 0;

                }

                // Перешли через южный полюс
                if (
                    (fl2_var2 == 0) &&
                    (Lat_Pel_N < 0) &&
                    (Lat_Pel_N <= -Math.PI / 2)
                   )
                {
                    Lat_Pel_N = -Math.PI / 2;
                    //Theta_Pel = 0;

                    fl2_var2 = 1;
                    fl_var2 = 0;
                    fl1_var2 = 0;

                }

                // .........................................................................
                // LONG

                // Перешли через полюс
                if (
                    (fl1_var2 == 0) &&
                    ((fl_var2 == 1) || (fl2_var2 == 1))
                   )
                {
                    if (Long_Pel_N1 >= 0)
                        Long_Pel_N1 = Long_Pel_N1 + Math.PI;
                    else if (Long_Pel_N1 < 0)
                        Long_Pel_N1 = Long_Pel_N1 - Math.PI;

                    if (Long_Pel_N1 > Math.PI)
                        Long_Pel_N1 = -(2 * Math.PI - Long_Pel_N1);
                    else if (Long_Pel_N1 < -Math.PI)
                        Long_Pel_N1 = 2 * Math.PI + Long_Pel_N1;


                    fl1_var2 = 1;
                }


                // Обычный расчет
                Long_Pel_N = Long_Pel_N1 +
                            (V_Pel * Math.Sin(Theta_Pel_tmp)) / (REarthP_Pel * Math.Cos(Lat_Pel_N1));

                if (Long_Pel_N > Math.PI)
                    Long_Pel_N = -(2 * Math.PI - Long_Pel_N);
                else if (Long_Pel_N < -Math.PI)
                    Long_Pel_N = 2 * Math.PI + Long_Pel_N;
                // .........................................................................

                // .........................................................................
                XiG_Pel = REarthP_Pel * Math.Cos(Lat_Pel_N) * Math.Cos(Long_Pel_N);
                YiG_Pel = REarthP_Pel * Math.Cos(Lat_Pel_N) * Math.Sin(Long_Pel_N);
                ZiG_Pel = REarthP_Pel * Math.Sin(Lat_Pel_N);
                Ri_Pel = Math.Sqrt(XiG_Pel * XiG_Pel + YiG_Pel * YiG_Pel + ZiG_Pel * ZiG_Pel);

                // .........................................................................

                Lat_Pel_N1 = Lat_Pel_N;
                Long_Pel_N1 = Long_Pel_N;
                // .........................................................................


                // Массивы ***************************************************************
                // Занесение в массивы

                // LATi,LONGi (град)
                mas_Pel[IndMas_Pel * 2] = (Lat_Pel_N * 180) / Math.PI;
                mas_Pel[IndMas_Pel * 2 + 1] = (Long_Pel_N * 180) / Math.PI;


                // LLPP1
                ClassGeoCalculator.f_BLH_XYZ_84(
                                                mas_Pel[IndMas_Pel * 2], 
                                                mas_Pel[IndMas_Pel * 2 + 1],
                                                ref XiG_Pel,
                                                ref YiG_Pel,
                                                ref ZiG_Pel
                                                );


                // XYZ в опорной геоцентрической СК (м)
                mas_Pel_XYZ[IndMas_Pel * 3] = XiG_Pel;
                mas_Pel_XYZ[IndMas_Pel * 3 + 1] = YiG_Pel;
                mas_Pel_XYZ[IndMas_Pel * 3 + 2] = ZiG_Pel;

                // *************************************************************** Массивы

                // Вывод для отладки ****************************************************

                //*(pmas_Otl_Pel+IndMas_Pel) = Di_Pel;

                // **************************************************** Вывод для отладки`

                // ***********************************************************************
                // Переход к следующей фиктивной точке

                IndFP_Pel += 1;
                IndMas_Pel += 1;
                // ***********************************************************************

            }; // WHILE

            // ***************************************************************** WHILE

            XFN_Pel = XiG_Pel;
            YFN_Pel = YiG_Pel;
            ZFN_Pel = ZiG_Pel;

        } // Функция f_Bearing (1-й вариант)

        //************************************************************************

        // ***********************************************************************
        // 2-й вариант

        // Отрисовка траектории N фиктивных точек по пеленгу
        //
        // Входные параметры: 
        // Theta_Pel(град) - пеленг,
        // Mmax_Pel(м) - максимальная дальность отображения пеленга,
        // NumbFikt_Pel - количество фиктивных точек
        // LatP_Pel,LongP_Pel (град) - широта и долгота стояния пеленгатора 

        // fl_84_42
        //=1-> широта и долгота в WGS84
        //=2-> широта и долгота в СК42(Красовский)

        // Выходные параметры: 

        // LatN_Pel,LongN_Pel -координаты N-й фиктивной точки (градусы) 

        // Широта -90(юг)...+90(север)
        // Долгота -180(на запад от Гринвича)...+180(на восток от Гринвича)
        // Пеленг 0...360(по часовой стрелке)
        // ***********************************************************************
        public static void f_Bearing
            (

                 // Пеленг (град)
                 double Theta_Pel,
                 // Max дальность отображения пеленга (m)
                 double Mmax_Pel,
                 // Количество фиктивных точек в плоскости пеленгования пеленгатора
                 uint NumbFikt_Pel,

                 // Широта и долгота стояния пеленгатора (град)
                 double LatP_Pel,
                 double LongP_Pel,

                 //=1-> широта и долгота в WGS84
                 //=2-> широта и долгота в СК42(Красовский)
                 int fl_84_42,

                 // Координаты N-й фиктивной точки (градусы)
                 ref double LatN_Pel,
                 ref double LongN_Pel

            )
        {

            // ------------------------------------------------------------------------

            // Индекс фиктивной точки (1,2, ...)
            uint IndFP_Pel = 0;
            // Индекс i-й фиктивной точки в массивах(0,...)
            uint IndMas_Pel = 0;

            // Радиус Земли (шар)
            double REarth_Pel = 0;
            // Радиус Земли  в точке стояния пеленгатора с учетом текущей широты 
            double REarthP_Pel = 0;
            // Координаты фиктивной точки в опорной геоцентрической СК
            // (Поворот СК1 на угол=долготе пеленгатора)
            double XiG_Pel = 0;
            double YiG_Pel = 0;
            double ZiG_Pel = 0;

            // Угловые координаты фиктивной точки в опорной геоцентрической СК
            double Ri_Pel = 0;
            double LATi_Pel = 0;
            double LONGi_Pel = 0;

            // Широта и долгота стояния пеленгатора
            double LatP_Pel_tmp = 0;
            double LongP_Pel_tmp = 0;

            // Количество фиктивных точек в плоскости пеленгования пеленгатора
            uint NumbFikt_Pel_tmp = 0;

            // Max дальность отображения пеленга
            double Mmax_Pel_tmp = 0;

            // Пеленг
            double Theta_Pel_tmp = 0;

            // Координаты пеленгатора в опорной геоцентрической СК
            double XP_Pel_tmp = 0;
            double YP_Pel_tmp = 0;
            double ZP_Pel_tmp = 0;

            // ----------------------------------------------------------------

            double Lat_Pel_N = 0;  //Lat(n)
            double Lat_Pel_N1 = 0;  //Lat(n-1)
            double Long_Pel_N = 0;  //Long(n)
            double Long_Pel_N1 = 0;  //Long(n-1)
            double V_Pel = 0;

            int fl_var2 = 0;
            int fl1_var2 = 0;
            int fl2_var2 = 0;


            // Initial conditions ****************************************************

            // ...............................................................

            // Индекс фиктивной точки (1,2, ...)
            IndFP_Pel = 1;

            // Old
            //REarth_Pel = 6378245; // !!! Был в старом проекте

            //RRR
            //Средний радиус Земли (шар)
            if (fl_84_42 == 2) // Красовский
                // SSS1
                //REarth_Pel = 6371220;
                REarth_Pel = 6371117.673;
            else
                // SSS1
                //REarth_Pel = 6378136.3; // WGS84
                //REarth_Pel = 6371008.8;
                REarth_Pel = 6371008.771333;


            // m  Old
            REarthP_Pel = REarth_Pel; // Шар

            // PPLL
            // Сжатие=(a-b)/a
            // 0.003352 -Красовский
            if (fl_84_42 == 2) // Красовский
            {
                REarthP_Pel = REarth_Pel * (1 - 0.003352 * Math.Sin(LatP_Pel_tmp) * Math.Sin(LatP_Pel_tmp));
                // SSS1
                REarthP_Pel = 6371117.673;

            }
            else
            {
                REarthP_Pel = REarth_Pel * (1 - 0.00669438 * Math.Sin(LatP_Pel_tmp) * Math.Sin(LatP_Pel_tmp)); // WGS84

                //REarthP_Pel = Math.Sqrt( ((aa*aa*Math.Cos(LatP_Pel_tmp))*(aa * aa * Math.Cos(LatP_Pel_tmp)) +(bb*bb*Math.Sin(LatP_Pel_tmp))*(bb * bb * Math.Sin(LatP_Pel_tmp)))/
                //                        ((aa*Math.Cos(LatP_Pel_tmp))*(aa * Math.Cos(LatP_Pel_tmp)) +(bb * Math.Sin(LatP_Pel_tmp)) *(bb * Math.Sin(LatP_Pel_tmp))) );

                // RRR
                //REarthP_Pel = 6371008.8;
                // SSS1
                REarthP_Pel = 6371008.771333;

            }
            // .........................................................................
            // Широта и долгота стояния пеленгатора

            LatP_Pel_tmp = (LatP_Pel * Math.PI) / 180;   // grad->rad
            LongP_Pel_tmp = (LongP_Pel * Math.PI) / 180;
            // ...............................................................
            // Радиус Земли  в точке стояния пеленгатора с учетом текущей широты 

            // m  Old
            REarthP_Pel = REarth_Pel; // Шар

            // PPLL
            // Сжатие=(a-b)/a
            // 0.003352 -Красовский
            if (fl_84_42 == 2) // Красовский
            {
                REarthP_Pel = REarth_Pel * (1 - 0.003352 * Math.Sin(LatP_Pel_tmp) * Math.Sin(LatP_Pel_tmp));

                // SSS1
                REarthP_Pel = 6371117.673;

            }
            else
            {
                REarthP_Pel = REarth_Pel * (1 - 0.00669438 * Math.Sin(LatP_Pel_tmp) * Math.Sin(LatP_Pel_tmp)); // WGS84

                // RRR
                //REarthP_Pel = 6371008.8;
                // SSS1
                REarthP_Pel = 6371008.771333;

            }
            // .........................................................................
            // Количество фиктивных точек в плоскости пеленгования пеленгатора

            NumbFikt_Pel_tmp = NumbFikt_Pel;
            // .........................................................................
            // Max дальность отображения пеленга (m)

            Mmax_Pel_tmp = Mmax_Pel;
            // .........................................................................
            // Координаты пеленгатора в опорной геоцентрической СК

            // m
            XP_Pel_tmp = REarthP_Pel * Math.Cos(LatP_Pel_tmp) * Math.Cos(LongP_Pel_tmp);
            YP_Pel_tmp = REarthP_Pel * Math.Cos(LatP_Pel_tmp) * Math.Sin(LongP_Pel_tmp);
            ZP_Pel_tmp = REarthP_Pel * Math.Sin(LatP_Pel_tmp);


            // LLPP1
            ClassGeoCalculator.f_BLH_XYZ_84(
                                            LatP_Pel,
                                            LongP_Pel,
                                            ref XP_Pel_tmp,
                                            ref YP_Pel_tmp,
                                            ref ZP_Pel_tmp
                                            );

            // m
            //XP_Pel = XP_Pel_tmp;
            //YP_Pel = YP_Pel_tmp;
            //ZP_Pel = ZP_Pel_tmp;
            // .........................................................................
            // Пеленг

            // grad -> rad
            Theta_Pel_tmp = (Theta_Pel * Math.PI) / 180;
            // .........................................................................

            V_Pel = Mmax_Pel_tmp / NumbFikt_Pel;
            Lat_Pel_N1 = LatP_Pel_tmp;
            Long_Pel_N1 = LongP_Pel_tmp;
            fl_var2 = 0;
            fl1_var2 = 0;
            fl2_var2 = 0;
            // .........................................................................


            // **************************************************** Initial conditions

            // WHILE *****************************************************************

            while (IndMas_Pel < (NumbFikt_Pel))
            {

                // PPLL
                if (fl_84_42 == 2) // Красовский
                {
                    REarthP_Pel = REarth_Pel * (1 - 0.003352 * Math.Sin(Lat_Pel_N1) * Math.Sin(Lat_Pel_N1));

                    // SSS1
                    REarthP_Pel = 6371117.673;

                }
                else
                {
                    REarthP_Pel = REarth_Pel * (1 - 0.00669438 * Math.Sin(Lat_Pel_N1) * Math.Sin(Lat_Pel_N1)); // WGS84

                    //REarthP_Pel = Math.Sqrt(((aa * aa * Math.Cos(Lat_Pel_N1)) * (aa * aa * Math.Cos(Lat_Pel_N1)) + (bb * bb * Math.Sin(Lat_Pel_N1)) * (bb * bb * Math.Sin(Lat_Pel_N1))) /
                    //                        ((aa * Math.Cos(Lat_Pel_N1)) * (aa * Math.Cos(Lat_Pel_N1)) + (bb * Math.Sin(Lat_Pel_N1)) * (bb * Math.Sin(Lat_Pel_N1))));


                    // RRR
                    //REarthP_Pel = 6371008.8;

                    // SSS1
                    REarthP_Pel = 6371008.771333;

                }

                // .........................................................................
                // LAT

                if ((fl_var2 == 0) && (fl2_var2 == 0))
                    Lat_Pel_N = Lat_Pel_N1 + (V_Pel * Math.Cos(Theta_Pel_tmp)) / REarthP_Pel;

                // Перешли через северный полюс
                else if ((fl_var2 == 1) && (fl2_var2 == 0) && (Math.Cos(Theta_Pel_tmp) >= 0))
                    Lat_Pel_N = Lat_Pel_N1 - (V_Pel * Math.Cos(Theta_Pel_tmp)) / REarthP_Pel;
                else if ((fl_var2 == 1) && (fl2_var2 == 0) && (Math.Cos(Theta_Pel_tmp) < 0))
                    Lat_Pel_N = Lat_Pel_N1 + (V_Pel * Math.Cos(Theta_Pel_tmp)) / REarthP_Pel;

                // Перешли через южный полюс 
                else if ((fl2_var2 == 1) && (fl_var2 == 0) && (Math.Cos(Theta_Pel_tmp) >= 0))
                    Lat_Pel_N = Lat_Pel_N1 + (V_Pel * Math.Cos(Theta_Pel_tmp)) / REarthP_Pel;
                else if ((fl2_var2 == 1) && (fl_var2 == 0) && (Math.Cos(Theta_Pel_tmp) < 0))
                    Lat_Pel_N = Lat_Pel_N1 - (V_Pel * Math.Cos(Theta_Pel_tmp)) / REarthP_Pel;


                // Перешли через северный полюс
                if (
                    (fl_var2 == 0) &&
                    (Lat_Pel_N > 0) &&
                    (Lat_Pel_N >= Math.PI / 2)
                   )
                {
                    Lat_Pel_N = Math.PI / 2;

                    fl_var2 = 1;
                    fl2_var2 = 0;
                    fl1_var2 = 0;

                }

                // Перешли через южный полюс
                if (
                    (fl2_var2 == 0) &&
                    (Lat_Pel_N < 0) &&
                    (Lat_Pel_N <= -Math.PI / 2)
                   )
                {
                    Lat_Pel_N = -Math.PI / 2;
                    //Theta_Pel = 0;

                    fl2_var2 = 1;
                    fl_var2 = 0;
                    fl1_var2 = 0;

                }

                // .........................................................................
                // LONG

                // Перешли через полюс
                if (
                    (fl1_var2 == 0) &&
                    ((fl_var2 == 1) || (fl2_var2 == 1))
                   )
                {
                    if (Long_Pel_N1 >= 0)
                        Long_Pel_N1 = Long_Pel_N1 + Math.PI;
                    else if (Long_Pel_N1 < 0)
                        Long_Pel_N1 = Long_Pel_N1 - Math.PI;

                    if (Long_Pel_N1 > Math.PI)
                        Long_Pel_N1 = -(2 * Math.PI - Long_Pel_N1);
                    else if (Long_Pel_N1 < -Math.PI)
                        Long_Pel_N1 = 2 * Math.PI + Long_Pel_N1;


                    fl1_var2 = 1;
                }


                // Обычный расчет
                Long_Pel_N = Long_Pel_N1 +
                            (V_Pel * Math.Sin(Theta_Pel_tmp)) / (REarthP_Pel * Math.Cos(Lat_Pel_N1));

                if (Long_Pel_N > Math.PI)
                    Long_Pel_N = -(2 * Math.PI - Long_Pel_N);
                else if (Long_Pel_N < -Math.PI)
                    Long_Pel_N = 2 * Math.PI + Long_Pel_N;
                // .........................................................................

                // .........................................................................
                XiG_Pel = REarthP_Pel * Math.Cos(Lat_Pel_N) * Math.Cos(Long_Pel_N);
                YiG_Pel = REarthP_Pel * Math.Cos(Lat_Pel_N) * Math.Sin(Long_Pel_N);
                ZiG_Pel = REarthP_Pel * Math.Sin(Lat_Pel_N);
                Ri_Pel = Math.Sqrt(XiG_Pel * XiG_Pel + YiG_Pel * YiG_Pel + ZiG_Pel * ZiG_Pel);
                // .........................................................................

                Lat_Pel_N1 = Lat_Pel_N;
                Long_Pel_N1 = Long_Pel_N;
                // .........................................................................


                // Массивы ***************************************************************
                // Занесение в массивы

                // LATi,LONGi (град)
                //mas_Pel[IndMas_Pel * 2] = (Lat_Pel_N * 180) / Math.PI;
                //mas_Pel[IndMas_Pel * 2 + 1] = (Long_Pel_N * 180) / Math.PI;

                // XYZ в опорной геоцентрической СК (м)
                //mas_Pel_XYZ[IndMas_Pel * 3] = XiG_Pel;
                //mas_Pel_XYZ[IndMas_Pel * 3 + 1] = YiG_Pel;
                //mas_Pel_XYZ[IndMas_Pel * 3 + 2] = ZiG_Pel;



                // LLPP1
                //ClassGeoCalculator.f_BLH_XYZ_84(
                //                                mas_Pel[IndMas_Pel * 2],
                //                                mas_Pel[IndMas_Pel * 2 + 1],
                //                                ref XiG_Pel,
                //                                ref YiG_Pel,
                //                                ref ZiG_Pel
                //                                );

                // *************************************************************** Массивы

                // Вывод для отладки ****************************************************

                //*(pmas_Otl_Pel+IndMas_Pel) = Di_Pel;

                // **************************************************** Вывод для отладки`

                // ***********************************************************************
                // Переход к следующей фиктивной точке

                IndFP_Pel += 1;
                IndMas_Pel += 1;
                // ***********************************************************************

            }; // WHILE

            // ***************************************************************** WHILE

            //XFN_Pel = XiG_Pel; // m
            //YFN_Pel = YiG_Pel;
            //ZFN_Pel = ZiG_Pel;
            LatN_Pel = (Lat_Pel_N * 180) / Math.PI;
            LongN_Pel = (Long_Pel_N * 180) / Math.PI;


        } // Функция f_Bearing (2-й вариант)
        //************************************************************************

        // ***********************************************************************
        // Определение координаты середины отрезка
        // ***********************************************************************

        public static void DefMiddle_Pel(
                            double X1,
                            double X2,
                            ref double XM

                                  )
        {

            // -------------------------------------------------------------------
            if (X1 == 0)
                XM = X2 / 2;
            // -------------------------------------------------------------------
            else if (X2 == 0)
                XM = X1 / 2;
            // -------------------------------------------------------------------
            else if ((X1 > 0) && (X2 > 0))
            {
                if (X2 > (X1))
                    XM = X1 + ((X2 - (X1)) / 2);
                else if (X1 > (X2))
                    XM = X2 + ((X1 - (X2)) / 2);
                else
                    XM = X1 / 2;
            }
            // -------------------------------------------------------------------
            else if ((X1 < 0) && (X2 < 0))
            {
                if (X2 < (X1))
                    XM = X1 - ((module(X2) - module(X1)) / 2);
                else if (X1 < (X2))
                    XM = X2 - ((module(X1) - module(X2)) / 2);
                else
                    XM = X1 / 2;
            }
            // -------------------------------------------------------------------
            else if ((X1 > 0) && (X2 < 0))
            {
                if (module(X1) > module(X2))
                    XM = (module(X2) + module(X1)) / 2;
                else if (module(X1) < module(X2))
                    XM = -(module(X2) + module(X1)) / 2;
                else
                    XM = 0;
            }
            // -------------------------------------------------------------------
            else
            {
                if (module(X2) > module(X1))
                    XM = (module(X2) + module(X1)) / 2;
                else if (module(X2) < module(X1))
                    XM = -(module(X2) + module(X1)) / 2;
                else
                    XM = 0;
            }
            // -------------------------------------------------------------------

        } // Функция DefMiddle_Pel
        // ***********************************************************************

        // ***********************************************************************
        // Расчет долготы
        // [0...180] к востоку от Гринвича
        // ]0...-180[ к западу от Гринвича
        // Широта -90(юг)...+90(север)
        // ***********************************************************************

        public static void Def_Longitude_180(
                            double X,
                            double Y,
                            double R,
                            double Lat, // rad
                            ref double Long

                                  )
        {
            double Long1;

            // -------------------------------------------------------------------
            if (module(R * Math.Cos(Lat)) > 1E-10)
                Long1 = Math.Asin(module(Y) / (R * Math.Cos(Lat)));
            else
                Long1 = Math.Asin(module(Y) / 1E-10);

            // -------------------------------------------------------------------
            if ((Y == 0) && (X > 0))
                Long = 0;
            // -------------------------------------------------------------------
            else if ((Y == 0) && (X < 0))
                Long = Math.PI;
            // -------------------------------------------------------------------
            else if ((Y > 0) && (X > 0))
                Long = Long1;
            // -------------------------------------------------------------------
            else if ((Y < 0) && (X > 0))
                Long = -Long1;
            // -------------------------------------------------------------------
            else if ((Y > 0) && (X < 0))
                Long = Math.PI - Long1;
            // ------------------------------------------------------------------
            else // X<0 Y<0
                Long = -(Math.PI - Long1);
            // -------------------------------------------------------------------

        } // Функция Def_Longitude_180
        // ***********************************************************************

        // ***********************************************************************
        // !!! НЕ используется
        // !!! Вариант1 (перед триангуляцией н. вызывать два раза функции пеленга)

        // Расчет координат точки пересечения двух пеленгов

        // Входные параметры: 
        // XP1_Tr,YP1_Tr,ZP1_Tr - Координаты пеленгатора1 в опорной геоцентрической СК(м)
        // XP2_Tr,YP2_Tr,ZP2_Tr - Координаты пеленгатора2 в опорной геоцентрической СК(м)
        // XFN1_Tr,YFN1_Tr,ZFN1_Tr -  Координаты N-й фиктивной точки в опорной 
        //                            геоцентрической СК для пеленгатора1(м)
        // XFN2_Tr,YFN2_Tr,ZFN2_Tr -  Координаты N-й фиктивной точки в опорной 
        //                            геоцентрической СК для пеленгатора2(м)

        //
        // Выходные параметры: 
        // XIRI_Tr,YIRI_Tr,ZIRI_Tr - Координаты искомого ИРИ в опорной геоцентрической СК(м)
        // LatIRI_Tr,LongIRI_Tr - Угловые координаты искомого ИРИ в опорной геоцентрической СК(град)
        // RIRI_Tr - Дальность до искомого ИРИ в км

        // Широта -90(юг)...+90(север)
        // Долгота -180(на запад от Гринвича)...+180(на восток от Гринвича)
        // Пеленг 0...360(по часовой стрелке)
        // ***********************************************************************

        public static void f_Triang

            (
            // Координаты пеленгатора в опорной геоцентрической СК(км)
                double XP1_Tr,
                double YP1_Tr,
                double ZP1_Tr,
                double XP2_Tr,
                double YP2_Tr,
                double ZP2_Tr,

                // Координаты N-й фиктивной точки в опорной геоцентрической СК(км)
                double XFN1_Tr,
                double YFN1_Tr,
                double ZFN1_Tr,
                double XFN2_Tr,
                double YFN2_Tr,
                double ZFN2_Tr,

                // Координаты искомого ИРИ в опорной геоцентрической СК(км)
                ref double XIRI_Tr,
                ref double YIRI_Tr,
                ref double ZIRI_Tr,

                // Угловые координаты искомого ИРИ в опорной геоцентрической СК(град)
                ref double RIRI_Tr,  // м
                ref double LatIRI_Tr,
                ref double LongIRI_Tr

            )
        {

            // ------------------------------------------------------------------------

            // Радиус Земли  в точке стояния пеленгатора с учетом текущей широты 
            double REarthP1_Tr = 0;
            double REarthP2_Tr = 0;

            // Расчетные коэффициенты
            double K1_Tr = 0;
            double K2_Tr = 0;
            double K3_Tr = 0;

            // Коэффициенты канонического уравнения для плоскости пеленгования ИРИ
            // i-ым пеленгатором 
            double A1_Tr = 0; // Для пеленгатора1
            double T1_Tr = 0;
            double C1_Tr = 0;
            double A2_Tr = 0; // Для пеленгатора2
            double T2_Tr = 0;
            double C2_Tr = 0;

            // Координаты искомого ИРИ в опорной геоцентрической СК
            double XIRI1_Tr = 0;
            double XIRI2_Tr = 0;
            double YIRI1_Tr = 0;
            double YIRI2_Tr = 0;
            double ZIRI1_Tr = 0;
            double ZIRI2_Tr = 0;

            // Координаты середины отрезка, соединяющего N-ые фиктивные точки
            double XM_Tr = 0;
            double YM_Tr = 0;
            double ZM_Tr = 0;

            // Расстояние до ПП
            double L1_Tr = 0;
            double L2_Tr = 0;

            // Координаты пеленгатора в опорной геоцентрической СК
            double XP1_Tr_tmp = 0;
            double YP1_Tr_tmp = 0;
            double ZP1_Tr_tmp = 0;
            double XP2_Tr_tmp = 0;
            double YP2_Tr_tmp = 0;
            double ZP2_Tr_tmp = 0;

            // Координаты N-й фиктивной точки в опорной геоцентрической СК
            double XFN1_Tr_tmp = 0;
            double YFN1_Tr_tmp = 0;
            double ZFN1_Tr_tmp = 0;
            double XFN2_Tr_tmp = 0;
            double YFN2_Tr_tmp = 0;
            double ZFN2_Tr_tmp = 0;

            // Радиус Земли (шар)
            double REarth_Pel = 6378245;

            // ------------------------------------------------------------------------


            // Initial conditions ****************************************************
            // Initial conditions
            // м 

            XP1_Tr_tmp = XP1_Tr;
            YP1_Tr_tmp = YP1_Tr;
            ZP1_Tr_tmp = ZP1_Tr;
            XP2_Tr_tmp = XP2_Tr;
            YP2_Tr_tmp = YP2_Tr;
            ZP2_Tr_tmp = ZP2_Tr;

            XFN1_Tr_tmp = XFN1_Tr;
            YFN1_Tr_tmp = YFN1_Tr;
            ZFN1_Tr_tmp = ZFN1_Tr;
            XFN2_Tr_tmp = XFN2_Tr;
            YFN2_Tr_tmp = YFN2_Tr;
            ZFN2_Tr_tmp = ZFN2_Tr;

            // **************************************************** Initial conditions

            // A,T,C *****************************************************************
            // Расчет коэффициентов пллоскости пеленгования

            A1_Tr = YP1_Tr_tmp * (ZFN1_Tr_tmp) - ZP1_Tr_tmp * (YFN1_Tr_tmp);
            T1_Tr = ZP1_Tr_tmp * (XFN1_Tr_tmp) - XP1_Tr_tmp * (ZFN1_Tr_tmp);
            C1_Tr = XP1_Tr_tmp * (YFN1_Tr_tmp) - YP1_Tr_tmp * (XFN1_Tr_tmp);

            A2_Tr = YP2_Tr_tmp * (ZFN2_Tr_tmp) - ZP2_Tr_tmp * (YFN2_Tr_tmp);
            T2_Tr = ZP2_Tr_tmp * (XFN2_Tr_tmp) - XP2_Tr_tmp * (ZFN2_Tr_tmp);
            C2_Tr = XP2_Tr_tmp * (YFN2_Tr_tmp) - YP2_Tr_tmp * (XFN2_Tr_tmp);

            // ***************************************************************** A,T,C

            // C1!=0 11111111111111111111111111111111111111111111111111111111111111111
            if (C1_Tr != 0)
            {
                // .......................................................................

                if (module(C1_Tr) < 1E-10)
                {
                    K1_Tr = -(A1_Tr / 1E-10);
                    K2_Tr = -(T1_Tr / 1E-10);
                }

                else
                {
                    K1_Tr = -(A1_Tr / C1_Tr);
                    K2_Tr = -(T1_Tr / C1_Tr);
                }

                if (module(T1_Tr * C2_Tr - C1_Tr * T2_Tr) < 1E-10)
                    K3_Tr = (C1_Tr * A2_Tr - A1_Tr * C2_Tr) / 1E-10;
                else
                    K3_Tr = (C1_Tr * A2_Tr - A1_Tr * C2_Tr) / (T1_Tr * C2_Tr - C1_Tr * T2_Tr);
                // .......................................................................
                // Координаты искомого ИРИ в опорной геоцентрической СК

                if (module(Math.Sqrt(1 + K3_Tr * K3_Tr + (K1_Tr + K2_Tr * K3_Tr) * (K1_Tr + K2_Tr * K3_Tr))) < 1E-10)
                {
                    XIRI1_Tr = REarth_Pel / 1E-10;
                    XIRI2_Tr = -REarth_Pel / 1E-10;
                }
                else
                {
                    XIRI1_Tr = REarth_Pel / Math.Sqrt(1 + K3_Tr * K3_Tr + (K1_Tr + K2_Tr * K3_Tr) * (K1_Tr + K2_Tr * K3_Tr));
                    XIRI2_Tr = -REarth_Pel / Math.Sqrt(1 + K3_Tr * K3_Tr + (K1_Tr + K2_Tr * K3_Tr) * (K1_Tr + K2_Tr * K3_Tr));
                }

                YIRI1_Tr = K3_Tr * XIRI1_Tr;
                YIRI2_Tr = K3_Tr * XIRI2_Tr;

                ZIRI1_Tr = K1_Tr * XIRI1_Tr + K2_Tr * YIRI1_Tr;
                ZIRI2_Tr = K1_Tr * XIRI2_Tr + K2_Tr * YIRI2_Tr;
                // .......................................................................

            } // C1!=0
            // 11111111111111111111111111111111111111111111111111111111111111111 C1!=0

            // C1=0 222222222222222222222222222222222222222222222222222222222222222222
            else
            {
                // .......................................................................
                if (module(T1_Tr) < 1E-10)
                    K3_Tr = -A1_Tr / 1E-10;
                else
                    K3_Tr = -A1_Tr / T1_Tr;

                if (module(C2_Tr) < 1E-10)
                    K2_Tr = -(A2_Tr + K3_Tr * T2_Tr) / 1E-10;
                else
                    K2_Tr = -(A2_Tr + K3_Tr * T2_Tr) / C2_Tr;

                // .......................................................................
                // Координаты искомого ИРИ в опорной геоцентрической СК

                if (module(Math.Sqrt(1 + K2_Tr * K2_Tr + K3_Tr * K3_Tr)) < 1E-10)
                {
                    XIRI1_Tr = REarth_Pel / 1E-10;
                    XIRI2_Tr = -REarth_Pel / 1E-10;
                }

                else
                {
                    XIRI1_Tr = REarth_Pel / Math.Sqrt(1 + K2_Tr * K2_Tr + K3_Tr * K3_Tr);
                    XIRI2_Tr = -REarth_Pel / Math.Sqrt(1 + K2_Tr * K2_Tr + K3_Tr * K3_Tr);
                }

                YIRI1_Tr = K3_Tr * XIRI1_Tr;
                YIRI2_Tr = K3_Tr * XIRI2_Tr;

                ZIRI1_Tr = K2_Tr * XIRI1_Tr;
                ZIRI2_Tr = K2_Tr * XIRI2_Tr;
                // .......................................................................

            } // C1=0
            // 222222222222222222222222222222222222222222222222222222222222222222 C1=0

            // XIRI_Tr,YIRI_Tr,ZIRI_Tr ***********************************************
            // Критерий выбора координат искомого ИРИ в опорной геоцентрической СК
            // (минимальное расстояние до ПП)

            // Координаты середины отрезка, соединяющего N-ые фиктивные точки
            DefMiddle_Pel(XFN1_Tr_tmp, XFN2_Tr_tmp, ref XM_Tr);
            DefMiddle_Pel(YFN1_Tr_tmp, YFN2_Tr_tmp, ref YM_Tr);
            DefMiddle_Pel(ZFN1_Tr_tmp, ZFN2_Tr_tmp, ref ZM_Tr);

            L1_Tr = Math.Sqrt((XM_Tr - XIRI1_Tr) * (XM_Tr - XIRI1_Tr) +
                              (YM_Tr - YIRI1_Tr) * (YM_Tr - YIRI1_Tr) +
                              (ZM_Tr - ZIRI1_Tr) * (ZM_Tr - ZIRI1_Tr));

            L2_Tr = Math.Sqrt((XM_Tr - XIRI2_Tr) * (XM_Tr - XIRI2_Tr) +
                              (YM_Tr - YIRI2_Tr) * (YM_Tr - YIRI2_Tr) +
                              (ZM_Tr - ZIRI2_Tr) * (ZM_Tr - ZIRI2_Tr));

            if (L1_Tr < L2_Tr)
            {
                XIRI_Tr = XIRI1_Tr;
                YIRI_Tr = YIRI1_Tr;
                ZIRI_Tr = ZIRI1_Tr;
            }
            else
            {
                XIRI_Tr = XIRI2_Tr;
                YIRI_Tr = YIRI2_Tr;
                ZIRI_Tr = ZIRI2_Tr;
            }
            // *********************************************** XIRI_Tr,YIRI_Tr,ZIRI_Tr

            // RIRI_Tr,LatIRI_Tr,LongIRI_Tr ******************************************
            // Угловые координаты искомого ИРИ в опорной геоцентрической СК

            // !!! Д.б. *pRIRI_Tr==REarth_Pel, т.е. r=Rз
            RIRI_Tr = Math.Sqrt(XIRI_Tr * XIRI_Tr + YIRI_Tr * YIRI_Tr + ZIRI_Tr * ZIRI_Tr);

            LatIRI_Tr = Math.Asin(ZIRI_Tr / RIRI_Tr);

            Def_Longitude_180(
                          XIRI_Tr,
                          YIRI_Tr,
                          RIRI_Tr,
                          LatIRI_Tr,
                          ref LongIRI_Tr
                         );

            // ****************************************** RIRI_Tr,LatIRI_Tr,LongIRI_Tr

            // *********************************************************************
            // Перевод в град

            // Угловые координаты искомого ИРИ в опорной геоцентрической СК
            LatIRI_Tr = (LatIRI_Tr * 180) / Math.PI;
            LongIRI_Tr = (LongIRI_Tr * 180) / Math.PI;
            // *********************************************************************


        } // Функция f_Triang
        //**************************************************************************

        // ***********************************************************************
        // !!! Вариант2 (перед триангуляцией НЕ нужно вызывать два раза функции пеленга)

        // Расчет координат точки пересечения двух пеленгов

        // Входные параметры: 

        // Mmax_Pel(м) - максимальная дальность отображения пеленга,
        // NumbFikt_Pel - количество фиктивных точек
        // Theta_Pel1, Theta_Pel2(град) - пеленг,
        // LatP_Pel1,LongP_Pel1,LatP_Pel2,LongP_Pel2 (град) - широта и долгота стояния пеленгатора 

        // fl_84_42=1-> долгота и широта в WGS84, =2 -> SK42
        // Smax- максимальная видимость пеленгатора

        // Выходные параметры: 
        // XIRI_Tr,YIRI_Tr,ZIRI_Tr - Координаты искомого ИРИ в опорной геоцентрической СК(м)
        // LatIRI_Tr,LongIRI_Tr - Угловые координаты искомого ИРИ в опорной геоцентрической СК(град)
        // RIRI_Tr - Дальность до искомого ИРИ в км

        // Возврат:
        // -2 -> недопустимые параметры
        // -1 -> несовместимые пеленги

        // Широта -90(юг)...+90(север)
        // Долгота -180(на запад от Гринвича)...+180(на восток от Гринвича)
        // Пеленг 0...360(по часовой стрелке)
        // ***********************************************************************

        public static int f_Triangulation

            (
            // Входные параметры:

                 // Max дальность отображения пеленга (m)
                 double Mmax_Pel1,
                 //double Mmax_Pel2,

                 // Количество фиктивных точек в плоскости пеленгования пеленгатора
                 uint NumbFikt_Pel1,

                 // Пеленг (град)
                 double Theta_Pel1,
                 double Theta_Pel2,

                 // Широта и долгота стояния пеленгатора (град)
                 double LatP_Pel1,
                 double LongP_Pel1,
                 double LatP_Pel2,
                 double LongP_Pel2,

                 int fl_84_42,
                 double Smax,

                // Выходные параметры:

                // Координаты искомого ИРИ в опорной геоцентрической СК(м)
                ref double XIRI_Tr,
                ref double YIRI_Tr,
                ref double ZIRI_Tr,

                // Угловые координаты искомого ИРИ в опорной геоцентрической СК(град)
                ref double RIRI_Tr,  // м
                ref double LatIRI_Tr,
                ref double LongIRI_Tr

            )
        {


            //double Mmax_Pel1 = 0;
            // ------------------------------------------------------------------------

            // Радиус Земли  в точке стояния пеленгатора с учетом текущей широты 
            //double REarthP1_Tr = 0;
            //double REarthP2_Tr = 0;

            // Расчетные коэффициенты
            double K1_Tr = 0;
            double K2_Tr = 0;
            double K3_Tr = 0;

            // Коэффициенты канонического уравнения для плоскости пеленгования ИРИ
            // i-ым пеленгатором 
            double A1_Tr = 0; // Для пеленгатора1
            double T1_Tr = 0;
            double C1_Tr = 0;
            double A2_Tr = 0; // Для пеленгатора2
            double T2_Tr = 0;
            double C2_Tr = 0;

            // Координаты искомого ИРИ в опорной геоцентрической СК
            double XIRI1_Tr = 0;
            double XIRI2_Tr = 0;
            double YIRI1_Tr = 0;
            double YIRI2_Tr = 0;
            double ZIRI1_Tr = 0;
            double ZIRI2_Tr = 0;

            // Координаты середины отрезка, соединяющего N-ые фиктивные точки
            double XM_Tr = 0;
            double YM_Tr = 0;
            double ZM_Tr = 0;

            // Расстояние до ПП
            double L1_Tr = 0;
            double L2_Tr = 0;

            // Координаты пеленгатора в опорной геоцентрической СК
            double XP1_Tr_tmp = 0;
            double YP1_Tr_tmp = 0;
            double ZP1_Tr_tmp = 0;
            double XP2_Tr_tmp = 0;
            double YP2_Tr_tmp = 0;
            double ZP2_Tr_tmp = 0;

            // Координаты N-й фиктивной точки в опорной геоцентрической СК
            double XFN1_Tr_tmp = 0;
            double YFN1_Tr_tmp = 0;
            double ZFN1_Tr_tmp = 0;
            double XFN2_Tr_tmp = 0;
            double YFN2_Tr_tmp = 0;
            double ZFN2_Tr_tmp = 0;

            // Радиус Земли (шар)
            double REarth_Pel = 0;

            // Координаты пеленгатора в опорной геоцентрической СК(км)
            double XP1_Tr = 0;
            double YP1_Tr = 0;
            double ZP1_Tr = 0;
            double XP2_Tr = 0;
            double YP2_Tr = 0;
            double ZP2_Tr = 0;
            // Координаты N-й фиктивной точки в опорной геоцентрической СК(км)
            double XFN1_Tr = 0;
            double YFN1_Tr = 0;
            double ZFN1_Tr = 0;
            double XFN2_Tr = 0;
            double YFN2_Tr = 0;
            double ZFN2_Tr = 0;

            double[] arr_Pel1 = new double[NumbFikt_Pel1 * 2];
            double[] arr_Pel2 = new double[NumbFikt_Pel1 * 2];

            double[] arr_Pel_XYZ1 = new double[NumbFikt_Pel1 * 3];
            double[] arr_Pel_XYZ2 = new double[NumbFikt_Pel1 * 3];



            double LatN_Pel1 = 0;
            double LongN_Pel1= 0;
            double LatN_Pel2 = 0;
            double LongN_Pel2 = 0;


            // WGS84
            double aa = 6378137;
            double bb = 6356752.3142;

            // ------------------------------------------------------------------------
            double lll = 0;
            lll = (LatP_Pel1 + LatP_Pel2) / 2;
            lll = (lll * Math.PI) / 180;

            // Радиус Земли (шар)
            //REarth_Pel = 6378245;

            //RRR
            // Средний радиус Земли (шар)
            if (fl_84_42 == 2) // Красовский
                //REarth_Pel = 6371220;

                // SSS1
                REarth_Pel = 6371117.673;

            else
                // SSS1
                //REarth_Pel = 6378136.3; // WGS84
                REarth_Pel = 6371008.771333; // WGS84

            // PPLL
            // Сжатие=(a-b)/a
            // 0.003352 -Красовский
            if (fl_84_42 == 2) // Красовский
            {
                REarth_Pel = REarth_Pel * (1 - 0.003352 * Math.Sin(lll) * Math.Sin(lll));

                // SSS1
                REarth_Pel = 6371117.673;

            }

            else
            {
                REarth_Pel = REarth_Pel * (1 - 0.00669438 * Math.Sin(lll) * Math.Sin(lll)); // WGS84
                //REarth_Pel = Math.Sqrt(((aa * aa * Math.Cos(lll)) * (aa * aa * Math.Cos(lll)) + (bb * bb * Math.Sin(lll)) * (bb * bb * Math.Sin(lll))) /
                //                        ((aa * Math.Cos(lll)) * (aa * Math.Cos(lll)) + (bb * Math.Sin(lll)) * (bb * Math.Sin(lll))));

                // RRR
                // SSS1
                //REarth_Pel = 6371008.8;
                REarth_Pel = 6371008.771333;

            }
            // ------------------------------------------------------------------------
            // Расчет траектории фиктивных точек по пеленгу1

            f_Bearing
             (
                 
                                   // Пеленг (град)
                                   Theta_Pel1,
                                 // Max дальность отображения пеленга (m)
                                   Mmax_Pel1,
                                 // Количество фиктивных точек в плоскости пеленгования пеленгатора
                                   NumbFikt_Pel1,

                                   // Широта и долгота стояния пеленгатора (град)
                                   LatP_Pel1,
                                   LongP_Pel1,

                                   fl_84_42,

                                   // Координаты пеленгатора в опорной геоцентрической СК
                                   ref XP1_Tr,
                                   ref YP1_Tr,
                                   ref ZP1_Tr,

                                   // Координаты N-й фиктивной точки в опорной геоцентрической СК
                                   ref XFN1_Tr,
                                   ref YFN1_Tr,
                                   ref ZFN1_Tr,

                                   // Координаты фиктивных точек
                                   ref arr_Pel1,
                                   ref arr_Pel_XYZ1


             );

            // ------------------------------------------------------------------------
            // Расчет траектории фиктивных точек по пеленгу2

            f_Bearing
             (
                 
                                   // Пеленг (град)
                                   Theta_Pel2,
                                 // Max дальность отображения пеленга (m)
                                   Mmax_Pel1,
                                 // Количество фиктивных точек в плоскости пеленгования пеленгатора
                                   NumbFikt_Pel1,

                                   // Широта и долгота стояния пеленгатора (град)
                                   LatP_Pel2,
                                   LongP_Pel2,

                                   fl_84_42,

                                   // Координаты пеленгатора в опорной геоцентрической СК
                                   ref XP2_Tr,
                                   ref YP2_Tr,
                                   ref ZP2_Tr,

                                   // Координаты N-й фиктивной точки в опорной геоцентрической СК
                                   ref XFN2_Tr,
                                   ref YFN2_Tr,
                                   ref ZFN2_Tr,

                                   // Координаты фиктивных точек
                                   ref arr_Pel2,
                                   ref arr_Pel_XYZ2
                 

             );
            // ------------------------------------------------------------------------



            // ????????????????????????????????????????????????????????????????????

/*
            int vvv1 = 0;
            int vvv2 = 0;
            double ltv1 = 0;
            double ltv2 = 0;
            double lnv1 = 0;
            double lnv2 = 0;
            double sv = 0;
            int flv = 0;

            // FOR1
            vvv1 = 0;
            for (int vvv = 0; vvv < NumbFikt_Pel1; vvv++)
            {
                ltv1 = arr_Pel1[vvv1];
                vvv1 += 1;
                lnv1 = arr_Pel1[vvv1];
                vvv1 += 1;

                //FOR2
                vvv2 = 0;
                for (int vvv_1 = 0; vvv_1 < NumbFikt_Pel1; vvv_1++)
                {
                    ltv2 = arr_Pel2[vvv2];
                    vvv2 += 1;
                    lnv2 = arr_Pel2[vvv2];
                    vvv2 += 1;

                    sv = f_D_2Points
                   (
                     // Широта и долгота стояния точек (град)
                     ltv1,
                     lnv1,
                     ltv2,
                     lnv2,
                     1
                   );

                if (sv < 10000)

                {
                    flv = 1;

                    XFN2_Tr = arr_Pel_XYZ2[vvv_1 * 3];
                    YFN2_Tr = arr_Pel_XYZ2[vvv_1 * 3 + 1];
                    ZFN2_Tr = arr_Pel_XYZ2[vvv_1 * 3 + 2];

                    vvv_1= (int)(NumbFikt_Pel1+10);
                }

                } // FOR2

                if(flv==1)
                {
                    XFN1_Tr = arr_Pel_XYZ1[vvv * 3];
                    YFN1_Tr = arr_Pel_XYZ1[vvv * 3 + 1];
                    ZFN1_Tr = arr_Pel_XYZ1[vvv * 3 + 2];

                    vvv = (int)(NumbFikt_Pel1 + 10);

                }

            } // FOR1
*/
            // ????????????????????????????????????????????????????????????????????

            // SOVMEST_PEL ***********************************************************
            // Проверка совместимости пеленгов

            // ------------------------------------------------------------------------

            int fls = 0;

            fls = f_Bearing_Compatibility

            (
                // Входные параметры:
                // Пеленг (град)
                 Theta_Pel1,
                 Theta_Pel2,
                // Широта и долгота стояния пеленгатора (град)
                 LatP_Pel1,
                 LongP_Pel1,
                 LatP_Pel2,
                 LongP_Pel2,
                 Smax,
                 fl_84_42
            );
            // ------------------------------------------------------------------------
            if (fls == -1)
                return -1;
            if (fls == -2)
                return -2;
            // ------------------------------------------------------------------------
            // Решение на середине отрезка, соединяющего станции

            if (fls == 2)
            {
                // Угловые координаты искомого ИРИ в опорной геоцентрической СК
                LatIRI_Tr = (LatP_Pel1 + LatP_Pel2) / 2;
                LongIRI_Tr = (LongP_Pel1 * LongP_Pel2) / 2;
                return 0;

            } // fls=2
            // ------------------------------------------------------------------------


            // *********************************************************** SOVMEST_PEL

            // Initial conditions ****************************************************
            // Initial conditions
            // м 

            XP1_Tr_tmp = XP1_Tr;
            YP1_Tr_tmp = YP1_Tr;
            ZP1_Tr_tmp = ZP1_Tr;
            XP2_Tr_tmp = XP2_Tr;
            YP2_Tr_tmp = YP2_Tr;
            ZP2_Tr_tmp = ZP2_Tr;

            XFN1_Tr_tmp = XFN1_Tr;
            YFN1_Tr_tmp = YFN1_Tr;
            ZFN1_Tr_tmp = ZFN1_Tr;
            XFN2_Tr_tmp = XFN2_Tr;
            YFN2_Tr_tmp = YFN2_Tr;
            ZFN2_Tr_tmp = ZFN2_Tr;

            // **************************************************** Initial conditions

            // A,T,C *****************************************************************
            // Расчет коэффициентов пллоскости пеленгования

            A1_Tr = YP1_Tr_tmp * (ZFN1_Tr_tmp) - ZP1_Tr_tmp * (YFN1_Tr_tmp);
            T1_Tr = ZP1_Tr_tmp * (XFN1_Tr_tmp) - XP1_Tr_tmp * (ZFN1_Tr_tmp);
            C1_Tr = XP1_Tr_tmp * (YFN1_Tr_tmp) - YP1_Tr_tmp * (XFN1_Tr_tmp);

            A2_Tr = YP2_Tr_tmp * (ZFN2_Tr_tmp) - ZP2_Tr_tmp * (YFN2_Tr_tmp);
            T2_Tr = ZP2_Tr_tmp * (XFN2_Tr_tmp) - XP2_Tr_tmp * (ZFN2_Tr_tmp);
            C2_Tr = XP2_Tr_tmp * (YFN2_Tr_tmp) - YP2_Tr_tmp * (XFN2_Tr_tmp);

            // ***************************************************************** A,T,C

            // C1!=0 11111111111111111111111111111111111111111111111111111111111111111
            if (C1_Tr != 0)
            {
                // .......................................................................

                if (module(C1_Tr) < 1E-10)
                {
                    K1_Tr = -(A1_Tr / 1E-10);
                    K2_Tr = -(T1_Tr / 1E-10);
                }

                else
                {
                    K1_Tr = -(A1_Tr / C1_Tr);
                    K2_Tr = -(T1_Tr / C1_Tr);
                }

                if (module(T1_Tr * C2_Tr - C1_Tr * T2_Tr) < 1E-10)
                    K3_Tr = (C1_Tr * A2_Tr - A1_Tr * C2_Tr) / 1E-10;
                else
                    K3_Tr = (C1_Tr * A2_Tr - A1_Tr * C2_Tr) / (T1_Tr * C2_Tr - C1_Tr * T2_Tr);
                // .......................................................................
                // Координаты искомого ИРИ в опорной геоцентрической СК

                if (module(Math.Sqrt(1 + K3_Tr * K3_Tr + (K1_Tr + K2_Tr * K3_Tr) * (K1_Tr + K2_Tr * K3_Tr))) < 1E-10)
                {
                    XIRI1_Tr = REarth_Pel / 1E-10;
                    XIRI2_Tr = -REarth_Pel / 1E-10;
                }
                else
                {
                    XIRI1_Tr = REarth_Pel / Math.Sqrt(1 + K3_Tr * K3_Tr + (K1_Tr + K2_Tr * K3_Tr) * (K1_Tr + K2_Tr * K3_Tr));
                    XIRI2_Tr = -REarth_Pel / Math.Sqrt(1 + K3_Tr * K3_Tr + (K1_Tr + K2_Tr * K3_Tr) * (K1_Tr + K2_Tr * K3_Tr));
                }

                YIRI1_Tr = K3_Tr * XIRI1_Tr;
                YIRI2_Tr = K3_Tr * XIRI2_Tr;

                ZIRI1_Tr = K1_Tr * XIRI1_Tr + K2_Tr * YIRI1_Tr;
                ZIRI2_Tr = K1_Tr * XIRI2_Tr + K2_Tr * YIRI2_Tr;
                // .......................................................................

            } // C1!=0
            // 11111111111111111111111111111111111111111111111111111111111111111 C1!=0

            // C1=0 222222222222222222222222222222222222222222222222222222222222222222
            else
            {
                // .......................................................................
                if (module(T1_Tr) < 1E-10)
                    K3_Tr = -A1_Tr / 1E-10;
                else
                    K3_Tr = -A1_Tr / T1_Tr;

                if (module(C2_Tr) < 1E-10)
                    K2_Tr = -(A2_Tr + K3_Tr * T2_Tr) / 1E-10;
                else
                    K2_Tr = -(A2_Tr + K3_Tr * T2_Tr) / C2_Tr;

                // .......................................................................
                // Координаты искомого ИРИ в опорной геоцентрической СК

                if (module(Math.Sqrt(1 + K2_Tr * K2_Tr + K3_Tr * K3_Tr)) < 1E-10)
                {
                    XIRI1_Tr = REarth_Pel / 1E-10;
                    XIRI2_Tr = -REarth_Pel / 1E-10;
                }

                else
                {
                    XIRI1_Tr = REarth_Pel / Math.Sqrt(1 + K2_Tr * K2_Tr + K3_Tr * K3_Tr);
                    XIRI2_Tr = -REarth_Pel / Math.Sqrt(1 + K2_Tr * K2_Tr + K3_Tr * K3_Tr);
                }

                YIRI1_Tr = K3_Tr * XIRI1_Tr;
                YIRI2_Tr = K3_Tr * XIRI2_Tr;

                ZIRI1_Tr = K2_Tr * XIRI1_Tr;
                ZIRI2_Tr = K2_Tr * XIRI2_Tr;
                // .......................................................................

            } // C1=0
            // 222222222222222222222222222222222222222222222222222222222222222222 C1=0

            // XIRI_Tr,YIRI_Tr,ZIRI_Tr ***********************************************
            // Критерий выбора координат искомого ИРИ в опорной геоцентрической СК
            // (минимальное расстояние до ПП)

            // Координаты середины отрезка, соединяющего N-ые фиктивные точки
            DefMiddle_Pel(XFN1_Tr_tmp, XFN2_Tr_tmp, ref XM_Tr);
            DefMiddle_Pel(YFN1_Tr_tmp, YFN2_Tr_tmp, ref YM_Tr);
            DefMiddle_Pel(ZFN1_Tr_tmp, ZFN2_Tr_tmp, ref ZM_Tr);

            L1_Tr = Math.Sqrt((XM_Tr - XIRI1_Tr) * (XM_Tr - XIRI1_Tr) +
                              (YM_Tr - YIRI1_Tr) * (YM_Tr - YIRI1_Tr) +
                              (ZM_Tr - ZIRI1_Tr) * (ZM_Tr - ZIRI1_Tr));

            L2_Tr = Math.Sqrt((XM_Tr - XIRI2_Tr) * (XM_Tr - XIRI2_Tr) +
                              (YM_Tr - YIRI2_Tr) * (YM_Tr - YIRI2_Tr) +
                              (ZM_Tr - ZIRI2_Tr) * (ZM_Tr - ZIRI2_Tr));

            if (L1_Tr < L2_Tr)
            {
                XIRI_Tr = XIRI1_Tr;
                YIRI_Tr = YIRI1_Tr;
                ZIRI_Tr = ZIRI1_Tr;
            }
            else
            {
                XIRI_Tr = XIRI2_Tr;
                YIRI_Tr = YIRI2_Tr;
                ZIRI_Tr = ZIRI2_Tr;
            }
            // *********************************************** XIRI_Tr,YIRI_Tr,ZIRI_Tr

            // RIRI_Tr,LatIRI_Tr,LongIRI_Tr ******************************************
            // Угловые координаты искомого ИРИ в опорной геоцентрической СК

            // !!! Д.б. *pRIRI_Tr==REarth_Pel, т.е. r=Rз
            RIRI_Tr = Math.Sqrt(XIRI_Tr * XIRI_Tr + YIRI_Tr * YIRI_Tr + ZIRI_Tr * ZIRI_Tr);

            LatIRI_Tr = Math.Asin(ZIRI_Tr / RIRI_Tr);

            Def_Longitude_180(
                          XIRI_Tr,
                          YIRI_Tr,
                          RIRI_Tr,
                          LatIRI_Tr,
                          ref LongIRI_Tr
                         );


            // Перевод в град
            // Угловые координаты искомого ИРИ в опорной геоцентрической СК
            LatIRI_Tr = (LatIRI_Tr * 180) / Math.PI;
            LongIRI_Tr = (LongIRI_Tr * 180) / Math.PI;

                        // LLPP1
                        ClassGeoCalculator.f_XYZ_BLH_84(
                                      XIRI_Tr,
                                      YIRI_Tr,
                                      ZIRI_Tr,
                                      ref LatIRI_Tr,
                                      ref LongIRI_Tr
                                                 );



            // ****************************************** RIRI_Tr,LatIRI_Tr,LongIRI_Tr


            // PPLL
            //double aa = 6378137;
            //double bb = 6356000;
            //double bb = 6356752.3142;
            //LatIRI_Tr = Math.Atan(((aa * aa) / (bb * bb)) * Math.Tan(LatIRI_Tr));
            //LatIRI_Tr = Math.Atan(((bb * bb) / (aa * aa)) * Math.Tan(LatIRI_Tr));

            // Уточнение координат ********************************************************************
            // Уточнение координат 

            double sss1 = 0;
            double sss2 = 0;

            // 
            sss1 = f_D_2Points
           (
             // Широта и долгота стояния точек (град)
             LatP_Pel1,
             LongP_Pel1,
             LatIRI_Tr,
             LongIRI_Tr,
             1
           );

            sss2 = f_D_2Points
           (
             // Широта и долгота стояния точек (град)
             LatP_Pel2,
             LongP_Pel2,
             LatIRI_Tr,
             LongIRI_Tr,
             1
           );

            // ------------------------------------------------------------------------
            // Расчет траектории фиктивных точек по пеленгу1

            f_Bearing
             (

                                   // Пеленг (град)
                                   Theta_Pel1,
                                   // Max дальность отображения пеленга (m)
                                   sss1,
                                   // Количество фиктивных точек в плоскости пеленгования пеленгатора
                                   NumbFikt_Pel1,

                                   // Широта и долгота стояния пеленгатора (град)
                                   LatP_Pel1,
                                   LongP_Pel1,

                                   fl_84_42,

                                   // Координаты пеленгатора в опорной геоцентрической СК
                                   ref XP1_Tr,
                                   ref YP1_Tr,
                                   ref ZP1_Tr,

                                   // Координаты N-й фиктивной точки в опорной геоцентрической СК
                                   ref XFN1_Tr,
                                   ref YFN1_Tr,
                                   ref ZFN1_Tr,

                                   // Координаты фиктивных точек
                                   ref arr_Pel1,
                                   ref arr_Pel_XYZ1


             );

            // ------------------------------------------------------------------------
            // Расчет траектории фиктивных точек по пеленгу2

            f_Bearing
             (

                                   // Пеленг (град)
                                   Theta_Pel2,
                                   // Max дальность отображения пеленга (m)
                                   sss2,
                                   // Количество фиктивных точек в плоскости пеленгования пеленгатора
                                   NumbFikt_Pel1,

                                   // Широта и долгота стояния пеленгатора (град)
                                   LatP_Pel2,
                                   LongP_Pel2,

                                   fl_84_42,

                                   // Координаты пеленгатора в опорной геоцентрической СК
                                   ref XP2_Tr,
                                   ref YP2_Tr,
                                   ref ZP2_Tr,

                                   // Координаты N-й фиктивной точки в опорной геоцентрической СК
                                   ref XFN2_Tr,
                                   ref YFN2_Tr,
                                   ref ZFN2_Tr,

                                   // Координаты фиктивных точек
                                   ref arr_Pel2,
                                   ref arr_Pel_XYZ2


             );
            // ------------------------------------------------------------------------


            // Initial conditions ****************************************************
            // Initial conditions
            // м 

            XP1_Tr_tmp = XP1_Tr;
            YP1_Tr_tmp = YP1_Tr;
            ZP1_Tr_tmp = ZP1_Tr;
            XP2_Tr_tmp = XP2_Tr;
            YP2_Tr_tmp = YP2_Tr;
            ZP2_Tr_tmp = ZP2_Tr;

            XFN1_Tr_tmp = XFN1_Tr;
            YFN1_Tr_tmp = YFN1_Tr;
            ZFN1_Tr_tmp = ZFN1_Tr;
            XFN2_Tr_tmp = XFN2_Tr;
            YFN2_Tr_tmp = YFN2_Tr;
            ZFN2_Tr_tmp = ZFN2_Tr;

            // **************************************************** Initial conditions

            // A,T,C *****************************************************************
            // Расчет коэффициентов пллоскости пеленгования

            A1_Tr = YP1_Tr_tmp * (ZFN1_Tr_tmp) - ZP1_Tr_tmp * (YFN1_Tr_tmp);
            T1_Tr = ZP1_Tr_tmp * (XFN1_Tr_tmp) - XP1_Tr_tmp * (ZFN1_Tr_tmp);
            C1_Tr = XP1_Tr_tmp * (YFN1_Tr_tmp) - YP1_Tr_tmp * (XFN1_Tr_tmp);

            A2_Tr = YP2_Tr_tmp * (ZFN2_Tr_tmp) - ZP2_Tr_tmp * (YFN2_Tr_tmp);
            T2_Tr = ZP2_Tr_tmp * (XFN2_Tr_tmp) - XP2_Tr_tmp * (ZFN2_Tr_tmp);
            C2_Tr = XP2_Tr_tmp * (YFN2_Tr_tmp) - YP2_Tr_tmp * (XFN2_Tr_tmp);

            // ***************************************************************** A,T,C

            // C1!=0 11111111111111111111111111111111111111111111111111111111111111111
            if (C1_Tr != 0)
            {
                // .......................................................................

                if (module(C1_Tr) < 1E-10)
                {
                    K1_Tr = -(A1_Tr / 1E-10);
                    K2_Tr = -(T1_Tr / 1E-10);
                }

                else
                {
                    K1_Tr = -(A1_Tr / C1_Tr);
                    K2_Tr = -(T1_Tr / C1_Tr);
                }

                if (module(T1_Tr * C2_Tr - C1_Tr * T2_Tr) < 1E-10)
                    K3_Tr = (C1_Tr * A2_Tr - A1_Tr * C2_Tr) / 1E-10;
                else
                    K3_Tr = (C1_Tr * A2_Tr - A1_Tr * C2_Tr) / (T1_Tr * C2_Tr - C1_Tr * T2_Tr);
                // .......................................................................
                // Координаты искомого ИРИ в опорной геоцентрической СК

                if (module(Math.Sqrt(1 + K3_Tr * K3_Tr + (K1_Tr + K2_Tr * K3_Tr) * (K1_Tr + K2_Tr * K3_Tr))) < 1E-10)
                {
                    XIRI1_Tr = REarth_Pel / 1E-10;
                    XIRI2_Tr = -REarth_Pel / 1E-10;
                }
                else
                {
                    XIRI1_Tr = REarth_Pel / Math.Sqrt(1 + K3_Tr * K3_Tr + (K1_Tr + K2_Tr * K3_Tr) * (K1_Tr + K2_Tr * K3_Tr));
                    XIRI2_Tr = -REarth_Pel / Math.Sqrt(1 + K3_Tr * K3_Tr + (K1_Tr + K2_Tr * K3_Tr) * (K1_Tr + K2_Tr * K3_Tr));
                }

                YIRI1_Tr = K3_Tr * XIRI1_Tr;
                YIRI2_Tr = K3_Tr * XIRI2_Tr;

                ZIRI1_Tr = K1_Tr * XIRI1_Tr + K2_Tr * YIRI1_Tr;
                ZIRI2_Tr = K1_Tr * XIRI2_Tr + K2_Tr * YIRI2_Tr;
                // .......................................................................

            } // C1!=0
            // 11111111111111111111111111111111111111111111111111111111111111111 C1!=0

            // C1=0 222222222222222222222222222222222222222222222222222222222222222222
            else
            {
                // .......................................................................
                if (module(T1_Tr) < 1E-10)
                    K3_Tr = -A1_Tr / 1E-10;
                else
                    K3_Tr = -A1_Tr / T1_Tr;

                if (module(C2_Tr) < 1E-10)
                    K2_Tr = -(A2_Tr + K3_Tr * T2_Tr) / 1E-10;
                else
                    K2_Tr = -(A2_Tr + K3_Tr * T2_Tr) / C2_Tr;

                // .......................................................................
                // Координаты искомого ИРИ в опорной геоцентрической СК

                if (module(Math.Sqrt(1 + K2_Tr * K2_Tr + K3_Tr * K3_Tr)) < 1E-10)
                {
                    XIRI1_Tr = REarth_Pel / 1E-10;
                    XIRI2_Tr = -REarth_Pel / 1E-10;
                }

                else
                {
                    XIRI1_Tr = REarth_Pel / Math.Sqrt(1 + K2_Tr * K2_Tr + K3_Tr * K3_Tr);
                    XIRI2_Tr = -REarth_Pel / Math.Sqrt(1 + K2_Tr * K2_Tr + K3_Tr * K3_Tr);
                }

                YIRI1_Tr = K3_Tr * XIRI1_Tr;
                YIRI2_Tr = K3_Tr * XIRI2_Tr;

                ZIRI1_Tr = K2_Tr * XIRI1_Tr;
                ZIRI2_Tr = K2_Tr * XIRI2_Tr;
                // .......................................................................

            } // C1=0
            // 222222222222222222222222222222222222222222222222222222222222222222 C1=0

            // XIRI_Tr,YIRI_Tr,ZIRI_Tr ***********************************************
            // Критерий выбора координат искомого ИРИ в опорной геоцентрической СК
            // (минимальное расстояние до ПП)

            // Координаты середины отрезка, соединяющего N-ые фиктивные точки
            DefMiddle_Pel(XFN1_Tr_tmp, XFN2_Tr_tmp, ref XM_Tr);
            DefMiddle_Pel(YFN1_Tr_tmp, YFN2_Tr_tmp, ref YM_Tr);
            DefMiddle_Pel(ZFN1_Tr_tmp, ZFN2_Tr_tmp, ref ZM_Tr);

            L1_Tr = Math.Sqrt((XM_Tr - XIRI1_Tr) * (XM_Tr - XIRI1_Tr) +
                              (YM_Tr - YIRI1_Tr) * (YM_Tr - YIRI1_Tr) +
                              (ZM_Tr - ZIRI1_Tr) * (ZM_Tr - ZIRI1_Tr));

            L2_Tr = Math.Sqrt((XM_Tr - XIRI2_Tr) * (XM_Tr - XIRI2_Tr) +
                              (YM_Tr - YIRI2_Tr) * (YM_Tr - YIRI2_Tr) +
                              (ZM_Tr - ZIRI2_Tr) * (ZM_Tr - ZIRI2_Tr));

            if (L1_Tr < L2_Tr)
            {
                XIRI_Tr = XIRI1_Tr;
                YIRI_Tr = YIRI1_Tr;
                ZIRI_Tr = ZIRI1_Tr;
            }
            else
            {
                XIRI_Tr = XIRI2_Tr;
                YIRI_Tr = YIRI2_Tr;
                ZIRI_Tr = ZIRI2_Tr;
            }
            // *********************************************** XIRI_Tr,YIRI_Tr,ZIRI_Tr

            // RIRI_Tr,LatIRI_Tr,LongIRI_Tr ******************************************
            // Угловые координаты искомого ИРИ в опорной геоцентрической СК

            // !!! Д.б. *pRIRI_Tr==REarth_Pel, т.е. r=Rз
            RIRI_Tr = Math.Sqrt(XIRI_Tr * XIRI_Tr + YIRI_Tr * YIRI_Tr + ZIRI_Tr * ZIRI_Tr);

            LatIRI_Tr = Math.Asin(ZIRI_Tr / RIRI_Tr);

            Def_Longitude_180(
                          XIRI_Tr,
                          YIRI_Tr,
                          RIRI_Tr,
                          LatIRI_Tr,
                          ref LongIRI_Tr
                         );


            // Перевод в град
            // Угловые координаты искомого ИРИ в опорной геоцентрической СК
            LatIRI_Tr = (LatIRI_Tr * 180) / Math.PI;
            LongIRI_Tr = (LongIRI_Tr * 180) / Math.PI;

            // LLPP1
            ClassGeoCalculator.f_XYZ_BLH_84(
                          XIRI_Tr,
                          YIRI_Tr,
                          ZIRI_Tr,
                          ref LatIRI_Tr,
                          ref LongIRI_Tr
                                     );
            // ********************************************************** Уточнение координат



            return 0;

        } // Функция f_Triangulation (2-й вариант)
        //**************************************************************************

        // ***********************************************************************
        // !!! Вариант2_1 (перед триангуляцией НЕ нужно вызывать два раза функции пеленга)
        // PPLL  (уравнение эллипсоида)

        // Расчет координат точки пересечения двух пеленгов

        // Входные параметры: 

        // Mmax_Pel(м) - максимальная дальность отображения пеленга,
        // NumbFikt_Pel - количество фиктивных точек
        // Theta_Pel1, Theta_Pel2(град) - пеленг,
        // LatP_Pel1,LongP_Pel1,LatP_Pel2,LongP_Pel2 (град) - широта и долгота стояния пеленгатора 

        // fl_84_42=1-> долгота и широта в WGS84, =2 -> SK42
        // Smax- максимальная видимость пеленгатора

        // Выходные параметры: 
        // XIRI_Tr,YIRI_Tr,ZIRI_Tr - Координаты искомого ИРИ в опорной геоцентрической СК(м)
        // LatIRI_Tr,LongIRI_Tr - Угловые координаты искомого ИРИ в опорной геоцентрической СК(град)
        // RIRI_Tr - Дальность до искомого ИРИ в км

        // Возврат:
        // -2 -> недопустимые параметры
        // -1 -> несовместимые пеленги

        // Широта -90(юг)...+90(север)
        // Долгота -180(на запад от Гринвича)...+180(на восток от Гринвича)
        // Пеленг 0...360(по часовой стрелке)
        // ***********************************************************************

        public static int f_Triangulation1

            (
                 // Входные параметры:

                 // Max дальность отображения пеленга (m)
                 double Mmax_Pel1,
                 // Количество фиктивных точек в плоскости пеленгования пеленгатора
                 uint NumbFikt_Pel1,

                 // Пеленг (град)
                 double Theta_Pel1,
                 double Theta_Pel2,

                 // Широта и долгота стояния пеленгатора (град)
                 double LatP_Pel1,
                 double LongP_Pel1,
                 double LatP_Pel2,
                 double LongP_Pel2,

                 int fl_84_42,
                 double Smax,

                // Выходные параметры:

                // Координаты искомого ИРИ в опорной геоцентрической СК(м)
                ref double XIRI_Tr,
                ref double YIRI_Tr,
                ref double ZIRI_Tr,

                // Угловые координаты искомого ИРИ в опорной геоцентрической СК(град)
                ref double RIRI_Tr,  // м
                ref double LatIRI_Tr,
                ref double LongIRI_Tr

            )
        {


            //double Mmax_Pel1 = 0;
            // ------------------------------------------------------------------------

            // Радиус Земли  в точке стояния пеленгатора с учетом текущей широты 
            //double REarthP1_Tr = 0;
            //double REarthP2_Tr = 0;

            // Расчетные коэффициенты
            double K1_Tr = 0;
            double K2_Tr = 0;
            double K3_Tr = 0;

            // Коэффициенты канонического уравнения для плоскости пеленгования ИРИ
            // i-ым пеленгатором 
            double A1_Tr = 0; // Для пеленгатора1
            double T1_Tr = 0;
            double C1_Tr = 0;
            double A2_Tr = 0; // Для пеленгатора2
            double T2_Tr = 0;
            double C2_Tr = 0;

            // PPLL
            double TMP_X = 0;

            // Координаты искомого ИРИ в опорной геоцентрической СК
            double XIRI1_Tr = 0;
            double XIRI2_Tr = 0;
            double YIRI1_Tr = 0;
            double YIRI2_Tr = 0;
            double ZIRI1_Tr = 0;
            double ZIRI2_Tr = 0;

            // Координаты середины отрезка, соединяющего N-ые фиктивные точки
            double XM_Tr = 0;
            double YM_Tr = 0;
            double ZM_Tr = 0;

            // Расстояние до ПП
            double L1_Tr = 0;
            double L2_Tr = 0;

            // Координаты пеленгатора в опорной геоцентрической СК
            double XP1_Tr_tmp = 0;
            double YP1_Tr_tmp = 0;
            double ZP1_Tr_tmp = 0;
            double XP2_Tr_tmp = 0;
            double YP2_Tr_tmp = 0;
            double ZP2_Tr_tmp = 0;

            // Координаты N-й фиктивной точки в опорной геоцентрической СК
            double XFN1_Tr_tmp = 0;
            double YFN1_Tr_tmp = 0;
            double ZFN1_Tr_tmp = 0;
            double XFN2_Tr_tmp = 0;
            double YFN2_Tr_tmp = 0;
            double ZFN2_Tr_tmp = 0;

            // Радиус Земли (шар)
            double REarth_Pel = 0;

            // Координаты пеленгатора в опорной геоцентрической СК(км)
            double XP1_Tr = 0;
            double YP1_Tr = 0;
            double ZP1_Tr = 0;
            double XP2_Tr = 0;
            double YP2_Tr = 0;
            double ZP2_Tr = 0;
            // Координаты N-й фиктивной точки в опорной геоцентрической СК(км)
            double XFN1_Tr = 0;
            double YFN1_Tr = 0;
            double ZFN1_Tr = 0;
            double XFN2_Tr = 0;
            double YFN2_Tr = 0;
            double ZFN2_Tr = 0;

            double[] arr_Pel1 = new double[NumbFikt_Pel1 * 2];
            double[] arr_Pel2 = new double[NumbFikt_Pel1 * 2];

            double[] arr_Pel_XYZ1 = new double[NumbFikt_Pel1 * 3];
            double[] arr_Pel_XYZ2 = new double[NumbFikt_Pel1 * 3];



            double LatN_Pel1 = 0;
            double LongN_Pel1 = 0;
            double LatN_Pel2 = 0;
            double LongN_Pel2 = 0;

            // WGS84
            double aa = 6378137;
            double bb = 6356752.3142;

            // ------------------------------------------------------------------------
            double lll = 0;
            lll = (LatP_Pel1 + LatP_Pel2) / 2;
            lll = (lll * Math.PI) / 180;

            // Радиус Земли (шар)
            //REarth_Pel = 6378245;

            //RRR
            // Средний радиус Земли (шар)
            if (fl_84_42 == 2) // Красовский
                REarth_Pel = 6371220;
            else
                REarth_Pel = 6378136.3; // WGS84


            // PPLL
            // Сжатие=(a-b)/a
            // 0.003352 -Красовский
            if (fl_84_42 == 2) // Красовский
                REarth_Pel = REarth_Pel * (1 - 0.003352 * Math.Sin(lll) * Math.Sin(lll));
            else
            {
                REarth_Pel = REarth_Pel * (1 - 0.00669438 * Math.Sin(lll) * Math.Sin(lll)); // WGS84
                //REarth_Pel = Math.Sqrt(((aa * aa * Math.Cos(lll)) * (aa * aa * Math.Cos(lll)) + (bb * bb * Math.Sin(lll)) * (bb * bb * Math.Sin(lll))) /
                //                        ((aa * Math.Cos(lll)) * (aa * Math.Cos(lll)) + (bb * Math.Sin(lll)) * (bb * Math.Sin(lll))));
                //REarth_Pel = 6371008.8;

                // RRR
                //REarth_Pel = 6371008.8;

            }


            // ------------------------------------------------------------------------
            // Расчет траектории фиктивных точек по пеленгу1

            f_Bearing
             (

                                   // Пеленг (град)
                                   Theta_Pel1,
                                   // Max дальность отображения пеленга (m)
                                   Mmax_Pel1,
                                   // Количество фиктивных точек в плоскости пеленгования пеленгатора
                                   NumbFikt_Pel1,

                                   // Широта и долгота стояния пеленгатора (град)
                                   LatP_Pel1,
                                   LongP_Pel1,

                                   fl_84_42,

                                   // Координаты пеленгатора в опорной геоцентрической СК
                                   ref XP1_Tr,
                                   ref YP1_Tr,
                                   ref ZP1_Tr,

                                   // Координаты N-й фиктивной точки в опорной геоцентрической СК
                                   ref XFN1_Tr,
                                   ref YFN1_Tr,
                                   ref ZFN1_Tr,

                                   // Координаты фиктивных точек
                                   ref arr_Pel1,
                                   ref arr_Pel_XYZ1

             );

            // ------------------------------------------------------------------------
            // Расчет траектории фиктивных точек по пеленгу2

            f_Bearing
             (

                                   // Пеленг (град)
                                   Theta_Pel2,
                                   // Max дальность отображения пеленга (m)
                                   Mmax_Pel1,
                                   // Количество фиктивных точек в плоскости пеленгования пеленгатора
                                   NumbFikt_Pel1,

                                   // Широта и долгота стояния пеленгатора (град)
                                   LatP_Pel2,
                                   LongP_Pel2,

                                   fl_84_42,

                                   // Координаты пеленгатора в опорной геоцентрической СК
                                   ref XP2_Tr,
                                   ref YP2_Tr,
                                   ref ZP2_Tr,

                                   // Координаты N-й фиктивной точки в опорной геоцентрической СК
                                   ref XFN2_Tr,
                                   ref YFN2_Tr,
                                   ref ZFN2_Tr,

                                   // Координаты фиктивных точек
                                   ref arr_Pel2,
                                   ref arr_Pel_XYZ2


             );
            // ------------------------------------------------------------------------

            // SOVMEST_PEL ***********************************************************
            // Проверка совместимости пеленгов


            // ------------------------------------------------------------------------

            int fls = 0;

            fls = f_Bearing_Compatibility

            (
                 // Входные параметры:
                 // Пеленг (град)
                 Theta_Pel1,
                 Theta_Pel2,
                 // Широта и долгота стояния пеленгатора (град)
                 LatP_Pel1,
                 LongP_Pel1,
                 LatP_Pel2,
                 LongP_Pel2,
                 Smax,
                 fl_84_42
            );
            // ------------------------------------------------------------------------
            if (fls == -1)
                return -1;
            if (fls == -2)
                return -2;
            // ------------------------------------------------------------------------
            // Решение на середине отрезка, соединяющего станции

            if (fls == 2)
            {
                // Угловые координаты искомого ИРИ в опорной геоцентрической СК
                LatIRI_Tr = (LatP_Pel1 + LatP_Pel2) / 2;
                LongIRI_Tr = (LongP_Pel1 * LongP_Pel2) / 2;
                return 0;

            } // fls=2
            // ------------------------------------------------------------------------


            // *********************************************************** SOVMEST_PEL

            // Initial conditions ****************************************************
            // Initial conditions
            // м 

            XP1_Tr_tmp = XP1_Tr;
            YP1_Tr_tmp = YP1_Tr;
            ZP1_Tr_tmp = ZP1_Tr;
            XP2_Tr_tmp = XP2_Tr;
            YP2_Tr_tmp = YP2_Tr;
            ZP2_Tr_tmp = ZP2_Tr;

            XFN1_Tr_tmp = XFN1_Tr;
            YFN1_Tr_tmp = YFN1_Tr;
            ZFN1_Tr_tmp = ZFN1_Tr;
            XFN2_Tr_tmp = XFN2_Tr;
            YFN2_Tr_tmp = YFN2_Tr;
            ZFN2_Tr_tmp = ZFN2_Tr;

            // **************************************************** Initial conditions

            // A,T,C *****************************************************************
            // Расчет коэффициентов пллоскости пеленгования

            A1_Tr = YP1_Tr_tmp * (ZFN1_Tr_tmp) - ZP1_Tr_tmp * (YFN1_Tr_tmp);
            T1_Tr = ZP1_Tr_tmp * (XFN1_Tr_tmp) - XP1_Tr_tmp * (ZFN1_Tr_tmp);
            C1_Tr = XP1_Tr_tmp * (YFN1_Tr_tmp) - YP1_Tr_tmp * (XFN1_Tr_tmp);

            A2_Tr = YP2_Tr_tmp * (ZFN2_Tr_tmp) - ZP2_Tr_tmp * (YFN2_Tr_tmp);
            T2_Tr = ZP2_Tr_tmp * (XFN2_Tr_tmp) - XP2_Tr_tmp * (ZFN2_Tr_tmp);
            C2_Tr = XP2_Tr_tmp * (YFN2_Tr_tmp) - YP2_Tr_tmp * (XFN2_Tr_tmp);

            // ***************************************************************** A,T,C

            // C1!=0 11111111111111111111111111111111111111111111111111111111111111111
            if (C1_Tr != 0)
            {
                // .......................................................................

                if (module(C1_Tr) < 1E-10)
                {
                    K1_Tr = -(A1_Tr / 1E-10);
                    K2_Tr = -(T1_Tr / 1E-10);
                }

                else
                {
                    K1_Tr = -(A1_Tr / C1_Tr);
                    K2_Tr = -(T1_Tr / C1_Tr);
                }

                if (module(T1_Tr * C2_Tr - C1_Tr * T2_Tr) < 1E-10)
                    K3_Tr = (C1_Tr * A2_Tr - A1_Tr * C2_Tr) / 1E-10;
                else
                    K3_Tr = (C1_Tr * A2_Tr - A1_Tr * C2_Tr) / (T1_Tr * C2_Tr - C1_Tr * T2_Tr);

                TMP_X = (1/(aa*aa)) * (1+K3_Tr*K3_Tr) +
                        ((K1_Tr+K2_Tr*K3_Tr)* (K1_Tr + K2_Tr * K3_Tr)) / (bb*bb);
                // .......................................................................
                // Координаты искомого ИРИ в опорной геоцентрической СК

                if (module(Math.Sqrt(TMP_X)) < 1E-10)
                {
                    XIRI1_Tr = 1 / 1E-10;
                    XIRI2_Tr = -1 / 1E-10;
                }
                else
                {
                    XIRI1_Tr = 1 / Math.Sqrt(TMP_X);
                    XIRI2_Tr = -1 / Math.Sqrt(TMP_X);
                }

                YIRI1_Tr = K3_Tr * XIRI1_Tr;
                YIRI2_Tr = K3_Tr * XIRI2_Tr;

                ZIRI1_Tr = K1_Tr * XIRI1_Tr + K2_Tr * YIRI1_Tr;
                ZIRI2_Tr = K1_Tr * XIRI2_Tr + K2_Tr * YIRI2_Tr;
                // .......................................................................

            } // C1!=0
            // 11111111111111111111111111111111111111111111111111111111111111111 C1!=0

            // C1=0 222222222222222222222222222222222222222222222222222222222222222222
            else
            {
                // .......................................................................
                if (module(T1_Tr) < 1E-10)
                    K3_Tr = -A1_Tr / 1E-10;
                else
                    K3_Tr = -A1_Tr / T1_Tr;

                if (module(C2_Tr) < 1E-10)
                    K2_Tr = -(A2_Tr + K3_Tr * T2_Tr) / 1E-10;
                else
                    K2_Tr = -(A2_Tr + K3_Tr * T2_Tr) / C2_Tr;

                TMP_X = (1 / (aa * aa)) * (1 + K3_Tr * K3_Tr) +
                        (K2_Tr*K2_Tr) / (bb * bb);

                // .......................................................................
                // Координаты искомого ИРИ в опорной геоцентрической СК

                if (module(Math.Sqrt(TMP_X)) < 1E-10)
                {
                    XIRI1_Tr = 1 / 1E-10;
                    XIRI2_Tr = -1 / 1E-10;
                }

                else
                {
                    XIRI1_Tr = 1 / Math.Sqrt(TMP_X);
                    XIRI2_Tr = -1 / Math.Sqrt(TMP_X);
                }

                YIRI1_Tr = K3_Tr * XIRI1_Tr;
                YIRI2_Tr = K3_Tr * XIRI2_Tr;

                ZIRI1_Tr = K2_Tr * XIRI1_Tr;
                ZIRI2_Tr = K2_Tr * XIRI2_Tr;
                // .......................................................................

            } // C1=0
            // 222222222222222222222222222222222222222222222222222222222222222222 C1=0

            // XIRI_Tr,YIRI_Tr,ZIRI_Tr ***********************************************
            // Критерий выбора координат искомого ИРИ в опорной геоцентрической СК
            // (минимальное расстояние до ПП)

            // Координаты середины отрезка, соединяющего N-ые фиктивные точки
            DefMiddle_Pel(XFN1_Tr_tmp, XFN2_Tr_tmp, ref XM_Tr);
            DefMiddle_Pel(YFN1_Tr_tmp, YFN2_Tr_tmp, ref YM_Tr);
            DefMiddle_Pel(ZFN1_Tr_tmp, ZFN2_Tr_tmp, ref ZM_Tr);

            L1_Tr = Math.Sqrt((XM_Tr - XIRI1_Tr) * (XM_Tr - XIRI1_Tr) +
                              (YM_Tr - YIRI1_Tr) * (YM_Tr - YIRI1_Tr) +
                              (ZM_Tr - ZIRI1_Tr) * (ZM_Tr - ZIRI1_Tr));

            L2_Tr = Math.Sqrt((XM_Tr - XIRI2_Tr) * (XM_Tr - XIRI2_Tr) +
                              (YM_Tr - YIRI2_Tr) * (YM_Tr - YIRI2_Tr) +
                              (ZM_Tr - ZIRI2_Tr) * (ZM_Tr - ZIRI2_Tr));

            if (L1_Tr < L2_Tr)
            {
                XIRI_Tr = XIRI1_Tr;
                YIRI_Tr = YIRI1_Tr;
                ZIRI_Tr = ZIRI1_Tr;
            }
            else
            {
                XIRI_Tr = XIRI2_Tr;
                YIRI_Tr = YIRI2_Tr;
                ZIRI_Tr = ZIRI2_Tr;
            }
            // *********************************************** XIRI_Tr,YIRI_Tr,ZIRI_Tr

            // RIRI_Tr,LatIRI_Tr,LongIRI_Tr ******************************************
            // Угловые координаты искомого ИРИ в опорной геоцентрической СК

            // !!! Д.б. *pRIRI_Tr==REarth_Pel, т.е. r=Rз
            RIRI_Tr = Math.Sqrt(XIRI_Tr * XIRI_Tr + YIRI_Tr * YIRI_Tr + ZIRI_Tr * ZIRI_Tr);

            LatIRI_Tr = Math.Asin(ZIRI_Tr / RIRI_Tr);

            Def_Longitude_180(
                          XIRI_Tr,
                          YIRI_Tr,
                          RIRI_Tr,
                          LatIRI_Tr,
                          ref LongIRI_Tr
                         );

            // ****************************************** RIRI_Tr,LatIRI_Tr,LongIRI_Tr



            // *********************************************************************
            // Перевод в град

            // Угловые координаты искомого ИРИ в опорной геоцентрической СК
            LatIRI_Tr = (LatIRI_Tr * 180) / Math.PI;
            LongIRI_Tr = (LongIRI_Tr * 180) / Math.PI;
            // *********************************************************************


            // LLPP1
            ClassGeoCalculator.f_XYZ_BLH_84(
                          XIRI_Tr,
                          YIRI_Tr,
                          ZIRI_Tr,
                          ref LatIRI_Tr,
                          ref LongIRI_Tr
                                     );

            // Уточнение координат ********************************************************************
            // Уточнение координат 

            double sss1 = 0;
            double sss2 = 0;

            // 
            sss1 = f_D_2Points
           (
             // Широта и долгота стояния точек (град)
             LatP_Pel1,
             LongP_Pel1,
             LatIRI_Tr,
             LongIRI_Tr,
             1
           );

            sss2 = f_D_2Points
           (
             // Широта и долгота стояния точек (град)
             LatP_Pel2,
             LongP_Pel2,
             LatIRI_Tr,
             LongIRI_Tr,
             1
           );

            // ------------------------------------------------------------------------
            // Расчет траектории фиктивных точек по пеленгу1

            f_Bearing
             (

                                   // Пеленг (град)
                                   Theta_Pel1,
                                   // Max дальность отображения пеленга (m)
                                   sss1,
                                   // Количество фиктивных точек в плоскости пеленгования пеленгатора
                                   NumbFikt_Pel1,

                                   // Широта и долгота стояния пеленгатора (град)
                                   LatP_Pel1,
                                   LongP_Pel1,

                                   fl_84_42,

                                   // Координаты пеленгатора в опорной геоцентрической СК
                                   ref XP1_Tr,
                                   ref YP1_Tr,
                                   ref ZP1_Tr,

                                   // Координаты N-й фиктивной точки в опорной геоцентрической СК
                                   ref XFN1_Tr,
                                   ref YFN1_Tr,
                                   ref ZFN1_Tr,

                                   // Координаты фиктивных точек
                                   ref arr_Pel1,
                                   ref arr_Pel_XYZ1


             );

            // ------------------------------------------------------------------------
            // Расчет траектории фиктивных точек по пеленгу2

            f_Bearing
             (

                                   // Пеленг (град)
                                   Theta_Pel2,
                                   // Max дальность отображения пеленга (m)
                                   sss2,
                                   // Количество фиктивных точек в плоскости пеленгования пеленгатора
                                   NumbFikt_Pel1,

                                   // Широта и долгота стояния пеленгатора (град)
                                   LatP_Pel2,
                                   LongP_Pel2,

                                   fl_84_42,

                                   // Координаты пеленгатора в опорной геоцентрической СК
                                   ref XP2_Tr,
                                   ref YP2_Tr,
                                   ref ZP2_Tr,

                                   // Координаты N-й фиктивной точки в опорной геоцентрической СК
                                   ref XFN2_Tr,
                                   ref YFN2_Tr,
                                   ref ZFN2_Tr,

                                   // Координаты фиктивных точек
                                   ref arr_Pel2,
                                   ref arr_Pel_XYZ2


             );
            // ------------------------------------------------------------------------


            // Initial conditions ****************************************************
            // Initial conditions
            // м 

            XP1_Tr_tmp = XP1_Tr;
            YP1_Tr_tmp = YP1_Tr;
            ZP1_Tr_tmp = ZP1_Tr;
            XP2_Tr_tmp = XP2_Tr;
            YP2_Tr_tmp = YP2_Tr;
            ZP2_Tr_tmp = ZP2_Tr;

            XFN1_Tr_tmp = XFN1_Tr;
            YFN1_Tr_tmp = YFN1_Tr;
            ZFN1_Tr_tmp = ZFN1_Tr;
            XFN2_Tr_tmp = XFN2_Tr;
            YFN2_Tr_tmp = YFN2_Tr;
            ZFN2_Tr_tmp = ZFN2_Tr;

            // **************************************************** Initial conditions

            // A,T,C *****************************************************************
            // Расчет коэффициентов пллоскости пеленгования

            A1_Tr = YP1_Tr_tmp * (ZFN1_Tr_tmp) - ZP1_Tr_tmp * (YFN1_Tr_tmp);
            T1_Tr = ZP1_Tr_tmp * (XFN1_Tr_tmp) - XP1_Tr_tmp * (ZFN1_Tr_tmp);
            C1_Tr = XP1_Tr_tmp * (YFN1_Tr_tmp) - YP1_Tr_tmp * (XFN1_Tr_tmp);

            A2_Tr = YP2_Tr_tmp * (ZFN2_Tr_tmp) - ZP2_Tr_tmp * (YFN2_Tr_tmp);
            T2_Tr = ZP2_Tr_tmp * (XFN2_Tr_tmp) - XP2_Tr_tmp * (ZFN2_Tr_tmp);
            C2_Tr = XP2_Tr_tmp * (YFN2_Tr_tmp) - YP2_Tr_tmp * (XFN2_Tr_tmp);

            // ***************************************************************** A,T,C

            // C1!=0 11111111111111111111111111111111111111111111111111111111111111111
            if (C1_Tr != 0)
            {
                // .......................................................................

                if (module(C1_Tr) < 1E-10)
                {
                    K1_Tr = -(A1_Tr / 1E-10);
                    K2_Tr = -(T1_Tr / 1E-10);
                }

                else
                {
                    K1_Tr = -(A1_Tr / C1_Tr);
                    K2_Tr = -(T1_Tr / C1_Tr);
                }

                if (module(T1_Tr * C2_Tr - C1_Tr * T2_Tr) < 1E-10)
                    K3_Tr = (C1_Tr * A2_Tr - A1_Tr * C2_Tr) / 1E-10;
                else
                    K3_Tr = (C1_Tr * A2_Tr - A1_Tr * C2_Tr) / (T1_Tr * C2_Tr - C1_Tr * T2_Tr);
                // .......................................................................
                // Координаты искомого ИРИ в опорной геоцентрической СК

                if (module(Math.Sqrt(1 + K3_Tr * K3_Tr + (K1_Tr + K2_Tr * K3_Tr) * (K1_Tr + K2_Tr * K3_Tr))) < 1E-10)
                {
                    XIRI1_Tr = REarth_Pel / 1E-10;
                    XIRI2_Tr = -REarth_Pel / 1E-10;
                }
                else
                {
                    XIRI1_Tr = REarth_Pel / Math.Sqrt(1 + K3_Tr * K3_Tr + (K1_Tr + K2_Tr * K3_Tr) * (K1_Tr + K2_Tr * K3_Tr));
                    XIRI2_Tr = -REarth_Pel / Math.Sqrt(1 + K3_Tr * K3_Tr + (K1_Tr + K2_Tr * K3_Tr) * (K1_Tr + K2_Tr * K3_Tr));
                }

                YIRI1_Tr = K3_Tr * XIRI1_Tr;
                YIRI2_Tr = K3_Tr * XIRI2_Tr;

                ZIRI1_Tr = K1_Tr * XIRI1_Tr + K2_Tr * YIRI1_Tr;
                ZIRI2_Tr = K1_Tr * XIRI2_Tr + K2_Tr * YIRI2_Tr;
                // .......................................................................

            } // C1!=0
            // 11111111111111111111111111111111111111111111111111111111111111111 C1!=0

            // C1=0 222222222222222222222222222222222222222222222222222222222222222222
            else
            {
                // .......................................................................
                if (module(T1_Tr) < 1E-10)
                    K3_Tr = -A1_Tr / 1E-10;
                else
                    K3_Tr = -A1_Tr / T1_Tr;

                if (module(C2_Tr) < 1E-10)
                    K2_Tr = -(A2_Tr + K3_Tr * T2_Tr) / 1E-10;
                else
                    K2_Tr = -(A2_Tr + K3_Tr * T2_Tr) / C2_Tr;

                // .......................................................................
                // Координаты искомого ИРИ в опорной геоцентрической СК

                if (module(Math.Sqrt(1 + K2_Tr * K2_Tr + K3_Tr * K3_Tr)) < 1E-10)
                {
                    XIRI1_Tr = REarth_Pel / 1E-10;
                    XIRI2_Tr = -REarth_Pel / 1E-10;
                }

                else
                {
                    XIRI1_Tr = REarth_Pel / Math.Sqrt(1 + K2_Tr * K2_Tr + K3_Tr * K3_Tr);
                    XIRI2_Tr = -REarth_Pel / Math.Sqrt(1 + K2_Tr * K2_Tr + K3_Tr * K3_Tr);
                }

                YIRI1_Tr = K3_Tr * XIRI1_Tr;
                YIRI2_Tr = K3_Tr * XIRI2_Tr;

                ZIRI1_Tr = K2_Tr * XIRI1_Tr;
                ZIRI2_Tr = K2_Tr * XIRI2_Tr;
                // .......................................................................

            } // C1=0
            // 222222222222222222222222222222222222222222222222222222222222222222 C1=0

            // XIRI_Tr,YIRI_Tr,ZIRI_Tr ***********************************************
            // Критерий выбора координат искомого ИРИ в опорной геоцентрической СК
            // (минимальное расстояние до ПП)

            // Координаты середины отрезка, соединяющего N-ые фиктивные точки
            DefMiddle_Pel(XFN1_Tr_tmp, XFN2_Tr_tmp, ref XM_Tr);
            DefMiddle_Pel(YFN1_Tr_tmp, YFN2_Tr_tmp, ref YM_Tr);
            DefMiddle_Pel(ZFN1_Tr_tmp, ZFN2_Tr_tmp, ref ZM_Tr);

            L1_Tr = Math.Sqrt((XM_Tr - XIRI1_Tr) * (XM_Tr - XIRI1_Tr) +
                              (YM_Tr - YIRI1_Tr) * (YM_Tr - YIRI1_Tr) +
                              (ZM_Tr - ZIRI1_Tr) * (ZM_Tr - ZIRI1_Tr));

            L2_Tr = Math.Sqrt((XM_Tr - XIRI2_Tr) * (XM_Tr - XIRI2_Tr) +
                              (YM_Tr - YIRI2_Tr) * (YM_Tr - YIRI2_Tr) +
                              (ZM_Tr - ZIRI2_Tr) * (ZM_Tr - ZIRI2_Tr));

            if (L1_Tr < L2_Tr)
            {
                XIRI_Tr = XIRI1_Tr;
                YIRI_Tr = YIRI1_Tr;
                ZIRI_Tr = ZIRI1_Tr;
            }
            else
            {
                XIRI_Tr = XIRI2_Tr;
                YIRI_Tr = YIRI2_Tr;
                ZIRI_Tr = ZIRI2_Tr;
            }
            // *********************************************** XIRI_Tr,YIRI_Tr,ZIRI_Tr

            // RIRI_Tr,LatIRI_Tr,LongIRI_Tr ******************************************
            // Угловые координаты искомого ИРИ в опорной геоцентрической СК

            // !!! Д.б. *pRIRI_Tr==REarth_Pel, т.е. r=Rз
            RIRI_Tr = Math.Sqrt(XIRI_Tr * XIRI_Tr + YIRI_Tr * YIRI_Tr + ZIRI_Tr * ZIRI_Tr);

            LatIRI_Tr = Math.Asin(ZIRI_Tr / RIRI_Tr);

            Def_Longitude_180(
                          XIRI_Tr,
                          YIRI_Tr,
                          RIRI_Tr,
                          LatIRI_Tr,
                          ref LongIRI_Tr
                         );


            // Перевод в град
            // Угловые координаты искомого ИРИ в опорной геоцентрической СК
            LatIRI_Tr = (LatIRI_Tr * 180) / Math.PI;
            LongIRI_Tr = (LongIRI_Tr * 180) / Math.PI;

            // LLPP1
            ClassGeoCalculator.f_XYZ_BLH_84(
                          XIRI_Tr,
                          YIRI_Tr,
                          ZIRI_Tr,
                          ref LatIRI_Tr,
                          ref LongIRI_Tr
                                     );
            // ********************************************************** Уточнение координат



            return 0;

        } // Функция f_Triangulation (2_1-й вариант)
        //**************************************************************************

        // ***********************************************************************
        // !!! Вариант3 (перед триангуляцией НЕ нужно вызывать два раза функции пеленга)

        // Расчет координат точки пересечения двух пеленгов

        // Входные параметры: 

        // Mmax_Pel(м) - максимальная дальность отображения пеленга,
        // NumbFikt_Pel - количество фиктивных точек
        // Theta_Pel1, Theta_Pel2(град) - пеленг,
        // LatP_Pel1,LongP_Pel1,LatP_Pel2,LongP_Pel2 (град) - широта и долгота стояния пеленгатора 

        // fl_84_42=1-> долгота и широта в WGS84, =2 -> SK42
        // Smax- максимальная видимость пеленгатора

        // Выходные параметры: 
        // LatIRI_Tr,LongIRI_Tr - Угловые координаты искомого ИРИ в опорной геоцентрической СК(град)

        // Возврат:
        // -2 -> недопустимые параметры
        // -1 -> несовместимые пеленги

        // Широта -90(юг)...+90(север)
        // Долгота -180(на запад от Гринвича)...+180(на восток от Гринвича)
        // Пеленг 0...360(по часовой стрелке)
        // ***********************************************************************

        public static int f_Triangulation

            (
            // Входные параметры:

                 // Max дальность отображения пеленга (m)
                 double Mmax_Pel,
            // Количество фиктивных точек в плоскости пеленгования пеленгатора
                 uint NumbFikt_Pel,

                 // Пеленг (град)
                 double Theta_Pel1,
                 double Theta_Pel2,

                 // Широта и долгота стояния пеленгатора (град)
                 double LatP_Pel1,
                 double LongP_Pel1,
                 double LatP_Pel2,
                 double LongP_Pel2,

                 int fl_84_42,
                 double Smax,

                // Выходные параметры:

                // Координаты искомого ИРИ в опорной геоцентрической СК(м)
                //ref double XIRI_Tr,
                //ref double YIRI_Tr,
                //ref double ZIRI_Tr,

                // Угловые координаты искомого ИРИ в опорной геоцентрической СК(град)
                //ref double RIRI_Tr,  // м
                ref double LatIRI_Tr,
                ref double LongIRI_Tr

            )
        {

            double XIRI_Tr = 0;
            double YIRI_Tr = 0;
            double ZIRI_Tr = 0;

            // Угловые координаты искомого ИРИ в опорной геоцентрической СК(град)
            double RIRI_Tr = 0;  // м

            // ------------------------------------------------------------------------

            // Радиус Земли  в точке стояния пеленгатора с учетом текущей широты 
            //double REarthP1_Tr = 0;
            //double REarthP2_Tr = 0;

            // Расчетные коэффициенты
            double K1_Tr = 0;
            double K2_Tr = 0;
            double K3_Tr = 0;

            // Коэффициенты канонического уравнения для плоскости пеленгования ИРИ
            // i-ым пеленгатором 
            double A1_Tr = 0; // Для пеленгатора1
            double T1_Tr = 0;
            double C1_Tr = 0;
            double A2_Tr = 0; // Для пеленгатора2
            double T2_Tr = 0;
            double C2_Tr = 0;

            // Координаты искомого ИРИ в опорной геоцентрической СК
            double XIRI1_Tr = 0;
            double XIRI2_Tr = 0;
            double YIRI1_Tr = 0;
            double YIRI2_Tr = 0;
            double ZIRI1_Tr = 0;
            double ZIRI2_Tr = 0;

            // Координаты середины отрезка, соединяющего N-ые фиктивные точки
            double XM_Tr = 0;
            double YM_Tr = 0;
            double ZM_Tr = 0;

            // Расстояние до ПП
            double L1_Tr = 0;
            double L2_Tr = 0;

            // Координаты пеленгатора в опорной геоцентрической СК
            double XP1_Tr_tmp = 0;
            double YP1_Tr_tmp = 0;
            double ZP1_Tr_tmp = 0;
            double XP2_Tr_tmp = 0;
            double YP2_Tr_tmp = 0;
            double ZP2_Tr_tmp = 0;

            // Координаты N-й фиктивной точки в опорной геоцентрической СК
            double XFN1_Tr_tmp = 0;
            double YFN1_Tr_tmp = 0;
            double ZFN1_Tr_tmp = 0;
            double XFN2_Tr_tmp = 0;
            double YFN2_Tr_tmp = 0;
            double ZFN2_Tr_tmp = 0;

            // Радиус Земли (шар)
            double REarth_Pel = 0;

            // Координаты пеленгатора в опорной геоцентрической СК(км)
            double XP1_Tr = 0;
            double YP1_Tr = 0;
            double ZP1_Tr = 0;
            double XP2_Tr = 0;
            double YP2_Tr = 0;
            double ZP2_Tr = 0;
            // Координаты N-й фиктивной точки в опорной геоцентрической СК(км)
            double XFN1_Tr = 0;
            double YFN1_Tr = 0;
            double ZFN1_Tr = 0;
            double XFN2_Tr = 0;
            double YFN2_Tr = 0;
            double ZFN2_Tr = 0;

            double[] arr_Pel1 = new double[NumbFikt_Pel * 2];
            double[] arr_Pel2 = new double[NumbFikt_Pel * 2];

            double[] arr_Pel_XYZ1 = new double[NumbFikt_Pel * 3];
            double[] arr_Pel_XYZ2 = new double[NumbFikt_Pel * 3];
            // ------------------------------------------------------------------------
            double lll = 0;
            lll = (LatP_Pel1 + LatP_Pel2) / 2;
            lll = (lll * Math.PI) / 180;

            // Радиус Земли (шар)
            //REarth_Pel = 6378245;

            //RRR
            // Средний радиус Земли (шар)
            if (fl_84_42 == 2) // Красовский
            {
                REarth_Pel = 6371220;

                // SSS1
                REarth_Pel = 6371117.673;

            }
            else
                //REarth_Pel = 6378136.3; // WGS84
                REarth_Pel = 6371008.771333; // WGS84


            // PPLL
            // Сжатие=(a-b)/a
            // 0.003352 -Красовский
            if (fl_84_42 == 2) // Красовский
                //REarth_Pel = REarth_Pel * (1 - 0.003352 * Math.Sin(lll) * Math.Sin(lll));
                // SSS1
                REarth_Pel = 6371117.673;
            else
            {
                double lll1 = 0;
                lll1 = (LatP_Pel1 * Math.PI) / 180;
                //REarth_Pel = REarth_Pel * (1 - 0.00669438 * Math.Sin(lll) * Math.Sin(lll)); // WGS84
                //REarth_Pel = REarth_Pel * (1 - 0.00669438 * Math.Sin(lll1) * Math.Sin(lll1)); // WGS84

                // RRR
                //REarth_Pel = 6371008.8;

                // SSS1
                REarth_Pel = 6371008.771333;

            }

            // ------------------------------------------------------------------------
            // Расчет траектории фиктивных точек по пеленгу1

            f_Bearing
             (

                  // Пеленг (град)
                  Theta_Pel1,
                // Max дальность отображения пеленга (m)
                  Mmax_Pel,
                // Количество фиктивных точек в плоскости пеленгования пеленгатора
                  NumbFikt_Pel,

                  // Широта и долгота стояния пеленгатора (град)
                  LatP_Pel1,
                  LongP_Pel1,

                  fl_84_42,

                  // Координаты пеленгатора в опорной геоцентрической СК
                  ref XP1_Tr,
                  ref YP1_Tr,
                  ref ZP1_Tr,

                  // Координаты N-й фиктивной точки в опорной геоцентрической СК
                  ref XFN1_Tr,
                  ref YFN1_Tr,
                  ref ZFN1_Tr,

                  // Координаты фиктивных точек
                  ref arr_Pel1,
                  ref arr_Pel_XYZ1

             );

            // ------------------------------------------------------------------------
            // Расчет траектории фиктивных точек по пеленгу2

            f_Bearing
             (

                  // Пеленг (град)
                  Theta_Pel2,
                // Max дальность отображения пеленга (m)
                  Mmax_Pel,
                // Количество фиктивных точек в плоскости пеленгования пеленгатора
                  NumbFikt_Pel,

                  // Широта и долгота стояния пеленгатора (град)
                  LatP_Pel2,
                  LongP_Pel2,

                  fl_84_42,

                  // Координаты пеленгатора в опорной геоцентрической СК
                  ref XP2_Tr,
                  ref YP2_Tr,
                  ref ZP2_Tr,

                  // Координаты N-й фиктивной точки в опорной геоцентрической СК
                  ref XFN2_Tr,
                  ref YFN2_Tr,
                  ref ZFN2_Tr,

                  // Координаты фиктивных точек
                  ref arr_Pel2,
                  ref arr_Pel_XYZ2

             );
            // ------------------------------------------------------------------------

            // SOVMEST_PEL ***********************************************************
            // Проверка совместимости пеленгов


            // ------------------------------------------------------------------------

            int fls = 0;

            fls = f_Bearing_Compatibility

            (
                // Входные параметры:
                // Пеленг (град)
                 Theta_Pel1,
                 Theta_Pel2,
                // Широта и долгота стояния пеленгатора (град)
                 LatP_Pel1,
                 LongP_Pel1,
                 LatP_Pel2,
                 LongP_Pel2,
                 Smax,
                 fl_84_42
            );
            // ------------------------------------------------------------------------
            if (fls == -1)
                return -1;
            if (fls == -2)
                return -2;
            // ------------------------------------------------------------------------
            // Решение на середине отрезка, соединяющего станции

            if (fls == 2)
            {
                // Угловые координаты искомого ИРИ в опорной геоцентрической СК
                LatIRI_Tr = (LatP_Pel1 + LatP_Pel2) / 2;
                LongIRI_Tr = (LongP_Pel1 * LongP_Pel2) / 2;
                return 0;

            } // fls=2
            // ------------------------------------------------------------------------


            // *********************************************************** SOVMEST_PEL

            // Initial conditions ****************************************************
            // Initial conditions
            // м 

            XP1_Tr_tmp = XP1_Tr;
            YP1_Tr_tmp = YP1_Tr;
            ZP1_Tr_tmp = ZP1_Tr;
            XP2_Tr_tmp = XP2_Tr;
            YP2_Tr_tmp = YP2_Tr;
            ZP2_Tr_tmp = ZP2_Tr;

            XFN1_Tr_tmp = XFN1_Tr;
            YFN1_Tr_tmp = YFN1_Tr;
            ZFN1_Tr_tmp = ZFN1_Tr;
            XFN2_Tr_tmp = XFN2_Tr;
            YFN2_Tr_tmp = YFN2_Tr;
            ZFN2_Tr_tmp = ZFN2_Tr;

            // **************************************************** Initial conditions

            // A,T,C *****************************************************************
            // Расчет коэффициентов пллоскости пеленгования

            A1_Tr = YP1_Tr_tmp * (ZFN1_Tr_tmp) - ZP1_Tr_tmp * (YFN1_Tr_tmp);
            T1_Tr = ZP1_Tr_tmp * (XFN1_Tr_tmp) - XP1_Tr_tmp * (ZFN1_Tr_tmp);
            C1_Tr = XP1_Tr_tmp * (YFN1_Tr_tmp) - YP1_Tr_tmp * (XFN1_Tr_tmp);

            A2_Tr = YP2_Tr_tmp * (ZFN2_Tr_tmp) - ZP2_Tr_tmp * (YFN2_Tr_tmp);
            T2_Tr = ZP2_Tr_tmp * (XFN2_Tr_tmp) - XP2_Tr_tmp * (ZFN2_Tr_tmp);
            C2_Tr = XP2_Tr_tmp * (YFN2_Tr_tmp) - YP2_Tr_tmp * (XFN2_Tr_tmp);

            // ***************************************************************** A,T,C

            // C1!=0 11111111111111111111111111111111111111111111111111111111111111111
            if (C1_Tr != 0)
            {
                // .......................................................................

                if (module(C1_Tr) < 1E-10)
                {
                    K1_Tr = -(A1_Tr / 1E-10);
                    K2_Tr = -(T1_Tr / 1E-10);
                }

                else
                {
                    K1_Tr = -(A1_Tr / C1_Tr);
                    K2_Tr = -(T1_Tr / C1_Tr);
                }

                if (module(T1_Tr * C2_Tr - C1_Tr * T2_Tr) < 1E-10)
                    K3_Tr = (C1_Tr * A2_Tr - A1_Tr * C2_Tr) / 1E-10;
                else
                    K3_Tr = (C1_Tr * A2_Tr - A1_Tr * C2_Tr) / (T1_Tr * C2_Tr - C1_Tr * T2_Tr);
                // .......................................................................
                // Координаты искомого ИРИ в опорной геоцентрической СК

                if (module(Math.Sqrt(1 + K3_Tr * K3_Tr + (K1_Tr + K2_Tr * K3_Tr) * (K1_Tr + K2_Tr * K3_Tr))) < 1E-10)
                {
                    XIRI1_Tr = REarth_Pel / 1E-10;
                    XIRI2_Tr = -REarth_Pel / 1E-10;
                }
                else
                {
                    XIRI1_Tr = REarth_Pel / Math.Sqrt(1 + K3_Tr * K3_Tr + (K1_Tr + K2_Tr * K3_Tr) * (K1_Tr + K2_Tr * K3_Tr));
                    XIRI2_Tr = -REarth_Pel / Math.Sqrt(1 + K3_Tr * K3_Tr + (K1_Tr + K2_Tr * K3_Tr) * (K1_Tr + K2_Tr * K3_Tr));
                }

                YIRI1_Tr = K3_Tr * XIRI1_Tr;
                YIRI2_Tr = K3_Tr * XIRI2_Tr;

                ZIRI1_Tr = K1_Tr * XIRI1_Tr + K2_Tr * YIRI1_Tr;
                ZIRI2_Tr = K1_Tr * XIRI2_Tr + K2_Tr * YIRI2_Tr;
                // .......................................................................

            } // C1!=0
            // 11111111111111111111111111111111111111111111111111111111111111111 C1!=0

            // C1=0 222222222222222222222222222222222222222222222222222222222222222222
            else
            {
                // .......................................................................
                if (module(T1_Tr) < 1E-10)
                    K3_Tr = -A1_Tr / 1E-10;
                else
                    K3_Tr = -A1_Tr / T1_Tr;

                if (module(C2_Tr) < 1E-10)
                    K2_Tr = -(A2_Tr + K3_Tr * T2_Tr) / 1E-10;
                else
                    K2_Tr = -(A2_Tr + K3_Tr * T2_Tr) / C2_Tr;

                // .......................................................................
                // Координаты искомого ИРИ в опорной геоцентрической СК

                if (module(Math.Sqrt(1 + K2_Tr * K2_Tr + K3_Tr * K3_Tr)) < 1E-10)
                {
                    XIRI1_Tr = REarth_Pel / 1E-10;
                    XIRI2_Tr = -REarth_Pel / 1E-10;
                }

                else
                {
                    XIRI1_Tr = REarth_Pel / Math.Sqrt(1 + K2_Tr * K2_Tr + K3_Tr * K3_Tr);
                    XIRI2_Tr = -REarth_Pel / Math.Sqrt(1 + K2_Tr * K2_Tr + K3_Tr * K3_Tr);
                }

                YIRI1_Tr = K3_Tr * XIRI1_Tr;
                YIRI2_Tr = K3_Tr * XIRI2_Tr;

                ZIRI1_Tr = K2_Tr * XIRI1_Tr;
                ZIRI2_Tr = K2_Tr * XIRI2_Tr;
                // .......................................................................

            } // C1=0
            // 222222222222222222222222222222222222222222222222222222222222222222 C1=0

            // XIRI_Tr,YIRI_Tr,ZIRI_Tr ***********************************************
            // Критерий выбора координат искомого ИРИ в опорной геоцентрической СК
            // (минимальное расстояние до ПП)

            // Координаты середины отрезка, соединяющего N-ые фиктивные точки
            DefMiddle_Pel(XFN1_Tr_tmp, XFN2_Tr_tmp, ref XM_Tr);
            DefMiddle_Pel(YFN1_Tr_tmp, YFN2_Tr_tmp, ref YM_Tr);
            DefMiddle_Pel(ZFN1_Tr_tmp, ZFN2_Tr_tmp, ref ZM_Tr);

            L1_Tr = Math.Sqrt((XM_Tr - XIRI1_Tr) * (XM_Tr - XIRI1_Tr) +
                              (YM_Tr - YIRI1_Tr) * (YM_Tr - YIRI1_Tr) +
                              (ZM_Tr - ZIRI1_Tr) * (ZM_Tr - ZIRI1_Tr));

            L2_Tr = Math.Sqrt((XM_Tr - XIRI2_Tr) * (XM_Tr - XIRI2_Tr) +
                              (YM_Tr - YIRI2_Tr) * (YM_Tr - YIRI2_Tr) +
                              (ZM_Tr - ZIRI2_Tr) * (ZM_Tr - ZIRI2_Tr));

            if (L1_Tr < L2_Tr)
            {
                XIRI_Tr = XIRI1_Tr;
                YIRI_Tr = YIRI1_Tr;
                ZIRI_Tr = ZIRI1_Tr;
            }
            else
            {
                XIRI_Tr = XIRI2_Tr;
                YIRI_Tr = YIRI2_Tr;
                ZIRI_Tr = ZIRI2_Tr;
            }
            // *********************************************** XIRI_Tr,YIRI_Tr,ZIRI_Tr

            // RIRI_Tr,LatIRI_Tr,LongIRI_Tr ******************************************
            // Угловые координаты искомого ИРИ в опорной геоцентрической СК

            // !!! Д.б. *pRIRI_Tr==REarth_Pel, т.е. r=Rз
            RIRI_Tr = Math.Sqrt(XIRI_Tr * XIRI_Tr + YIRI_Tr * YIRI_Tr + ZIRI_Tr * ZIRI_Tr);

            LatIRI_Tr = Math.Asin(ZIRI_Tr / RIRI_Tr);

            Def_Longitude_180(
                          XIRI_Tr,
                          YIRI_Tr,
                          RIRI_Tr,
                          LatIRI_Tr,
                          ref LongIRI_Tr
                         );

            // ****************************************** RIRI_Tr,LatIRI_Tr,LongIRI_Tr

            // *********************************************************************
            // Перевод в град

            // Угловые координаты искомого ИРИ в опорной геоцентрической СК
            LatIRI_Tr = (LatIRI_Tr * 180) / Math.PI;
            LongIRI_Tr = (LongIRI_Tr * 180) / Math.PI;
            // *********************************************************************

            // LLPP1
            ClassGeoCalculator.f_XYZ_BLH_84(
                          XIRI_Tr,
                          YIRI_Tr,
                          ZIRI_Tr,
                          ref LatIRI_Tr,
                          ref LongIRI_Tr
                                     );
            // *********************************************************************
            // Уточнение координат 

            double sss1 = 0;
            double sss2 = 0;

            // 
            sss1 = f_D_2Points
           (
             // Широта и долгота стояния точек (град)
             LatP_Pel1,
             LongP_Pel1,
             LatIRI_Tr,
             LongIRI_Tr,
             1
           );

            sss2 = f_D_2Points
           (
             // Широта и долгота стояния точек (град)
             LatP_Pel2,
             LongP_Pel2,
             LatIRI_Tr,
             LongIRI_Tr,
             1
           );
            // ------------------------------------------------------------------------
            // Расчет траектории фиктивных точек по пеленгу1

            f_Bearing
             (

                                   // Пеленг (град)
                                   Theta_Pel1,
                                   // Max дальность отображения пеленга (m)
                                   sss1,
                                   // Количество фиктивных точек в плоскости пеленгования пеленгатора
                                   NumbFikt_Pel,

                                   // Широта и долгота стояния пеленгатора (град)
                                   LatP_Pel1,
                                   LongP_Pel1,

                                   fl_84_42,

                                   // Координаты пеленгатора в опорной геоцентрической СК
                                   ref XP1_Tr,
                                   ref YP1_Tr,
                                   ref ZP1_Tr,

                                   // Координаты N-й фиктивной точки в опорной геоцентрической СК
                                   ref XFN1_Tr,
                                   ref YFN1_Tr,
                                   ref ZFN1_Tr,

                                   // Координаты фиктивных точек
                                   ref arr_Pel1,
                                   ref arr_Pel_XYZ1


             );

            // ------------------------------------------------------------------------
            // Расчет траектории фиктивных точек по пеленгу2

            f_Bearing
             (

                                   // Пеленг (град)
                                   Theta_Pel2,
                                   // Max дальность отображения пеленга (m)
                                   sss2,
                                   // Количество фиктивных точек в плоскости пеленгования пеленгатора
                                   NumbFikt_Pel,

                                   // Широта и долгота стояния пеленгатора (град)
                                   LatP_Pel2,
                                   LongP_Pel2,

                                   fl_84_42,

                                   // Координаты пеленгатора в опорной геоцентрической СК
                                   ref XP2_Tr,
                                   ref YP2_Tr,
                                   ref ZP2_Tr,

                                   // Координаты N-й фиктивной точки в опорной геоцентрической СК
                                   ref XFN2_Tr,
                                   ref YFN2_Tr,
                                   ref ZFN2_Tr,

                                   // Координаты фиктивных точек
                                   ref arr_Pel2,
                                   ref arr_Pel_XYZ2


             );
            // ------------------------------------------------------------------------


            // Initial conditions ****************************************************
            // Initial conditions
            // м 

            XP1_Tr_tmp = XP1_Tr;
            YP1_Tr_tmp = YP1_Tr;
            ZP1_Tr_tmp = ZP1_Tr;
            XP2_Tr_tmp = XP2_Tr;
            YP2_Tr_tmp = YP2_Tr;
            ZP2_Tr_tmp = ZP2_Tr;

            XFN1_Tr_tmp = XFN1_Tr;
            YFN1_Tr_tmp = YFN1_Tr;
            ZFN1_Tr_tmp = ZFN1_Tr;
            XFN2_Tr_tmp = XFN2_Tr;
            YFN2_Tr_tmp = YFN2_Tr;
            ZFN2_Tr_tmp = ZFN2_Tr;

            // **************************************************** Initial conditions

            // A,T,C *****************************************************************
            // Расчет коэффициентов пллоскости пеленгования

            A1_Tr = YP1_Tr_tmp * (ZFN1_Tr_tmp) - ZP1_Tr_tmp * (YFN1_Tr_tmp);
            T1_Tr = ZP1_Tr_tmp * (XFN1_Tr_tmp) - XP1_Tr_tmp * (ZFN1_Tr_tmp);
            C1_Tr = XP1_Tr_tmp * (YFN1_Tr_tmp) - YP1_Tr_tmp * (XFN1_Tr_tmp);

            A2_Tr = YP2_Tr_tmp * (ZFN2_Tr_tmp) - ZP2_Tr_tmp * (YFN2_Tr_tmp);
            T2_Tr = ZP2_Tr_tmp * (XFN2_Tr_tmp) - XP2_Tr_tmp * (ZFN2_Tr_tmp);
            C2_Tr = XP2_Tr_tmp * (YFN2_Tr_tmp) - YP2_Tr_tmp * (XFN2_Tr_tmp);

            // ***************************************************************** A,T,C

            // C1!=0 11111111111111111111111111111111111111111111111111111111111111111
            if (C1_Tr != 0)
            {
                // .......................................................................

                if (module(C1_Tr) < 1E-10)
                {
                    K1_Tr = -(A1_Tr / 1E-10);
                    K2_Tr = -(T1_Tr / 1E-10);
                }

                else
                {
                    K1_Tr = -(A1_Tr / C1_Tr);
                    K2_Tr = -(T1_Tr / C1_Tr);
                }

                if (module(T1_Tr * C2_Tr - C1_Tr * T2_Tr) < 1E-10)
                    K3_Tr = (C1_Tr * A2_Tr - A1_Tr * C2_Tr) / 1E-10;
                else
                    K3_Tr = (C1_Tr * A2_Tr - A1_Tr * C2_Tr) / (T1_Tr * C2_Tr - C1_Tr * T2_Tr);
                // .......................................................................
                // Координаты искомого ИРИ в опорной геоцентрической СК

                if (module(Math.Sqrt(1 + K3_Tr * K3_Tr + (K1_Tr + K2_Tr * K3_Tr) * (K1_Tr + K2_Tr * K3_Tr))) < 1E-10)
                {
                    XIRI1_Tr = REarth_Pel / 1E-10;
                    XIRI2_Tr = -REarth_Pel / 1E-10;
                }
                else
                {
                    XIRI1_Tr = REarth_Pel / Math.Sqrt(1 + K3_Tr * K3_Tr + (K1_Tr + K2_Tr * K3_Tr) * (K1_Tr + K2_Tr * K3_Tr));
                    XIRI2_Tr = -REarth_Pel / Math.Sqrt(1 + K3_Tr * K3_Tr + (K1_Tr + K2_Tr * K3_Tr) * (K1_Tr + K2_Tr * K3_Tr));
                }

                YIRI1_Tr = K3_Tr * XIRI1_Tr;
                YIRI2_Tr = K3_Tr * XIRI2_Tr;

                ZIRI1_Tr = K1_Tr * XIRI1_Tr + K2_Tr * YIRI1_Tr;
                ZIRI2_Tr = K1_Tr * XIRI2_Tr + K2_Tr * YIRI2_Tr;
                // .......................................................................

            } // C1!=0
            // 11111111111111111111111111111111111111111111111111111111111111111 C1!=0

            // C1=0 222222222222222222222222222222222222222222222222222222222222222222
            else
            {
                // .......................................................................
                if (module(T1_Tr) < 1E-10)
                    K3_Tr = -A1_Tr / 1E-10;
                else
                    K3_Tr = -A1_Tr / T1_Tr;

                if (module(C2_Tr) < 1E-10)
                    K2_Tr = -(A2_Tr + K3_Tr * T2_Tr) / 1E-10;
                else
                    K2_Tr = -(A2_Tr + K3_Tr * T2_Tr) / C2_Tr;

                // .......................................................................
                // Координаты искомого ИРИ в опорной геоцентрической СК

                if (module(Math.Sqrt(1 + K2_Tr * K2_Tr + K3_Tr * K3_Tr)) < 1E-10)
                {
                    XIRI1_Tr = REarth_Pel / 1E-10;
                    XIRI2_Tr = -REarth_Pel / 1E-10;
                }

                else
                {
                    XIRI1_Tr = REarth_Pel / Math.Sqrt(1 + K2_Tr * K2_Tr + K3_Tr * K3_Tr);
                    XIRI2_Tr = -REarth_Pel / Math.Sqrt(1 + K2_Tr * K2_Tr + K3_Tr * K3_Tr);
                }

                YIRI1_Tr = K3_Tr * XIRI1_Tr;
                YIRI2_Tr = K3_Tr * XIRI2_Tr;

                ZIRI1_Tr = K2_Tr * XIRI1_Tr;
                ZIRI2_Tr = K2_Tr * XIRI2_Tr;
                // .......................................................................

            } // C1=0
            // 222222222222222222222222222222222222222222222222222222222222222222 C1=0

            // XIRI_Tr,YIRI_Tr,ZIRI_Tr ***********************************************
            // Критерий выбора координат искомого ИРИ в опорной геоцентрической СК
            // (минимальное расстояние до ПП)

            // Координаты середины отрезка, соединяющего N-ые фиктивные точки
            DefMiddle_Pel(XFN1_Tr_tmp, XFN2_Tr_tmp, ref XM_Tr);
            DefMiddle_Pel(YFN1_Tr_tmp, YFN2_Tr_tmp, ref YM_Tr);
            DefMiddle_Pel(ZFN1_Tr_tmp, ZFN2_Tr_tmp, ref ZM_Tr);

            L1_Tr = Math.Sqrt((XM_Tr - XIRI1_Tr) * (XM_Tr - XIRI1_Tr) +
                              (YM_Tr - YIRI1_Tr) * (YM_Tr - YIRI1_Tr) +
                              (ZM_Tr - ZIRI1_Tr) * (ZM_Tr - ZIRI1_Tr));

            L2_Tr = Math.Sqrt((XM_Tr - XIRI2_Tr) * (XM_Tr - XIRI2_Tr) +
                              (YM_Tr - YIRI2_Tr) * (YM_Tr - YIRI2_Tr) +
                              (ZM_Tr - ZIRI2_Tr) * (ZM_Tr - ZIRI2_Tr));

            if (L1_Tr < L2_Tr)
            {
                XIRI_Tr = XIRI1_Tr;
                YIRI_Tr = YIRI1_Tr;
                ZIRI_Tr = ZIRI1_Tr;
            }
            else
            {
                XIRI_Tr = XIRI2_Tr;
                YIRI_Tr = YIRI2_Tr;
                ZIRI_Tr = ZIRI2_Tr;
            }
            // *********************************************** XIRI_Tr,YIRI_Tr,ZIRI_Tr

            // RIRI_Tr,LatIRI_Tr,LongIRI_Tr ******************************************
            // Угловые координаты искомого ИРИ в опорной геоцентрической СК

            // !!! Д.б. *pRIRI_Tr==REarth_Pel, т.е. r=Rз
            RIRI_Tr = Math.Sqrt(XIRI_Tr * XIRI_Tr + YIRI_Tr * YIRI_Tr + ZIRI_Tr * ZIRI_Tr);

            LatIRI_Tr = Math.Asin(ZIRI_Tr / RIRI_Tr);

            Def_Longitude_180(
                          XIRI_Tr,
                          YIRI_Tr,
                          RIRI_Tr,
                          LatIRI_Tr,
                          ref LongIRI_Tr
                         );


            // Перевод в град
            // Угловые координаты искомого ИРИ в опорной геоцентрической СК
            LatIRI_Tr = (LatIRI_Tr * 180) / Math.PI;
            LongIRI_Tr = (LongIRI_Tr * 180) / Math.PI;

            // LLPP1
            ClassGeoCalculator.f_XYZ_BLH_84(
                          XIRI_Tr,
                          YIRI_Tr,
                          ZIRI_Tr,
                          ref LatIRI_Tr,
                          ref LongIRI_Tr
                                     );


            return 0;

        } // Функция f_Triangulation (3-й вариант)
        //**************************************************************************

        // ***********************************************************************
        // Проверка совместимости двух пеленгов

        // Входные параметры: 

        // Theta_Pel1, Theta_Pel2(град) - пеленг,
        // LatP_Pel1,LongP_Pel1,LatP_Pel2,LongP_Pel2 (град) - широта и долгота 
        // стояния пеленгатора 
        // XP1_Pel,YP1_Pel,ZP1_Pel,XP2_Pel,YP2_Pel,ZP2_Pel (м) -координаты 
        // пеленгатора в опорной геоцентрической СК
        // Smax - дальность видимости пеленгатора
        // fl_84_42=1-> долгота и широта в WGS84, =2 -> SK42

        // Выходные параметры: 
        // -1 -> пеленги несовместимы
        // -2 -> недопустимое значение долготы/широты/пеленга
        // 0  -> совместимы
        // 2  -> решение будет на середине отрезка, соединяющего станции

        // Широта -90(юг)...+90(север)
        // Долгота -180(на запад от Гринвича)...+180(на восток от Гринвича)
        // Пеленг 0...360(по часовой стрелке)
        // ***********************************************************************

        public static int f_Bearing_Compatibility

            (
            // Входные параметры:
            // Пеленг (град)
                 double Theta_Pel1,
                 double Theta_Pel2,

                 // Широта и долгота стояния пеленгатора (град)
                 double LatP_Pel1,
                 double LongP_Pel1,
                 double LatP_Pel2,
                 double LongP_Pel2,

                // Координаты пеленгатора в опорной геоцентрической СК(км)
            //double XP1_Tr,
            //double YP1_Tr,
            //double ZP1_Tr,
            //double XP2_Tr,
            //double YP2_Tr,
            //double ZP2_Tr,

                double Smax,

                int fl_84_42


            )
        {

            // -----------------------------------------------------------------------
            double d = 0;
            double X1_Krug = 0;
            double Y1_Krug = 0;
            double X2_Krug = 0;
            double Y2_Krug = 0;
            double X1_UTM = 0;
            double Y1_UTM = 0;
            double X2_UTM = 0;
            double Y2_UTM = 0;
            double dX = 0;
            double dY = 0;
            double Az_2_1 = 0;  // Азимут пеленгатора2 относительно пеленгатора1
            double Az_Dop = 0;
            // -----------------------------------------------------------------------
            if (
               (Theta_Pel1 > 360) ||
               (Theta_Pel1 < 0) ||
               (Theta_Pel2 > 360) ||
               (Theta_Pel2 < 0) ||
               (LatP_Pel1 > 90) ||
               ((LatP_Pel1 < 0) && (LatP_Pel1 < -90)) ||
               (LatP_Pel2 > 90) ||
               ((LatP_Pel2 < 0) && (LatP_Pel2 < -90)) ||
               (LongP_Pel1 > 180) ||
               ((LongP_Pel1 < 0) && (LongP_Pel1 < -180)) ||
               (LongP_Pel2 > 180) ||
               ((LongP_Pel2 < 0) && (LongP_Pel2 < -180))

              )
                return -2;
            // -----------------------------------------------------------------------
            // !!! В СК42 X идет вверх (север), Y вправо (восток) -> dY=X2-X1(CK42)
            //     UTM - наоборот
            // -----------------------------------------------------------------------
            // Преобразование геодезических координат на эллипсоиде Красовского (BL) 
            // в картографическую проекцию Гаусса-Крюгера (XY)

            if (fl_84_42 == 2)
            {
                ClassGeoCalculator.f_SK42_Krug
                (
                   // Входные параметры (!!! град)
                   LatP_Pel1,   // широта
                   LongP_Pel1,  // долгота
                                // Выходные параметры (км)
                   ref X1_Krug,
                   ref Y1_Krug
                );
                /*
                                f_SK42_Krug
                                 (
                                    // Входные параметры (!!! град)
                                   LatP_Pel1,   // широта
                                   LongP_Pel1,  // долгота
                                    // Выходные параметры (км)
                                   ref X1_Krug,
                                   ref Y1_Krug
                                );
                */

                ClassGeoCalculator.f_SK42_Krug
                 (
                    // Входные параметры (!!! град)
                   LatP_Pel2,   // широта
                   LongP_Pel2,  // долгота
                    // Выходные параметры (км)
                   ref X2_Krug,
                   ref Y2_Krug
                );

                dX = Y2_Krug - Y1_Krug;
                dY = X2_Krug - X1_Krug;

            } // эллипсоид Красовского
            // -----------------------------------------------------------------------
            // Преобразование геодезических координат на эллипсоиде WGS84 (BL)  в 
            // универсальную поперечную проекцию Меркатора (XY)

            else
            {
                ClassGeoCalculator.f_WGS84_Mercator
                 (
                    // Входные параметры (grad)
                  LatP_Pel1,   // широта
                  LongP_Pel1,  // долгота
                    // Выходные параметры (m)
                  ref X1_UTM,
                  ref Y1_UTM
                 );

                ClassGeoCalculator.f_WGS84_Mercator
                 (
                    // Входные параметры (grad)
                  LatP_Pel2,   // широта
                  LongP_Pel2,  // долгота
                    // Выходные параметры (m)
                  ref X2_UTM,
                  ref Y2_UTM
                 );

                dX = X2_UTM - X1_UTM;
                dY = Y2_UTM - Y1_UTM;

            } // WGS84
            // -----------------------------------------------------------------------
            // Азимут пеленгатора2 относительно пеленгатора1

            Az_2_1 = f_Def_Azimuth(dY, dX);
            // -----------------------------------------------------------------------


            // SOVMEST_PEL ***********************************************************
            // Проверка совместимости пеленгов

            // Smax ******************************************************************
            // D между пеленгаторами превышает 2*Smax

            d = f_D_2Points

            (
                // Широта и долгота стояния точек (град)
                 LatP_Pel1,
                 LongP_Pel1,
                 LatP_Pel2,
                 LongP_Pel2,

                 fl_84_42

            );

            // RRR
            //if (d > 2 * Smax)
            //    return -1;
            //  ****************************************************************** Smax

            // Lat1=Lat2 **************************************************************
            // Lat1=Lat2

            if (LatP_Pel1 == LatP_Pel2)
            {

                // ------------------------------------------------------------------------
                if ((LatP_Pel1 == 90) || (LatP_Pel1 == -90))
                    return -1;
                // ------------------------------------------------------------------------
                if (LongP_Pel1 == LongP_Pel2)
                    return -1;
                // ------------------------------------------------------------------------
                // Пеленгатор2 находится справа от пеленгатора1

                // IF1
                if (
                   ((LongP_Pel1 >= 0) && ((LongP_Pel2 > 0) && (LongP_Pel2 <= 180)) && (LongP_Pel2 > LongP_Pel1)) ||
                   (((LongP_Pel1 > 0) && (LongP_Pel1 <= 180)) && (LongP_Pel2 < 0)) ||
                   ((LongP_Pel1 < 0) && (LongP_Pel2 < 0) && (Math.Abs(LongP_Pel1) > Math.Abs(LongP_Pel2))) ||
                   ((LongP_Pel1 < 0) && ((LongP_Pel2 >= 0) && (LongP_Pel2 < 180)))
                  )
                {

                    // ........................................................................
                    // p1=0/360

                    if ((Theta_Pel1 == 0) || (Theta_Pel1 == 360))
                    {
                        if ((Theta_Pel2 == 0) || (Theta_Pel2 == 360))
                            return -1;
                        if ((Theta_Pel2 > 0) && (Theta_Pel2 <= 270))
                            return -1;

                    } // p1=0/360
                    // ........................................................................
                    // p1=]0...90[

                    if ((Theta_Pel1 > 0) && (Theta_Pel1 < 90))
                    {
                        if ((Theta_Pel2 >= Theta_Pel1) && (Theta_Pel2 <= 270))
                            return -1;

                    } // p1=]0...90[
                    // ........................................................................
                    // p1=90

                    if (Theta_Pel1 == 90)
                    {
                        if (Theta_Pel2 == 270)
                            return 2;
                        else
                            return -1;

                    } // p1=90
                    // ........................................................................
                    // p1=]90...270[

                    if ((Theta_Pel1 > 90) && (Theta_Pel1 < 270))
                    {
                        if ((Theta_Pel2 == 0) || (Theta_Pel2 == 360))
                            return -1;
                        if ((Theta_Pel2 > 0) && (Theta_Pel2 <= 90))
                            return -1;
                        if ((Theta_Pel2 > 90) && (Theta_Pel2 <= Theta_Pel1))
                            return -1;
                        if ((Theta_Pel2 >= 270) && (Theta_Pel2 < 360))
                            return -1;

                    } // // p1=]90...270[
                    // ........................................................................
                    // p1=270

                    if (Theta_Pel1 == 270)
                        return -1;
                    // ........................................................................
                    // p1=]270...360[

                    if ((Theta_Pel1 > 270) && (Theta_Pel1 < 360))
                    {
                        if ((Theta_Pel2 >= 0) && (Theta_Pel2 <= 270))
                            return -1;
                        if ((Theta_Pel2 >= Theta_Pel1) && (Theta_Pel2 <= 360))
                            return -1;

                    } // // p1=]270...360[
                    // ........................................................................

                } // Пеленгатор2 находится справа от пеленгатора1
                // ------------------------------------------------------------------------
                // Пеленгатор2 находится слева от пеленгатора1

                // ELSE по IF1
                else
                {
                    // ........................................................................
                    // p2=0/360

                    if ((Theta_Pel2 == 0) || (Theta_Pel2 == 360))
                    {
                        if ((Theta_Pel1 == 0) || (Theta_Pel1 == 360))
                            return -1;
                        if ((Theta_Pel1 > 0) && (Theta_Pel1 <= 270))
                            return -1;

                    } // p2=0/360
                    // ........................................................................
                    // p2=]0...90[

                    if ((Theta_Pel2 > 0) && (Theta_Pel2 < 90))
                    {
                        if ((Theta_Pel1 >= Theta_Pel2) && (Theta_Pel1 <= 270))
                            return -1;

                    } // p2=]0...90[
                    // ........................................................................
                    // p2=90

                    if (Theta_Pel2 == 90)
                    {
                        if (Theta_Pel1 == 270)
                            return 2;
                        else
                            return -1;

                    } // p2=90
                    // ........................................................................
                    // p2=]90...270[

                    if ((Theta_Pel2 > 90) && (Theta_Pel2 < 270))
                    {
                        if ((Theta_Pel1 == 0) || (Theta_Pel1 == 360))
                            return -1;
                        if ((Theta_Pel1 > 0) && (Theta_Pel1 <= 90))
                            return -1;
                        if ((Theta_Pel1 > 90) && (Theta_Pel1 <= Theta_Pel2))
                            return -1;
                        if ((Theta_Pel1 >= 270) && (Theta_Pel1 < 360))
                            return -1;

                    } // p2=]90...270[
                    // ........................................................................
                    // p2=270

                    if (Theta_Pel2 == 270)
                        return -1;
                    // ........................................................................
                    // p2=]270...360[

                    if ((Theta_Pel2 > 270) && (Theta_Pel2 < 360))
                    {
                        if ((Theta_Pel1 >= 0) && (Theta_Pel1 <= 270))
                            return -1;
                        if ((Theta_Pel1 >= Theta_Pel2) && (Theta_Pel1 <= 360))
                            return -1;

                    } // p2=]270...360[
                    // ........................................................................


                } // Пеленгатор2 находится слева от пеленгатора1
                // ------------------------------------------------------------------------

                return 0;

            } // Lat1=Lat2
            // ************************************************************** Lat1=Lat2

            // Long1=Long2 ************************************************************

            if (LongP_Pel1 == LongP_Pel2)
            {

                // ------------------------------------------------------------------------
                if (LatP_Pel1 == LatP_Pel2)
                    return -1;
                // ------------------------------------------------------------------------
                // Пеленгатор2 находится выше пеленгатора1

                // IF2
                if (
                    ((LatP_Pel1 >= 0) && ((LatP_Pel2 > 0) && (LatP_Pel2 <= 90)) && (LatP_Pel2 > LatP_Pel1)) ||
                    (((LatP_Pel1 < 0) && (LatP_Pel1 >= -90)) && (LatP_Pel2 >= 0)) ||
                    (((LatP_Pel1 < 0) && (LatP_Pel1 >= -90)) && (LatP_Pel2 < 0) && (Math.Abs(LatP_Pel2) < Math.Abs(LatP_Pel1)))
                   )
                {
                    // ........................................................................
                    // p1=0/360

                    if ((Theta_Pel1 == 0) || (Theta_Pel1 == 360))
                    {
                        if (Theta_Pel2 == 180)
                            return 2;
                        else
                            return -1;

                    } // p1=0/360
                    // ........................................................................
                    // p1=]0...180[

                    if ((Theta_Pel1 > 0) && (Theta_Pel1 < 180))
                    {
                        if ((Theta_Pel2 >= 0) && (Theta_Pel2 <= Theta_Pel1))
                            return -1;
                        if ((Theta_Pel2 >= 180) && (Theta_Pel2 <= 360))
                            return -1;

                    } // p1=]0...180[
                    // ........................................................................
                    // p1=180

                    if (Theta_Pel1 == 180)
                        return -1;
                    // ........................................................................
                    // p1=]180...360[

                    if ((Theta_Pel1 > 180) && (Theta_Pel1 < 360))
                    {
                        if ((Theta_Pel2 >= 0) && (Theta_Pel2 <= 180))
                            return -1;
                        if ((Theta_Pel2 >= Theta_Pel1) && (Theta_Pel2 <= 360))
                            return -1;

                    } // p1=]180...360[
                    // ........................................................................

                } // Пеленгатор2 находится выше пеленгатора1
                // ------------------------------------------------------------------------
                // Пеленгатор2 находится ниже пеленгатора1

                // ELSE по IF2
                else
                {
                    // ........................................................................
                    // p2=0/360

                    if ((Theta_Pel2 == 0) || (Theta_Pel2 == 360))
                    {
                        if (Theta_Pel1 == 180)
                            return 2;
                        else
                            return -1;

                    } // p2=0/360
                    // ........................................................................
                    // p2=]0...180[

                    if ((Theta_Pel2 > 0) && (Theta_Pel2 < 180))
                    {
                        if ((Theta_Pel1 >= 0) && (Theta_Pel1 <= Theta_Pel2))
                            return -1;
                        if ((Theta_Pel1 >= 180) && (Theta_Pel1 <= 360))
                            return -1;

                    } // p2=]0...180[
                    // ........................................................................
                    // p2=180

                    if (Theta_Pel2 == 180)
                        return -1;
                    // ........................................................................
                    // p2=]180...360[

                    if ((Theta_Pel2 > 180) && (Theta_Pel2 < 360))
                    {
                        if ((Theta_Pel1 >= 0) && (Theta_Pel1 <= 180))
                            return -1;
                        if ((Theta_Pel1 >= Theta_Pel2) && (Theta_Pel1 <= 360))
                            return -1;

                    } // p2=]180...360[
                    // ........................................................................


                } // Пеленгатор2 находится ниже пеленгатора1
                // ------------------------------------------------------------------------

                return 0;

            } // Long1=Long2
            // ************************************************************ Long1=Long2

            // Az_2_1=]0...90[ *******************************************************

            if ((Az_2_1 > 0) && (Az_2_1 < 90))
            {
                // ------------------------------------------------------------------------
                // p1=0/360

                if ((Theta_Pel1 == 0) || (Theta_Pel1 == 360))
                {
                    if ((Theta_Pel2 == 0) || (Theta_Pel2 == 360))
                        return -1;
                    if ((Theta_Pel2 > 0) && (Theta_Pel2 <= 180))
                        return -1;
                    if ((Theta_Pel2 > 180) && (Theta_Pel2 <= (180 + Az_2_1)))
                        return -1;

                } // p1=0/360
                // ------------------------------------------------------------------------
                // p1=]0...B21[

                if ((Theta_Pel1 > 0) && (Theta_Pel1 < Az_2_1))
                {
                    if ((Theta_Pel2 >= Theta_Pel1) && (Theta_Pel2 <= (180 + Az_2_1)))
                        return -1;

                } // p1=]0...B21[
                // ------------------------------------------------------------------------
                // p1=B21

                if (Theta_Pel1 == Az_2_1)
                {
                    if (Theta_Pel2 == (180 + Az_2_1))
                        return 2;
                    else
                        return -1;

                } // p1=B21
                // ------------------------------------------------------------------------
                // p1=]B21...90[

                if ((Theta_Pel1 > Az_2_1) && (Theta_Pel1 < 90))
                {
                    if ((Theta_Pel2 == 0) || (Theta_Pel2 == 360))
                        return -1;
                    if ((Theta_Pel2 > 0) && (Theta_Pel2 <= Theta_Pel1))
                        return -1;
                    if ((Theta_Pel2 >= (180 + Az_2_1)) && (Theta_Pel2 < 360))
                        return -1;

                } // p1=]B21...90[
                // ------------------------------------------------------------------------
                // p1=90

                if (Theta_Pel1 == 90)
                {
                    if ((Theta_Pel2 == 0) || (Theta_Pel2 == 360))
                        return -1;
                    if ((Theta_Pel2 > 0) && (Theta_Pel2 <= 90))
                        return -1;
                    if ((Theta_Pel2 >= (180 + Az_2_1)) && (Theta_Pel2 < 360))
                        return -1;

                } // p1=90
                // ------------------------------------------------------------------------
                // p1=]90...180[

                if ((Theta_Pel1 > 90) && (Theta_Pel1 < 180))
                {
                    if ((Theta_Pel2 == 0) || (Theta_Pel2 == 360))
                        return -1;
                    if ((Theta_Pel2 > 0) && (Theta_Pel2 <= 90))
                        return -1;
                    if ((Theta_Pel2 > 90) && (Theta_Pel2 <= Theta_Pel1))
                        return -1;
                    if ((Theta_Pel2 >= (180 + Az_2_1)) && (Theta_Pel2 < 360))
                        return -1;

                } // p1=]90...180[
                // ------------------------------------------------------------------------
                // p1=180

                if (Theta_Pel1 == 180)
                {
                    if ((Theta_Pel2 == 0) || (Theta_Pel2 == 360))
                        return -1;
                    if ((Theta_Pel2 > 0) && (Theta_Pel2 <= 180))
                        return -1;
                    if ((Theta_Pel2 >= (180 + Az_2_1)) && (Theta_Pel2 < 360))
                        return -1;

                } // p1=180
                // ------------------------------------------------------------------------
                // p1=]180...270[

                if ((Theta_Pel1 > 180) && (Theta_Pel1 < 270))
                {
                    if ((Theta_Pel2 == 0) || (Theta_Pel2 == 360))
                        return -1;
                    if ((Theta_Pel2 > 0) && (Theta_Pel2 <= Theta_Pel1))
                        return -1;
                    if ((Theta_Pel2 >= (180 + Az_2_1)) && (Theta_Pel2 < 360))
                        return -1;

                } // p1=]180...270[
                // ------------------------------------------------------------------------
                // p1=270

                if (Theta_Pel1 == 270)
                {
                    if ((Theta_Pel2 == 0) || (Theta_Pel2 == 360))
                        return -1;
                    if ((Theta_Pel2 > 0) && (Theta_Pel2 <= (180 + Az_2_1)))
                        return -1;
                    if ((Theta_Pel2 >= 270) && (Theta_Pel2 < 360))
                        return -1;

                } // p1=270
                // ------------------------------------------------------------------------
                // p1=]270...360[

                if ((Theta_Pel1 > 270) && (Theta_Pel1 < 360))
                {
                    if ((Theta_Pel2 == 0) || (Theta_Pel2 == 360))
                        return -1;
                    if ((Theta_Pel2 > 0) && (Theta_Pel2 <= (180 + Az_2_1)))
                        return -1;
                    if ((Theta_Pel2 >= Theta_Pel1) && (Theta_Pel2 < 360))
                        return -1;

                } // p1=]270...360[
                // ------------------------------------------------------------------------

                return 0;

            } // Az_2_1=]0...90[
            // ******************************************************* Az_2_1=]0...90[

            // Az_2_1=]90...180[ ******************************************************

            if ((Az_2_1 > 90) && (Az_2_1 < 180))
            {

                // ------------------------------------------------------------------------
                // p1=0/360

                if ((Theta_Pel1 == 0) || (Theta_Pel1 == 360))
                {
                    if ((Theta_Pel2 == 0) || (Theta_Pel2 == 360))
                        return -1;
                    if ((Theta_Pel2 > 0) && (Theta_Pel2 <= (180 + Az_2_1)))
                        return -1;

                } // p1=0/360
                // ------------------------------------------------------------------------
                // p1=]0...90]

                if ((Theta_Pel1 > 0) && (Theta_Pel1 <= 90))
                {
                    if ((Theta_Pel2 >= Theta_Pel1) && (Theta_Pel2 <= (180 + Az_2_1)))
                        return -1;

                } // p1=]0...90]
                // ------------------------------------------------------------------------
                // p1=]90...B21[

                if ((Theta_Pel1 > 90) && (Theta_Pel1 < Az_2_1))
                {
                    if ((Theta_Pel2 >= Theta_Pel1) && (Theta_Pel2 <= (180 + Az_2_1)))
                        return -1;

                } // p1=]90...B21[
                // ------------------------------------------------------------------------
                // p1=B21

                if (Theta_Pel1 == Az_2_1)
                {
                    if (Theta_Pel2 == (180 + Az_2_1))
                        return 2;
                    else
                        return -1;

                } // p1=B21
                // ------------------------------------------------------------------------
                // p1=]B21...180]

                if ((Theta_Pel1 > Az_2_1) && (Theta_Pel1 <= 180))
                {
                    if ((Theta_Pel2 == 0) || (Theta_Pel2 == 360))
                        return -1;
                    if ((Theta_Pel2 > 0) && (Theta_Pel2 <= Theta_Pel1))
                        return -1;
                    if ((Theta_Pel2 >= (180 + Az_2_1)) && (Theta_Pel2 < 360))
                        return -1;

                } // p1=]B21...180]
                // ------------------------------------------------------------------------
                // p1=]180...270]

                if ((Theta_Pel1 > 180) && (Theta_Pel1 <= 270))
                {
                    if ((Theta_Pel2 == 0) || (Theta_Pel2 == 360))
                        return -1;
                    if ((Theta_Pel2 > 0) && (Theta_Pel2 <= Theta_Pel1))
                        return -1;
                    if ((Theta_Pel2 >= (180 + Az_2_1)) && (Theta_Pel2 < 360))
                        return -1;

                } // p1=]180...270]
                // ------------------------------------------------------------------------
                // p1=]270...360[

                if ((Theta_Pel1 > 270) && (Theta_Pel1 < 360))
                {
                    if ((Theta_Pel2 == 0) || (Theta_Pel2 == 360))
                        return -1;
                    if ((Theta_Pel2 > 0) && (Theta_Pel2 <= (180 + Az_2_1)))
                        return -1;
                    if ((Theta_Pel2 >= Theta_Pel1) && (Theta_Pel2 < 360))
                        return -1;

                } // p1=]270...360[
                // ------------------------------------------------------------------------

                return 0;

            } // Az_2_1=]90...180[
            // ****************************************************** Az_2_1=]90...180[

            // Az_2_1=]180...270[ *****************************************************

            if ((Az_2_1 > 180) && (Az_2_1 < 270))
            {
                Az_Dop = Az_2_1 - 180;

                // ------------------------------------------------------------------------
                // p1=0/360

                if ((Theta_Pel1 == 0) || (Theta_Pel1 == 360))
                {
                    if ((Theta_Pel2 == 0) || (Theta_Pel2 == 360))
                        return -1;
                    if ((Theta_Pel2 >= Az_Dop) && (Theta_Pel2 < 360))
                        return -1;

                } // p1=0/360
                // ------------------------------------------------------------------------
                // p1=]0...90[

                if ((Theta_Pel1 > 0) && (Theta_Pel1 < 90))
                {
                    if ((Theta_Pel2 == 0) || (Theta_Pel2 == 360))
                        return -1;
                    if ((Theta_Pel2 > 0) && (Theta_Pel2 <= Theta_Pel1))
                        return -1;
                    if ((Theta_Pel2 >= Az_Dop) && (Theta_Pel2 < 360))
                        return -1;

                } // p1=]0...90[
                // ------------------------------------------------------------------------
                // p1=90

                if (Theta_Pel1 == 90)
                {
                    if ((Theta_Pel2 == 0) || (Theta_Pel2 == 360))
                        return -1;
                    if ((Theta_Pel2 > 0) && (Theta_Pel2 <= Az_Dop))
                        return -1;
                    if ((Theta_Pel2 >= 90) && (Theta_Pel2 < 360))
                        return -1;

                } // p1=90
                // ------------------------------------------------------------------------
                // p1=]90...180]

                if ((Theta_Pel1 > 90) && (Theta_Pel1 <= 180))
                {
                    if ((Theta_Pel2 == 0) || (Theta_Pel2 == 360))
                        return -1;
                    if ((Theta_Pel2 > 0) && (Theta_Pel2 <= Az_Dop))
                        return -1;
                    if ((Theta_Pel2 >= Theta_Pel1) && (Theta_Pel2 < 360))
                        return -1;

                } // p1=]90...180]
                // ------------------------------------------------------------------------
                // p1=]180...(180+Az_Dop)[

                if ((Theta_Pel1 > 180) && (Theta_Pel1 < (180 + Az_Dop)))
                {
                    if ((Theta_Pel2 == 0) || (Theta_Pel2 == 360))
                        return -1;
                    if ((Theta_Pel2 > 0) && (Theta_Pel2 <= Az_Dop))
                        return -1;
                    if ((Theta_Pel2 >= Theta_Pel1) && (Theta_Pel2 < 360))
                        return -1;

                } // p1=]180...(180+Az_Dop)[
                // ------------------------------------------------------------------------
                // p1=180+Az_Dop

                if (Theta_Pel1 == 180 + Az_Dop)
                {
                    if (Theta_Pel2 == Az_Dop)
                        return 2;
                    else
                        return -1;

                } // p1=180+Az_Dop
                // ------------------------------------------------------------------------
                // p1=](180+Az_Dop)...270]

                if ((Theta_Pel1 > (180 + Az_Dop)) && (Theta_Pel1 <= 270))
                {
                    if ((Theta_Pel2 >= Az_Dop) && (Theta_Pel2 <= Theta_Pel1))
                        return -1;

                } // p1=](180+Az_Dop)...270]
                // ------------------------------------------------------------------------
                // p1=]270...360[

                if ((Theta_Pel1 > 270) && (Theta_Pel1 <= 360))
                {
                    //if ((Theta_Pel2 >= Az_Dop) && (Theta_Pel2 < 360))
                    if ((Theta_Pel2 >= Az_Dop) && (Theta_Pel2 <=Theta_Pel1))
                            return -1;

                } // p1=]270...360[
                // ------------------------------------------------------------------------

                return 0;

            } // Az_2_1=]180...270[
            // ***************************************************** Az_2_1=]180...270[

            // Az_2_1=]270...360[ ******************************************************

            if ((Az_2_1 > 270) && (Az_2_1 < 360))
            {
                Az_Dop = Az_2_1 - 270;

                // ------------------------------------------------------------------------
                // p1=0/360

                if ((Theta_Pel1 == 0) || (Theta_Pel1 == 360))
                {
                    if ((Theta_Pel2 == 0) || (Theta_Pel2 == 360))
                        return -1;
                    if ((Theta_Pel2 >= (90 + Az_Dop)) && (Theta_Pel2 < 360))
                        return -1;

                } // p1=0/360
                // ------------------------------------------------------------------------
                // p1=]0...(90+Az_Dop)[

                if ((Theta_Pel1 > 0) && (Theta_Pel1 < (90 + Az_Dop)))
                {
                    if ((Theta_Pel2 == 0) || (Theta_Pel2 == 360))
                        return -1;
                    if ((Theta_Pel2 > 0) && (Theta_Pel2 <= Theta_Pel1))
                        return -1;
                    if ((Theta_Pel2 >= (90 + Az_Dop)) && (Theta_Pel2 < 360))
                        return -1;

                } // p1=]0...(90+Az_Dop)[
                // ------------------------------------------------------------------------
                // p1=90+Az_Dop

                if (Theta_Pel1 == (90 + Az_Dop))
                    return -1;
                // ------------------------------------------------------------------------
                // p1=](90+Az_Dop)...(270+Az_Dop)[

                if ((Theta_Pel1 > (90 + Az_Dop)) && (Theta_Pel1 < (270 + Az_Dop)))
                {
                    if ((Theta_Pel2 == 0) || (Theta_Pel2 == 360))
                        return -1;
                    if ((Theta_Pel2 > 0) && (Theta_Pel2 <= (90 + Az_Dop)))
                        return -1;
                    if ((Theta_Pel2 >= Theta_Pel1) && (Theta_Pel2 < 360))
                        return -1;

                } // p1=](90+Az_Dop)...(270+Az_Dop)[
                // ------------------------------------------------------------------------
                // p1=270+Az_Dop

                if (Theta_Pel1 == 270 + Az_Dop)
                {
                    if (Theta_Pel2 == (90 + Az_Dop))
                        return 2;
                    else
                        return -1;

                } // p1=270+Az_Dop
                // ------------------------------------------------------------------------
                // p1=](270+Az_Dop)...360[

                if ((Theta_Pel1 > (270 + Az_Dop)) && (Theta_Pel1 < 360))
                {
                    if ((Theta_Pel2 >= (90 + Az_Dop)) && (Theta_Pel2 <= Theta_Pel1))
                        return -1;

                } // p1=](270+Az_Dop)...360[
                // ------------------------------------------------------------------------

                return 0;

            } // Az_2_1=]270...360[
            // ****************************************************** Az_2_1=]270...360[


            // *********************************************************** SOVMEST_PEL

            return 0;

        } // Функция f_Bearing_Compatibility
        //**************************************************************************

        // *************************************************************************
        // Расчет азимута одного объекта относительно другого
        // dY,dX - разность координат на плоскости XOY (dX=X2-X1...)
        // Условно считаем Y - направление на север

        // !!! В СК42 X идет вверх (север), Y вправо (восток) -> dY=X2-X1(CK42)
        //     UTM - наоборот

        // Возврат: азимут в градусах (0...360 по часовой стрелке)
        // *************************************************************************

        public static double f_Def_Azimuth(
                                   double dY,
                                   double dX
                                      )
        {
            double Beta, Beta_tmp;

            Beta = 0;
            Beta_tmp = 0;
            // ------------------------------------------------------------------------
            if (dY != 0)
                Beta_tmp = Math.Atan(module(dX) / module(dY));
            // -------------------------------------------------------------------------
            if ((dX == 0) && (dY >= 0))
                Beta = 0;
            // -------------------------------------------------------------------------
            else if ((dX == 0) && (dY < 0))
                Beta = Math.PI;
            // ------------------------------------------------------------------------
            else if ((dY == 0) && (dX > 0))
                Beta = Math.PI / 2;
            // -------------------------------------------------------------------------
            else if ((dY == 0) && (dX < 0))
                Beta = 3 * Math.PI / 2;
            // -------------------------------------------------------------------------
            else if ((dY > 0) && (dX > 0))
                Beta = Beta_tmp;
            // -------------------------------------------------------------------------
            else if ((dY > 0) && (dX < 0))
                Beta = 2 * Math.PI - Beta_tmp;
            // -------------------------------------------------------------------------
            else if ((dY < 0) && (dX > 0))
                Beta = Math.PI - Beta_tmp;
            // -------------------------------------------------------------------------
            else if ((dY < 0) && (dX < 0))
                Beta = Math.PI + Beta_tmp;
            // -------------------------------------------------------------------------
            // Перевод в градусы

            Beta = (Beta * 180) / Math.PI;
            // -------------------------------------------------------------------------

            return Beta;

        } // P/P f_Def_Azimuth
        // *************************************************************************

        // ***********************************************************************
        // Расстояние между двумя точками с учетом кривизны Земли

        // Входные параметры: 

        // Lat1,Long1,Lat2,Long2 (град) - широта и долгота точек
        // fl_84_42=1-> долгота и широта в WGS84, =2 -> SK42

        // Выходные параметры: 
        // возвращает расстояние в м

        // Широта -90(юг)...+90(север)
        // Долгота -180(на запад от Гринвича)...+180(на восток от Гринвича)
        // ***********************************************************************

        public static double f_D_2Points

            (
            // Широта и долгота стояния точек (град)
                 double Lat1,
                 double Long1,
                 double Lat2,
                 double Long2,

                 int fl_84_42

            )
        {

            // -----------------------------------------------------------------------
            double REarth = 0;
            double REarth_Lat = 0;
            double LatSr = 0;
            double LatRad1 = 0;
            double LatRad2 = 0;
            double LongRad1 = 0;
            double LongRad2 = 0;
            double dLat = 0;
            double dLong = 0;
            double dq = 0;
            double s = 0;
            // -----------------------------------------------------------------------
            // Средний радиус Земли (шар)

            if (fl_84_42 == 2) // Красовский
                // SSS1
                //REarth = 6371220;
                REarth = 6371117.673;

            else
            {
                REarth = 6378136.3; // WGS84
                //RRR
                // SSS1
                //REarth = 6371008.8; // WGS84
                REarth = 6371008.771333; // WGS84

            }
            // -----------------------------------------------------------------------
            // Широта и долгота (rad)

            LatRad1 = (Lat1 * Math.PI) / 180;   // grad->rad
            LatRad2 = (Lat2 * Math.PI) / 180;
            LongRad1 = (Long1 * Math.PI) / 180;
            LongRad2 = (Long2 * Math.PI) / 180;
            dLat = LatRad2 - LatRad1;
            dLong = LongRad2 - LongRad1;
            LatSr = (LatRad1 + LatRad2) / 2;
            // -----------------------------------------------------------------------
            // Радиус Земли  в точке стояния пеленгатора с учетом текущей широты 

            // m  Old
            //REarthP_Pel = REarth_Pel; // Шар

            // Сжатие=(a-b)/a
            // 0.003352 -Красовский
            if (fl_84_42 == 2) // Красовский
            {
                REarth_Lat = REarth * (1 - 0.003352 * Math.Sin(LatSr) * Math.Sin(LatSr));

                // SSS1
                REarth_Lat = 6371117.673;

            }
            else
            {
                REarth_Lat = REarth * (1 - 0.00669438 * Math.Sin(LatSr) * Math.Sin(LatSr)); // WGS84
                // RRR
                // SSS1
                //REarth_Lat = 6371008.8;
                REarth_Lat = 6371008.771333;

            }
            // -----------------------------------------------------------------------
            // 1-й вариант

            dq = 2 * Math.Asin(Math.Sqrt(Math.Sin(dLat / 2) * Math.Sin(dLat / 2) +
                                         Math.Cos(LatRad1) * Math.Cos(LatRad2) * Math.Sin(dLong / 2) * Math.Sin(dLong / 2)));
            //s = REarth_Lat* dq;
            // -----------------------------------------------------------------------
            // !!! 2-й вариант

            dq = Math.Atan(Math.Sqrt((Math.Cos(LatRad2) * Math.Sin(dLong)) * (Math.Cos(LatRad2) * Math.Sin(dLong)) +
                                     (Math.Cos(LatRad1) * Math.Sin(LatRad2) - Math.Sin(LatRad1) * Math.Cos(LatRad2) * Math.Cos(dLong)) *
                                     (Math.Cos(LatRad1) * Math.Sin(LatRad2) - Math.Sin(LatRad1) * Math.Cos(LatRad2) * Math.Cos(dLong))) /
                           (Math.Sin(LatRad1) * Math.Sin(LatRad2) + Math.Cos(LatRad1) * Math.Cos(LatRad2) * Math.Cos(dLong)));
            // -----------------------------------------------------------------------
            // Otl
            //REarth_Lat = REarth * (1 - 0.00669438 * Math.Sin(LatRad1) * Math.Sin(LatRad1));


            // !!! ONlineCalculator
            // RRR
            // SSS1
            //REarth_Lat = 6371008.8;
            //REarth_Lat = 6371008.771333;

            s = REarth_Lat * dq;
            // -----------------------------------------------------------------------

            return s;

        } // Функция f_D_2Points
        //**************************************************************************

        // ***********************************************************************
        // !!! Вариант4 

        // Расчет координат точки пересечения двух пеленгов
        // !!! Все параметры в градусах WGS84

        // Входные параметры:
        // List<JamBearing> JamSt
        // JamSt[i].Bearing -> пеленг
        // JamSt[i].Coordinate.Latitude -> широта
        // JamSt[i].Coordinate.Longitude -> долгота

        // Выходные параметры:
        // Coord object
        // Coord.Latitude, Coord.Longitude -  координаты точки пересечения пеленгов

        // Широта -90(юг)...+90(север)
        // Долгота -180(на запад от Гринвича)...+180(на восток от Гринвича)
        // Пеленг 0...360(по часовой стрелке)
        // ***********************************************************************

        public static Coord DefineCoordTriang

            (
             List<JamBearing> JamSt
            )
        {
            // ------------------------------------------------------------------------
            Coord JamStObj = new Coord();
            // ------------------------------------------------------------------------
            // координаты искомого ИРИ в опорной геоцентрической СК(град)

            double XIRI_Tr = 0;
            double YIRI_Tr = 0;
            double ZIRI_Tr = 0;
            double RIRI_Tr = 0;  // м
            // ------------------------------------------------------------------------
            // Расчетные коэффициенты
            double K1_Tr = 0;
            double K2_Tr = 0;
            double K3_Tr = 0;

            // Коэффициенты канонического уравнения для плоскости пеленгования ИРИ
            // i-ым пеленгатором 
            double A1_Tr = 0; // Для пеленгатора1
            double T1_Tr = 0;
            double C1_Tr = 0;
            double A2_Tr = 0; // Для пеленгатора2
            double T2_Tr = 0;
            double C2_Tr = 0;

            // Координаты искомого ИРИ в опорной геоцентрической СК
            double XIRI1_Tr = 0;
            double XIRI2_Tr = 0;
            double YIRI1_Tr = 0;
            double YIRI2_Tr = 0;
            double ZIRI1_Tr = 0;
            double ZIRI2_Tr = 0;

            // Координаты середины отрезка, соединяющего N-ые фиктивные точки
            double XM_Tr = 0;
            double YM_Tr = 0;
            double ZM_Tr = 0;

            // Расстояние до ПП
            double L1_Tr = 0;
            double L2_Tr = 0;

            // Координаты пеленгатора в опорной геоцентрической СК
            double XP1_Tr_tmp = 0;
            double YP1_Tr_tmp = 0;
            double ZP1_Tr_tmp = 0;
            double XP2_Tr_tmp = 0;
            double YP2_Tr_tmp = 0;
            double ZP2_Tr_tmp = 0;

            // Координаты N-й фиктивной точки в опорной геоцентрической СК
            double XFN1_Tr_tmp = 0;
            double YFN1_Tr_tmp = 0;
            double ZFN1_Tr_tmp = 0;
            double XFN2_Tr_tmp = 0;
            double YFN2_Tr_tmp = 0;
            double ZFN2_Tr_tmp = 0;

            // Радиус Земли (шар)
            double REarth_Pel = 0;

            // Координаты пеленгатора в опорной геоцентрической СК
            double XP1_Tr = 0;
            double YP1_Tr = 0;
            double ZP1_Tr = 0;
            double XP2_Tr = 0;
            double YP2_Tr = 0;
            double ZP2_Tr = 0;
            // Координаты N-й фиктивной точки в опорной геоцентрической СК
            double XFN1_Tr = 0;
            double YFN1_Tr = 0;
            double ZFN1_Tr = 0;
            double XFN2_Tr = 0;
            double YFN2_Tr = 0;
            double ZFN2_Tr = 0;

            double[] arr_Pel1 = new double[GlobalVarLn.numberofdots * 2];
            double[] arr_Pel2 = new double[GlobalVarLn.numberofdots * 2];

            double[] arr_Pel_XYZ1 = new double[GlobalVarLn.numberofdots * 3];
            double[] arr_Pel_XYZ2 = new double[GlobalVarLn.numberofdots * 3];
            // ------------------------------------------------------------------------
            double lll = 0;
            lll = ((double)JamSt[0].Coordinate.Latitude + (double)JamSt[1].Coordinate.Latitude) / 2;
            lll = (lll * Math.PI) / 180;

            // Средний радиус Земли (шар)
            if (GlobalVarLn.fl_84_42 == 2) // Красовский
            {
                REarth_Pel = 6371220;

                // SSS1
                REarth_Pel = 6371117.673;
            }
            else
            {
                REarth_Pel = 6378136.3; // WGS84

                // SSS1
                REarth_Pel = 6371008.771333;

            }

            // Сжатие=(a-b)/a
            // 0.003352 -Красовский
            if (GlobalVarLn.fl_84_42 == 2) // Красовский
            {
                REarth_Pel = REarth_Pel * (1 - 0.003352 * Math.Sin(lll) * Math.Sin(lll));

                // SSS1
                REarth_Pel = 6371117.673;
            }
            else
            {
                REarth_Pel = REarth_Pel * (1 - 0.00669438 * Math.Sin(lll) * Math.Sin(lll)); // WGS84
                // RRR
                //REarth_Pel = 6371008.8;

                // SSS1
                REarth_Pel = 6371008.771333;

            }
            // ------------------------------------------------------------------------
            // Расчет траектории фиктивных точек по пеленгу1

            f_Bearing
             (
                  // Пеленг (град)
                  (double)JamSt[0].Bearing,
                  // Max дальность отображения пеленга (m)
                  GlobalVarLn.distance,
                  // Количество фиктивных точек в плоскости пеленгования пеленгатора
                  GlobalVarLn.numberofdots,

                  // Широта и долгота стояния пеленгатора (град)
                  (double)JamSt[0].Coordinate.Latitude,
                  (double)JamSt[0].Coordinate.Longitude,

                  GlobalVarLn.fl_84_42,

                  // Координаты пеленгатора в опорной геоцентрической СК
                  ref XP1_Tr,
                  ref YP1_Tr,
                  ref ZP1_Tr,

                  // Координаты N-й фиктивной точки в опорной геоцентрической СК
                  ref XFN1_Tr,
                  ref YFN1_Tr,
                  ref ZFN1_Tr,

                  // Координаты фиктивных точек
                  ref arr_Pel1,
                  ref arr_Pel_XYZ1

             );

            // ------------------------------------------------------------------------
            // Расчет траектории фиктивных точек по пеленгу2

            f_Bearing
             (
                  // Пеленг (град)
                  (double)JamSt[1].Bearing,
                  // Max дальность отображения пеленга (m)
                  GlobalVarLn.distance,
                  // Количество фиктивных точек в плоскости пеленгования пеленгатора
                  GlobalVarLn.numberofdots,

                  // Широта и долгота стояния пеленгатора (град)
                  (double)JamSt[1].Coordinate.Latitude,
                  (double)JamSt[1].Coordinate.Longitude,

                  GlobalVarLn.fl_84_42,

                  // Координаты пеленгатора в опорной геоцентрической СК
                  ref XP2_Tr,
                  ref YP2_Tr,
                  ref ZP2_Tr,

                  // Координаты N-й фиктивной точки в опорной геоцентрической СК
                  ref XFN2_Tr,
                  ref YFN2_Tr,
                  ref ZFN2_Tr,

                  // Координаты фиктивных точек
                  ref arr_Pel2,
                  ref arr_Pel_XYZ2

             );
            // ------------------------------------------------------------------------

            // SOVMEST_PEL ***********************************************************
            // Проверка совместимости пеленгов

            // ------------------------------------------------------------------------
            int fls = 0;

            fls = f_Bearing_Compatibility
            (
                 // Входные параметры:
                 // Пеленг (град)
                 (double)JamSt[0].Bearing,
                 (double)JamSt[1].Bearing,
                 // Широта и долгота стояния пеленгатора (град)
                 (double)JamSt[0].Coordinate.Latitude,
                 (double)JamSt[0].Coordinate.Longitude,
                 (double)JamSt[1].Coordinate.Latitude,
                 (double)JamSt[1].Coordinate.Longitude,
                 //Smax,
                 GlobalVarLn.distance_pel,
                 GlobalVarLn.fl_84_42
            );
            // ------------------------------------------------------------------------
            if (fls == -1)
            {
                MessageBox.Show("Несовместимые пеленги");
                JamStObj.Latitude = -1;
                JamStObj.Longitude = -1;
                return JamStObj;
            }
            if (fls == -2)
            {
                MessageBox.Show("Недопустимые параметры");
                JamStObj.Latitude = -1;
                JamStObj.Longitude = -1;
                return JamStObj;
            }
            // ------------------------------------------------------------------------
            // Решение на середине отрезка, соединяющего станции

            if (fls == 2)
            {
                // Угловые координаты искомого ИРИ в опорной геоцентрической СК
                JamStObj.Latitude = ((double)JamSt[0].Coordinate.Latitude + (double)JamSt[1].Coordinate.Latitude) / 2;
                JamStObj.Longitude = ((double)JamSt[0].Coordinate.Longitude * (double)JamSt[1].Coordinate.Longitude) / 2;
                return JamStObj;

            } // fls=2
            // ------------------------------------------------------------------------

            // *********************************************************** SOVMEST_PEL

            // Initial conditions ****************************************************
            // Initial conditions
            // м 

            XP1_Tr_tmp = XP1_Tr;
            YP1_Tr_tmp = YP1_Tr;
            ZP1_Tr_tmp = ZP1_Tr;
            XP2_Tr_tmp = XP2_Tr;
            YP2_Tr_tmp = YP2_Tr;
            ZP2_Tr_tmp = ZP2_Tr;

            XFN1_Tr_tmp = XFN1_Tr;
            YFN1_Tr_tmp = YFN1_Tr;
            ZFN1_Tr_tmp = ZFN1_Tr;
            XFN2_Tr_tmp = XFN2_Tr;
            YFN2_Tr_tmp = YFN2_Tr;
            ZFN2_Tr_tmp = ZFN2_Tr;

            // **************************************************** Initial conditions

            // A,T,C *****************************************************************
            // Расчет коэффициентов пллоскости пеленгования

            A1_Tr = YP1_Tr_tmp * (ZFN1_Tr_tmp) - ZP1_Tr_tmp * (YFN1_Tr_tmp);
            T1_Tr = ZP1_Tr_tmp * (XFN1_Tr_tmp) - XP1_Tr_tmp * (ZFN1_Tr_tmp);
            C1_Tr = XP1_Tr_tmp * (YFN1_Tr_tmp) - YP1_Tr_tmp * (XFN1_Tr_tmp);

            A2_Tr = YP2_Tr_tmp * (ZFN2_Tr_tmp) - ZP2_Tr_tmp * (YFN2_Tr_tmp);
            T2_Tr = ZP2_Tr_tmp * (XFN2_Tr_tmp) - XP2_Tr_tmp * (ZFN2_Tr_tmp);
            C2_Tr = XP2_Tr_tmp * (YFN2_Tr_tmp) - YP2_Tr_tmp * (XFN2_Tr_tmp);

            // ***************************************************************** A,T,C

            // C1!=0 11111111111111111111111111111111111111111111111111111111111111111
            if (C1_Tr != 0)
            {
                // .......................................................................
                if (module(C1_Tr) < 1E-10)
                {
                    K1_Tr = -(A1_Tr / 1E-10);
                    K2_Tr = -(T1_Tr / 1E-10);
                }

                else
                {
                    K1_Tr = -(A1_Tr / C1_Tr);
                    K2_Tr = -(T1_Tr / C1_Tr);
                }

                if (module(T1_Tr * C2_Tr - C1_Tr * T2_Tr) < 1E-10)
                    K3_Tr = (C1_Tr * A2_Tr - A1_Tr * C2_Tr) / 1E-10;
                else
                    K3_Tr = (C1_Tr * A2_Tr - A1_Tr * C2_Tr) / (T1_Tr * C2_Tr - C1_Tr * T2_Tr);
                // .......................................................................
                // Координаты искомого ИРИ в опорной геоцентрической СК

                if (module(Math.Sqrt(1 + K3_Tr * K3_Tr + (K1_Tr + K2_Tr * K3_Tr) * (K1_Tr + K2_Tr * K3_Tr))) < 1E-10)
                {
                    XIRI1_Tr = REarth_Pel / 1E-10;
                    XIRI2_Tr = -REarth_Pel / 1E-10;
                }
                else
                {
                    XIRI1_Tr = REarth_Pel / Math.Sqrt(1 + K3_Tr * K3_Tr + (K1_Tr + K2_Tr * K3_Tr) * (K1_Tr + K2_Tr * K3_Tr));
                    XIRI2_Tr = -REarth_Pel / Math.Sqrt(1 + K3_Tr * K3_Tr + (K1_Tr + K2_Tr * K3_Tr) * (K1_Tr + K2_Tr * K3_Tr));
                }

                YIRI1_Tr = K3_Tr * XIRI1_Tr;
                YIRI2_Tr = K3_Tr * XIRI2_Tr;

                ZIRI1_Tr = K1_Tr * XIRI1_Tr + K2_Tr * YIRI1_Tr;
                ZIRI2_Tr = K1_Tr * XIRI2_Tr + K2_Tr * YIRI2_Tr;
                // .......................................................................

            } // C1!=0
            // 11111111111111111111111111111111111111111111111111111111111111111 C1!=0

            // C1=0 222222222222222222222222222222222222222222222222222222222222222222
            else
            {
                // .......................................................................
                if (module(T1_Tr) < 1E-10)
                    K3_Tr = -A1_Tr / 1E-10;
                else
                    K3_Tr = -A1_Tr / T1_Tr;

                if (module(C2_Tr) < 1E-10)
                    K2_Tr = -(A2_Tr + K3_Tr * T2_Tr) / 1E-10;
                else
                    K2_Tr = -(A2_Tr + K3_Tr * T2_Tr) / C2_Tr;

                // .......................................................................
                // Координаты искомого ИРИ в опорной геоцентрической СК

                if (module(Math.Sqrt(1 + K2_Tr * K2_Tr + K3_Tr * K3_Tr)) < 1E-10)
                {
                    XIRI1_Tr = REarth_Pel / 1E-10;
                    XIRI2_Tr = -REarth_Pel / 1E-10;
                }

                else
                {
                    XIRI1_Tr = REarth_Pel / Math.Sqrt(1 + K2_Tr * K2_Tr + K3_Tr * K3_Tr);
                    XIRI2_Tr = -REarth_Pel / Math.Sqrt(1 + K2_Tr * K2_Tr + K3_Tr * K3_Tr);
                }

                YIRI1_Tr = K3_Tr * XIRI1_Tr;
                YIRI2_Tr = K3_Tr * XIRI2_Tr;

                ZIRI1_Tr = K2_Tr * XIRI1_Tr;
                ZIRI2_Tr = K2_Tr * XIRI2_Tr;
                // .......................................................................

            } // C1=0
            // 222222222222222222222222222222222222222222222222222222222222222222 C1=0

            // XIRI_Tr,YIRI_Tr,ZIRI_Tr ***********************************************
            // Критерий выбора координат искомого ИРИ в опорной геоцентрической СК
            // (минимальное расстояние до ПП)

            // Координаты середины отрезка, соединяющего N-ые фиктивные точки
            DefMiddle_Pel(XFN1_Tr_tmp, XFN2_Tr_tmp, ref XM_Tr);
            DefMiddle_Pel(YFN1_Tr_tmp, YFN2_Tr_tmp, ref YM_Tr);
            DefMiddle_Pel(ZFN1_Tr_tmp, ZFN2_Tr_tmp, ref ZM_Tr);

            L1_Tr = Math.Sqrt((XM_Tr - XIRI1_Tr) * (XM_Tr - XIRI1_Tr) +
                              (YM_Tr - YIRI1_Tr) * (YM_Tr - YIRI1_Tr) +
                              (ZM_Tr - ZIRI1_Tr) * (ZM_Tr - ZIRI1_Tr));

            L2_Tr = Math.Sqrt((XM_Tr - XIRI2_Tr) * (XM_Tr - XIRI2_Tr) +
                              (YM_Tr - YIRI2_Tr) * (YM_Tr - YIRI2_Tr) +
                              (ZM_Tr - ZIRI2_Tr) * (ZM_Tr - ZIRI2_Tr));

            if (L1_Tr < L2_Tr)
            {
                XIRI_Tr = XIRI1_Tr;
                YIRI_Tr = YIRI1_Tr;
                ZIRI_Tr = ZIRI1_Tr;
            }
            else
            {
                XIRI_Tr = XIRI2_Tr;
                YIRI_Tr = YIRI2_Tr;
                ZIRI_Tr = ZIRI2_Tr;
            }
            // *********************************************** XIRI_Tr,YIRI_Tr,ZIRI_Tr

            // RIRI_Tr,LatIRI_Tr,LongIRI_Tr ******************************************
            // Угловые координаты искомого ИРИ в опорной геоцентрической СК

            // !!! Д.б. *pRIRI_Tr==REarth_Pel, т.е. r=Rз
            RIRI_Tr = Math.Sqrt(XIRI_Tr * XIRI_Tr + YIRI_Tr * YIRI_Tr + ZIRI_Tr * ZIRI_Tr);

            JamStObj.Latitude = Math.Asin(ZIRI_Tr / RIRI_Tr);

            double tmp1 = 0;
            Def_Longitude_180(
                          XIRI_Tr,
                          YIRI_Tr,
                          RIRI_Tr,
                          JamStObj.Latitude,
                          ref tmp1
                         );

            JamStObj.Longitude = tmp1;
            // ****************************************** RIRI_Tr,LatIRI_Tr,LongIRI_Tr

            // *********************************************************************
            // Перевод в град

            // Угловые координаты искомого ИРИ в опорной геоцентрической СК
            JamStObj.Latitude = (JamStObj.Latitude * 180) / Math.PI;
            JamStObj.Longitude = (JamStObj.Longitude * 180) / Math.PI;

            // *********************************************************************







            // ????????????????????????????????????????????????????????????????????????????????????????


/*
            // Уточнение координат ********************************************************************
            // Уточнение координат 

            double sss1 = 0;
            double sss2 = 0;

            // 
            sss1 = f_D_2Points
           (
             // Широта и долгота стояния точек (град)
             LatP_Pel1,
             LongP_Pel1,
             LatIRI_Tr,
             LongIRI_Tr,
             1
           );

            sss2 = f_D_2Points
           (
             // Широта и долгота стояния точек (град)
             LatP_Pel2,
             LongP_Pel2,
             LatIRI_Tr,
             LongIRI_Tr,
             1
           );

            // ------------------------------------------------------------------------
            // Расчет траектории фиктивных точек по пеленгу1

            f_Bearing
             (

                                   // Пеленг (град)
                                   Theta_Pel1,
                                   // Max дальность отображения пеленга (m)
                                   sss1,
                                   // Количество фиктивных точек в плоскости пеленгования пеленгатора
                                   NumbFikt_Pel1,

                                   // Широта и долгота стояния пеленгатора (град)
                                   LatP_Pel1,
                                   LongP_Pel1,

                                   fl_84_42,

                                   // Координаты пеленгатора в опорной геоцентрической СК
                                   ref XP1_Tr,
                                   ref YP1_Tr,
                                   ref ZP1_Tr,

                                   // Координаты N-й фиктивной точки в опорной геоцентрической СК
                                   ref XFN1_Tr,
                                   ref YFN1_Tr,
                                   ref ZFN1_Tr,

                                   // Координаты фиктивных точек
                                   ref arr_Pel1,
                                   ref arr_Pel_XYZ1


             );

            // ------------------------------------------------------------------------
            // Расчет траектории фиктивных точек по пеленгу2

            f_Bearing
             (

                                   // Пеленг (град)
                                   Theta_Pel2,
                                   // Max дальность отображения пеленга (m)
                                   sss2,
                                   // Количество фиктивных точек в плоскости пеленгования пеленгатора
                                   NumbFikt_Pel1,

                                   // Широта и долгота стояния пеленгатора (град)
                                   LatP_Pel2,
                                   LongP_Pel2,

                                   fl_84_42,

                                   // Координаты пеленгатора в опорной геоцентрической СК
                                   ref XP2_Tr,
                                   ref YP2_Tr,
                                   ref ZP2_Tr,

                                   // Координаты N-й фиктивной точки в опорной геоцентрической СК
                                   ref XFN2_Tr,
                                   ref YFN2_Tr,
                                   ref ZFN2_Tr,

                                   // Координаты фиктивных точек
                                   ref arr_Pel2,
                                   ref arr_Pel_XYZ2


             );
            // ------------------------------------------------------------------------


            // Initial conditions ****************************************************
            // Initial conditions
            // м 

            XP1_Tr_tmp = XP1_Tr;
            YP1_Tr_tmp = YP1_Tr;
            ZP1_Tr_tmp = ZP1_Tr;
            XP2_Tr_tmp = XP2_Tr;
            YP2_Tr_tmp = YP2_Tr;
            ZP2_Tr_tmp = ZP2_Tr;

            XFN1_Tr_tmp = XFN1_Tr;
            YFN1_Tr_tmp = YFN1_Tr;
            ZFN1_Tr_tmp = ZFN1_Tr;
            XFN2_Tr_tmp = XFN2_Tr;
            YFN2_Tr_tmp = YFN2_Tr;
            ZFN2_Tr_tmp = ZFN2_Tr;

            // **************************************************** Initial conditions

            // A,T,C *****************************************************************
            // Расчет коэффициентов пллоскости пеленгования

            A1_Tr = YP1_Tr_tmp * (ZFN1_Tr_tmp) - ZP1_Tr_tmp * (YFN1_Tr_tmp);
            T1_Tr = ZP1_Tr_tmp * (XFN1_Tr_tmp) - XP1_Tr_tmp * (ZFN1_Tr_tmp);
            C1_Tr = XP1_Tr_tmp * (YFN1_Tr_tmp) - YP1_Tr_tmp * (XFN1_Tr_tmp);

            A2_Tr = YP2_Tr_tmp * (ZFN2_Tr_tmp) - ZP2_Tr_tmp * (YFN2_Tr_tmp);
            T2_Tr = ZP2_Tr_tmp * (XFN2_Tr_tmp) - XP2_Tr_tmp * (ZFN2_Tr_tmp);
            C2_Tr = XP2_Tr_tmp * (YFN2_Tr_tmp) - YP2_Tr_tmp * (XFN2_Tr_tmp);

            // ***************************************************************** A,T,C

            // C1!=0 11111111111111111111111111111111111111111111111111111111111111111
            if (C1_Tr != 0)
            {
                // .......................................................................

                if (module(C1_Tr) < 1E-10)
                {
                    K1_Tr = -(A1_Tr / 1E-10);
                    K2_Tr = -(T1_Tr / 1E-10);
                }

                else
                {
                    K1_Tr = -(A1_Tr / C1_Tr);
                    K2_Tr = -(T1_Tr / C1_Tr);
                }

                if (module(T1_Tr * C2_Tr - C1_Tr * T2_Tr) < 1E-10)
                    K3_Tr = (C1_Tr * A2_Tr - A1_Tr * C2_Tr) / 1E-10;
                else
                    K3_Tr = (C1_Tr * A2_Tr - A1_Tr * C2_Tr) / (T1_Tr * C2_Tr - C1_Tr * T2_Tr);
                // .......................................................................
                // Координаты искомого ИРИ в опорной геоцентрической СК

                if (module(Math.Sqrt(1 + K3_Tr * K3_Tr + (K1_Tr + K2_Tr * K3_Tr) * (K1_Tr + K2_Tr * K3_Tr))) < 1E-10)
                {
                    XIRI1_Tr = REarth_Pel / 1E-10;
                    XIRI2_Tr = -REarth_Pel / 1E-10;
                }
                else
                {
                    XIRI1_Tr = REarth_Pel / Math.Sqrt(1 + K3_Tr * K3_Tr + (K1_Tr + K2_Tr * K3_Tr) * (K1_Tr + K2_Tr * K3_Tr));
                    XIRI2_Tr = -REarth_Pel / Math.Sqrt(1 + K3_Tr * K3_Tr + (K1_Tr + K2_Tr * K3_Tr) * (K1_Tr + K2_Tr * K3_Tr));
                }

                YIRI1_Tr = K3_Tr * XIRI1_Tr;
                YIRI2_Tr = K3_Tr * XIRI2_Tr;

                ZIRI1_Tr = K1_Tr * XIRI1_Tr + K2_Tr * YIRI1_Tr;
                ZIRI2_Tr = K1_Tr * XIRI2_Tr + K2_Tr * YIRI2_Tr;
                // .......................................................................

            } // C1!=0
            // 11111111111111111111111111111111111111111111111111111111111111111 C1!=0

            // C1=0 222222222222222222222222222222222222222222222222222222222222222222
            else
            {
                // .......................................................................
                if (module(T1_Tr) < 1E-10)
                    K3_Tr = -A1_Tr / 1E-10;
                else
                    K3_Tr = -A1_Tr / T1_Tr;

                if (module(C2_Tr) < 1E-10)
                    K2_Tr = -(A2_Tr + K3_Tr * T2_Tr) / 1E-10;
                else
                    K2_Tr = -(A2_Tr + K3_Tr * T2_Tr) / C2_Tr;

                // .......................................................................
                // Координаты искомого ИРИ в опорной геоцентрической СК

                if (module(Math.Sqrt(1 + K2_Tr * K2_Tr + K3_Tr * K3_Tr)) < 1E-10)
                {
                    XIRI1_Tr = REarth_Pel / 1E-10;
                    XIRI2_Tr = -REarth_Pel / 1E-10;
                }

                else
                {
                    XIRI1_Tr = REarth_Pel / Math.Sqrt(1 + K2_Tr * K2_Tr + K3_Tr * K3_Tr);
                    XIRI2_Tr = -REarth_Pel / Math.Sqrt(1 + K2_Tr * K2_Tr + K3_Tr * K3_Tr);
                }

                YIRI1_Tr = K3_Tr * XIRI1_Tr;
                YIRI2_Tr = K3_Tr * XIRI2_Tr;

                ZIRI1_Tr = K2_Tr * XIRI1_Tr;
                ZIRI2_Tr = K2_Tr * XIRI2_Tr;
                // .......................................................................

            } // C1=0
            // 222222222222222222222222222222222222222222222222222222222222222222 C1=0

            // XIRI_Tr,YIRI_Tr,ZIRI_Tr ***********************************************
            // Критерий выбора координат искомого ИРИ в опорной геоцентрической СК
            // (минимальное расстояние до ПП)

            // Координаты середины отрезка, соединяющего N-ые фиктивные точки
            DefMiddle_Pel(XFN1_Tr_tmp, XFN2_Tr_tmp, ref XM_Tr);
            DefMiddle_Pel(YFN1_Tr_tmp, YFN2_Tr_tmp, ref YM_Tr);
            DefMiddle_Pel(ZFN1_Tr_tmp, ZFN2_Tr_tmp, ref ZM_Tr);

            L1_Tr = Math.Sqrt((XM_Tr - XIRI1_Tr) * (XM_Tr - XIRI1_Tr) +
                              (YM_Tr - YIRI1_Tr) * (YM_Tr - YIRI1_Tr) +
                              (ZM_Tr - ZIRI1_Tr) * (ZM_Tr - ZIRI1_Tr));

            L2_Tr = Math.Sqrt((XM_Tr - XIRI2_Tr) * (XM_Tr - XIRI2_Tr) +
                              (YM_Tr - YIRI2_Tr) * (YM_Tr - YIRI2_Tr) +
                              (ZM_Tr - ZIRI2_Tr) * (ZM_Tr - ZIRI2_Tr));

            if (L1_Tr < L2_Tr)
            {
                XIRI_Tr = XIRI1_Tr;
                YIRI_Tr = YIRI1_Tr;
                ZIRI_Tr = ZIRI1_Tr;
            }
            else
            {
                XIRI_Tr = XIRI2_Tr;
                YIRI_Tr = YIRI2_Tr;
                ZIRI_Tr = ZIRI2_Tr;
            }
            // *********************************************** XIRI_Tr,YIRI_Tr,ZIRI_Tr

            // RIRI_Tr,LatIRI_Tr,LongIRI_Tr ******************************************
            // Угловые координаты искомого ИРИ в опорной геоцентрической СК

            // !!! Д.б. *pRIRI_Tr==REarth_Pel, т.е. r=Rз
            RIRI_Tr = Math.Sqrt(XIRI_Tr * XIRI_Tr + YIRI_Tr * YIRI_Tr + ZIRI_Tr * ZIRI_Tr);

            LatIRI_Tr = Math.Asin(ZIRI_Tr / RIRI_Tr);

            Def_Longitude_180(
                          XIRI_Tr,
                          YIRI_Tr,
                          RIRI_Tr,
                          LatIRI_Tr,
                          ref LongIRI_Tr
                         );


            // Перевод в град
            // Угловые координаты искомого ИРИ в опорной геоцентрической СК
            LatIRI_Tr = (LatIRI_Tr * 180) / Math.PI;
            LongIRI_Tr = (LongIRI_Tr * 180) / Math.PI;

            // LLPP1
            ClassGeoCalculator.f_XYZ_BLH_84(
                          XIRI_Tr,
                          YIRI_Tr,
                          ZIRI_Tr,
                          ref LatIRI_Tr,
                          ref LongIRI_Tr
                                     );
            // ********************************************************** Уточнение координат

*/




            // ????????????????????????????????????????????????????????????????????????????????????????












            return JamStObj;










        } // Функция DefineCoordTriang
        //**************************************************************************


        // ***********************************************************************
        // Вычисление расстояния между объектами с учетом кривизны Земли

        // Входные параметры:
        // List<Coord> CoordJam:
        // CoordJam[i].Latitude,CoordJam[i].Longitude - координаты СПi в градусах WGS84
        // 
        // Coord CoordSource:
        // CoordSource.Latitude, CoordSource.Longitude - координаты ИРИ в градусах WGS84
        //
        // Выходные параметры:
        // List,double> - расстояния между СПi и ИРИ в м
        // ***********************************************************************

        public static List<double> Distance(List<Coord> CoordJam, Coord CoordSource)

        {
            // ------------------------------------------------------------------------
            List<double> dist = new List<double>();
            int i = 0;
            // ------------------------------------------------------------------------
            for(i=0; i< CoordJam.Count; i++)
            {
                double dst = 0;
                dst = f_D_2Points(
                                 CoordJam[i].Latitude,
                                 CoordJam[i].Longitude,
                                 CoordSource.Latitude,
                                 CoordSource.Longitude,
                                 1 // WGS84
                                 );

                dist.Add(dst);

            } // for
            // ------------------------------------------------------------------------
            return dist;
            // ------------------------------------------------------------------------

        } // Функция DefineCoordTriang
        //**************************************************************************

    } // ClassBearing
} // NameSpace
